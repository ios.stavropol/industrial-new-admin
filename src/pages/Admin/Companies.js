import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import { Box, Container } from '@material-ui/core';
import CustomerListResults from '../../components/customer/CustomerListResults';
import customers from '../../__mocks__/customers';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import { getCompanies } from '../../helpers/Network';

const Companies = ({ onMobileClose, openMobile }) => {

  const location = useLocation();
  const navigate = useNavigate();

  const [companies, setCompanies] = React.useState([]);

  useEffect(() => {
    if (openMobile && onMobileClose) {
      onMobileClose();
    }
    getCompanies()
    .then(companies => {
      setCompanies(companies);
    })
    .catch(err => {

    });
  }, [location.pathname]);

  return (<div>
    <Helmet>
      <title>Компании</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
      <Container maxWidth={false}>
      <Card>
            <CardHeader
                action={
                    <Button variant="contained" color="primary" onClick={() => {
                      navigate('/admin/company/0', { replace: true });
                    }}>
                        Добавить компанию
                    </Button>
                }
            />
            <CardContent>
                <CustomerListResults customers={companies} />
            </CardContent>
        </Card>
      </Container>
    </Box>
  </div>);
};

export default Companies;
