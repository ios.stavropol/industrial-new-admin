import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { Link as RouterLink, useLocation, useNavigate, useParams } from 'react-router-dom';
import { useTheme } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/styles';
import { Box, Container, Modal, Typography, FormControl, TextField, Autocomplete, FormControlLabel, Switch, Select, OutlinedInput, MenuItem, Grid } from '@material-ui/core';
import QuestionsTable from '../../components/Questions/QuestionsTable';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { getQuestions, getAllQuestions, deleteQuestion, uploadQuestions } from '../../helpers/Network';
import QuestionModalView from './../../components/Questions/QuestionModalView';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import swal from 'sweetalert';
import Config from '../../constants/Config';

const useStyles = makeStyles((theme) => ({
    root: {
      '& > *': {
        margin: 1,
      },
    },
    input: {
      display: 'none',
    },
}));

const Questions = ({ onMobileClose, openMobile }) => {

  const location = useLocation();
  const navigate = useNavigate();

  const classes = useStyles();

  let { id, checklist } = useParams();

  const [questions, setQuestions] = React.useState([]);
  const [selectedQuestion, setSelectedQuestion] = React.useState({
      id: 0,
      question: '',
      fields: [],
  });
  const [questionOptions, setQuestionOptions] = React.useState([]);
  const [showModal, setShowModal] = React.useState(false);
  const [open, setOpen] = React.useState(false);

  const [value, setValue] = React.useState(null);

  const theme = useTheme();

  const changeFileHandler = event => {
      setOpen(true);
      uploadQuestions(checklist, event.target.files[0])
      .then(() => {
        setOpen(false);
        getQuestions(checklist)
        .then(questions => {
            setQuestions(questions);
        })
        .catch(err => {

        });
        getAllQuestions(checklist)
        .then(questions => {
            let questionOptions = [];
            for (let i = 0; i < questions.length; i++) {
                questionOptions.push({
                    value: questions[i].id,
                    label: questions[i].question,
                    fields: questions[i].fields,
                });
            }

            setQuestionOptions(questionOptions);
        })
        .catch(err => {

        });
      })
      .catch(err => {
        setOpen(false);
        swal(Config.appName, err, "warning");
      });
  }
  
  useEffect(() => {
    getQuestions(checklist)
    .then(questions => {
        setQuestions(questions);
    })
    .catch(err => {

    });
    getAllQuestions(checklist)
    .then(questions => {
        let questionOptions = [];
        for (let i = 0; i < questions.length; i++) {
            questionOptions.push({
                value: questions[i].id,
                label: questions[i].question,
                fields: questions[i].fields,
            });
        }

        setQuestionOptions(questionOptions);
    })
    .catch(err => {

    });
  }, [location.pathname]);

  return (<div>
    <Helmet>
      <title>Чеклисты</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
      <Container maxWidth={false}>
      <Card>
            <CardContent>
                <Box display="flex" flexDirection="row" justifyContent="space-between" mb={1}>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => {
                            setSelectedQuestion({id: 0, question: '', fields: null, is_new: true});
                            setShowModal(true);
                        }}
                        style={{
                            marginBottom: 10,
                        }}
                    >
                        Добавить вопрос
                    </Button>
                    <input
                        className={classes.input}
                        id="contained-button-file"
                        type="file"
                        onChange={changeFileHandler}
                    />
                    <label htmlFor="contained-button-file">
                        <Button variant="contained" color="primary" component="span">
                            Загрузить вопросы
                        </Button>
                    </label>
                </Box>
                <QuestionsTable
                    customers={questions}
                    process_id={id}
                    onEditStaff={staff => {
                        setSelectedQuestion(staff);
                        setShowModal(true);
                    }}
                    onDeleteStaff={staff => {
                        swal({
                            title: Config.appName,
                            text: 'Вы хотите удалить вопрос?',
                            icon: "warning",
                            buttons: ['Нет', 'Да'],
                            dangerMode: true,
                        })
                        .then((willDelete) => {
                            if (willDelete) {
                                deleteQuestion(staff.id)
                                .then(() => {
                                    getQuestions(checklist)
                                    .then(questions => {
                                        setQuestions(questions);
                                    })
                                    .catch(err => {
                                        swal(Config.appName, err, "warning");
                                    });
                                })
                                .catch(err => {
                                    swal(Config.appName, err, "warning");
                                });
                            }
                        });
                    }}
                />
            </CardContent>
        </Card>
      </Container>
    </Box>
    <QuestionModalView
        open={showModal}
        questionsArray={questionOptions}
        question={selectedQuestion}
        onClose={() => {
            setShowModal(false);
        }}
        onRefresh={() => {
            setShowModal(false);
            getQuestions(checklist)
            .then(questions => {
                setQuestions(questions);
            })
            .catch(err => {
                swal(Config.appName, err, "warning");
            });
            getAllQuestions(checklist)
            .then(questions => {
                let questionOptions = [];
                for (let i = 0; i < questions.length; i++) {
                    questionOptions.push({
                        value: questions[i].id,
                        label: questions[i].question,
                        fields: questions[i].fields,
                    });
                }

                setQuestionOptions(questionOptions);
            })
            .catch(err => {

            });
        }}
    />
    <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={open}
        onClick={() => {

        }}
    >
        <CircularProgress color="inherit" />
    </Backdrop>
  </div>);
};

export default Questions;
