import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import { Box,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Container, Modal, Typography, FormControl, TextField, Link } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Network, { getProcess, createProcess, deleteProcess } from '../../helpers/Network';
import PropTypes from 'prop-types';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem, { useTreeItem } from '@material-ui/lab/TreeItem';
import clsx from 'clsx';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import swal from 'sweetalert';
import Config from '../../constants/Config';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    // border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const CustomContent = React.forwardRef(function CustomContent(props, ref) {
    const {
      classes,
      className,
      label,
      nodeId,
      icon: iconProp,
      expansionIcon,
      displayIcon,
      children,
    } = props;
  
    const {
      disabled,
      expanded,
      selected,
      focused,
      handleExpansion,
      handleSelection,
      preventSelection,
    } = useTreeItem(nodeId);
  
    const icon = iconProp || expansionIcon || displayIcon;
  
    const handleMouseDown = (event) => {
      preventSelection(event);
    };
  
    const handleExpansionClick = (event) => {
      handleExpansion(event);
    };
  
    const handleSelectionClick = (event) => {
      handleSelection(event);
    };

    const deleteButton = (children) => {

        if (children !== undefined && children.children !== undefined && children.children.length) {
            return null;
        }
        return (<IconButton onClick={() => {
            let data = JSON.parse(nodeId);
            if (Network.onAdminProcessDelete) {
                Network.onAdminProcessDelete(data);
            }
        }}>
            <DeleteIcon />
        </IconButton>);
    }
  
    return (
      // eslint-disable-next-line jsx-a11y/no-static-element-interactions
      <div
        className={clsx(className, classes.root, {
          [classes.expanded]: expanded,
          [classes.selected]: selected,
          [classes.focused]: focused,
          [classes.disabled]: disabled,
        })}
        onMouseDown={handleMouseDown}
        ref={ref}
      >
        {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
        <div onClick={handleExpansionClick} className={classes.iconContainer}>
          {icon}
        </div>
        <Link style={{
            fontFamily: 'Source Sans Pro',
        }} href={"/admin/process/"+JSON.parse(nodeId).id}>{label}</Link>
        <IconButton onClick={() => {
            let data = JSON.parse(nodeId);
            if (Network.onAdminProcessEdit) {
                Network.onAdminProcessEdit({
                    id: 0,
                    name: '',
                    parent_id: data.id,
                });
            }
        }}>
            <AddIcon />
        </IconButton>
        <IconButton onClick={() => {
            let data = JSON.parse(nodeId);
            if (Network.onAdminProcessEdit) {
                Network.onAdminProcessEdit(data);
            }
        }}>
            <EditIcon />
        </IconButton>
        {deleteButton(JSON.parse(nodeId))}
      </div>
    );
  });
  
  CustomContent.propTypes = {
    /**
     * Override or extend the styles applied to the component.
     */
    classes: PropTypes.object.isRequired,
    /**
     * className applied to the root element.
     */
    className: PropTypes.string,
    /**
     * The icon to display next to the tree node's label. Either a parent or end icon.
     */
    displayIcon: PropTypes.node,
    /**
     * The icon to display next to the tree node's label. Either an expansion or collapse icon.
     */
    expansionIcon: PropTypes.node,
    /**
     * The icon to display next to the tree node's label.
     */
    icon: PropTypes.node,
    /**
     * The tree node label.
     */
    label: PropTypes.node,
    /**
     * The id of the node.
     */
    nodeId: PropTypes.string.isRequired,
    /**
     * The data of the node.
     */
     onEdit: PropTypes.func.isRequired,
  };
  
  const CustomTreeItem = (props) => {
      return (
        <TreeItem ContentComponent={CustomContent} {...props} />
        )
    };

const Process = ({props}) => {

    const navigate = useNavigate();

    const [tree, setTree] = React.useState([]);
    const [process, setProcess] = React.useState([]);
    const [open, setOpen] = React.useState(false);
    const [showModal, setShowModal] = React.useState(false);
    const [name, setName] = React.useState('');
    const [parent_id, setParentId] = React.useState(0);
    const [selected_id, setSelectedId] = React.useState(0);

    const getChilds = (item) => {

        const isChildren = item.children !== null;
        if (isChildren) {
            return (
                <CustomTreeItem key={item.id} nodeId={JSON.stringify(item)} label={item.text + ' (' + item.checklists + ')'} onEdit={() => {
                    
                }}>
                    {item.children.map((node, idx) => getChilds(node))}
                </CustomTreeItem>
            );
        }
        return <CustomTreeItem key={item.id} nodeId={JSON.stringify(item)} label={item.text + ' (' + item.checklists + ')'}  onEdit={() => {
            
        }}/>;
    }

    const onAdminProcessEdit = data => {
        setName(data.text);
        setParentId(data.parent_id);
        setSelectedId(data.id);
        setShowModal(true);
    }

    const onAdminProcessDelete = data => {
        swal({
            title: Config.appName,
            text: 'Вы хотите удалить бизнес-процесс?',
            icon: "warning",
            buttons: ['Нет', 'Да'],
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                deleteProcess(data.id)
                .then(() => {
                    getProcess()
                    .then(process => {
                        setProcess(process);
                        let array = [];
                        for (let i = 0; i < process.length; i++) {
                            array.push(getChilds(process[i]));
                        }
                        setTree(array);
                    })
                    .catch(err => {

                    });
                })
                .catch(err => {
                    swal(Config.appName, err, "warning");
                });
            }
        });
    }

    useEffect(() => {
        Network.onAdminProcessEdit = onAdminProcessEdit;
        Network.onAdminProcessDelete = onAdminProcessDelete;
        getProcess()
        .then(process => {
            setProcess(process);
            let array = [];
            for (let i = 0; i < process.length; i++) {
                array.push(getChilds(process[i]));
            }
            setTree(array);
        })
        .catch(err => {

        });
    }, process);

  return (<div>
    <Helmet>
      <title>Бизнес-процессы</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
      <Container maxWidth={false}>
      <Card>
            <CardContent>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => {
                        if (Network.onAdminProcessEdit) {
                            Network.onAdminProcessEdit({
                                id: 0,
                                name: '',
                                parent_id: 0,
                            });
                        }
                    }}
                    style={{
                        marginBottom: 10,
                    }}
                >
                    Добавить родительский бизнес-процесс
                </Button>
                <TreeView
                    aria-label="icon expansion"
                    defaultCollapseIcon={<ExpandMoreIcon />}
                    defaultExpandIcon={<ChevronRightIcon />}
                    sx={{ height: '100%', flexGrow: 1, maxWidth: '100%', overflowY: 'auto' }}
                >
                    {tree}
                </TreeView>
            </CardContent>
        </Card>
      </Container>
    </Box>
    <Dialog
        open={showModal}
        onClose={(event, reason) => {
            if (reason !== 'backdropClick') {
                
            }
        }}
        fullWidth
        maxWidth="sm"
    >
        <DialogTitle>
            <Typography variant="h4">
                {selected_id !== null && selected_id === 0 ? 'Добавление бизнес-процесса' : 'Редактирование бизнес-процесса'}
            </Typography>
            <IconButton
                aria-label="close"
                onClick={() => {
                    setShowModal(false);
                }}
                sx={{
                    position: 'absolute',
                    right: 8,
                    top: 8,
                    color: (theme) => theme.palette.grey[500],
                }}
                >
                <CloseIcon />
            </IconButton>
        </DialogTitle>
        <DialogContent>
            <FormControl fullWidth sx={{
                m: 0,
                marginTop: 2,
            }}>
                <TextField
                    id="name"
                    size='small'
                    label="Название"
                    variant="outlined"
                    value={name}
                    onChange={event => {
                        setName(event.target.value);
                    }}
                />
            </FormControl>
        </DialogContent>
        <DialogActions>
            <Button variant="contained" color="primary" onClick={() => {
                if (name !== null && name !== undefined && name.length) {
                    createProcess(selected_id, name, parent_id)
                    .then(() => {
                        getProcess()
                        .then(process => {
                            setProcess(process);
                            let array = [];
                            for (let i = 0; i < process.length; i++) {
                                array.push(getChilds(process[i]));
                            }
                            setTree(array);
                            setShowModal(false);
                        })
                        .catch(err => {

                        });
                    })
                    .catch(err => {
                        swal(Config.appName, err, "warning");
                    });
                } else {
                    swal(Config.appName, 'Укажите название!', "warning");
                }
            }}>
                Сохранить
            </Button>
        </DialogActions>
    </Dialog>
  </div>);
};

export default Process;
