import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import { Box, 
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Container, Modal, Typography, FormControl, TextField } from '@material-ui/core';
import UnitTable from '../../components/Units/UnitTable';
import customers from '../../__mocks__/customers';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import { getUnits, saveUnit, deleteUnit, getCompanies } from '../../helpers/Network';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import swal from 'sweetalert';
import Config from '../../constants/Config';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    // border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const Units = ({ onMobileClose, openMobile }) => {

  const location = useLocation();
  const navigate = useNavigate();

  const [companies, setCompanies] = React.useState([]);
  const [units, setUnits] = React.useState([]);
  const [selectedUnit, setSelectedUnit] = React.useState(null);
  const [showModal, setShowModal] = React.useState(false);
  const [name, setName] = React.useState('');

  useEffect(() => {
    getCompanies()
    .then(companies => {
      getUnits()
      .then(units => {
        setCompanies(companies);
        setUnits(units);
      })
      .catch(err => {

      });
    })
    .catch(err => {

    });
  }, [location.pathname]);

  return (<div>
    <Helmet>
      <title>Должности</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
      <Container maxWidth={false}>
      <Card>
            <CardHeader
            />
            <CardContent>
                <UnitTable
                    customers={units}
                    companies={companies}
                    onEditStaff={unit => {
                        setSelectedUnit(unit);
                        setShowModal(true);
                        setName(unit.name);
                    }}
                    onDeleteStaff={staff => {
                        swal({
                            title: Config.appName,
                            text: 'Вы хотите удалить подразделение?',
                            icon: "warning",
                            buttons: ['Нет', 'Да'],
                            dangerMode: true,
                        })
                        .then((willDelete) => {
                            if (willDelete) {
                                deleteUnit(staff.id)
                                .then(() => {
                                    getUnits()
                                    .then(units => {
                                        setUnits(units);
                                    })
                                    .catch(err => {
                                        swal(Config.appName, err, "warning");
                                    });
                                })
                                .catch(err => {
                                    swal(Config.appName, err, "warning");
                                });
                            }
                        });
                    }}
                />
            </CardContent>
        </Card>
      </Container>
    </Box>
    <Dialog
        open={showModal}
        onClose={(event, reason) => {
            if (reason !== 'backdropClick') {
                
            }
        }}
        fullWidth
        maxWidth="sm"
    >
        <DialogTitle>
            <Typography variant="h4">
                Редактирование подразделения
            </Typography>
            <IconButton
                aria-label="close"
                onClick={() => {
                    setShowModal(false);
                }}
                sx={{
                    position: 'absolute',
                    right: 8,
                    top: 8,
                    color: (theme) => theme.palette.grey[500],
                }}
                >
                <CloseIcon />
            </IconButton>
        </DialogTitle>
        <DialogContent>
            <FormControl fullWidth sx={{
                m: 0,
                marginTop: 2,
            }}>
                <TextField
                    id="name"
                    size='small'
                    label="Название"
                    variant="outlined"
                    value={name}
                    onChange={event => {
                        setName(event.target.value);
                    }}
                />
            </FormControl>
        </DialogContent>
        <DialogActions>
            <Button variant="contained" color="primary" onClick={() => {
                if (name.length) {
                    saveUnit(selectedUnit.id, name)
                    .then(() => {
                        getUnits()
                        .then(units => {
                            setUnits(units);
                            setShowModal(false);
                        })
                        .catch(err => {
                            swal(Config.appName, err, "warning");
                        });
                    })
                    .catch(err => {
                        swal(Config.appName, err, "warning");
                    });
                } else {
                    swal(Config.appName, 'Укажите название!', "warning");
                }
            }}>
                Сохранить
            </Button>
        </DialogActions>
    </Dialog>
  </div>);
};

export default Units;
