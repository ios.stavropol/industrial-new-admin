import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { Link as RouterLink, useLocation, useNavigate, useParams } from 'react-router-dom';
import { useTheme } from '@material-ui/core/styles';
import { Box, 
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    ListItemText,
    Checkbox,
    Container, Modal, Typography, FormControl, TextField, Select, OutlinedInput, MenuItem } from '@material-ui/core';
import ChecklistTable from '../../components/Checklists/ChecklistTable';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import { getChecklists, saveChecklist, cloneChecklist, deleteChecklist } from '../../helpers/Network';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import swal from 'sweetalert';
import Config from '../../constants/Config';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    // border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
    //   width: '100%',
    },
  },
};

const roles = [
    {value: 'top-manager', label: 'Топ менеджер'},
    {value: 'middle-manager', label: 'Менеджер среднего звена'},
    {value: 'specialist', label: 'Специалист'},
    {value: 'worker', label: 'Рабочий'},
];

function getStyles(name, personName, theme) {
    return {
      fontWeight:
        personName.indexOf(name) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    };
}

const Checklists = ({ onMobileClose, openMobile }) => {

  const location = useLocation();
  const navigate = useNavigate();

  let { id } = useParams();

  const [checklists, setChecklists] = React.useState([]);
  const [selectedChecklist, setSelectedChecklist] = React.useState(null);
  const [showModal, setShowModal] = React.useState(false);
  const [clone, setClone] = React.useState(false);
  const [name, setName] = React.useState('');

  const theme = useTheme();
  const [personName, setPersonName] = React.useState([]);

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setPersonName(
      // On autofill we get a the stringified value.
      typeof value === 'string' ? value.split(',') : value,
    );
  };

  useEffect(() => {
    getChecklists(id)
    .then(checklists => {
        setChecklists(checklists);
    })
    .catch(err => {

    });
  }, [location.pathname]);

  return (<div>
    <Helmet>
      <title>Чеклисты</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
      <Container maxWidth={false}>
      <Card>
            <CardContent>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => {
                        setSelectedChecklist({
                            id: 0,
                            name: '',
                            roles: [],
                        });
                        setPersonName([]);
                        setShowModal(true);
                        setName('');
                    }}
                    style={{
                        marginBottom: 10,
                    }}
                >
                    Добавить чеклист
                </Button>
                <ChecklistTable
                    customers={checklists}
                    process_id={id}
                    onCloneStaff={checklist => {
                        setSelectedChecklist(checklist);
                        setPersonName(JSON.parse(checklist.role));
                        setName(checklist.name);
                        setClone(true);
                        setShowModal(true);
                    }}
                    onEditStaff={staff => {
                        setSelectedChecklist(staff);
                        setPersonName(JSON.parse(staff.role));
                        setClone(false);
                        setShowModal(true);
                        setName(staff.name);
                    }}
                    onDeleteStaff={staff => {
                        swal({
                            title: Config.appName,
                            text: 'Вы хотите удалить чеклист?',
                            icon: "warning",
                            buttons: ['Нет', 'Да'],
                            dangerMode: true,
                        })
                        .then((willDelete) => {
                            if (willDelete) {
                                deleteChecklist(staff.id)
                                .then(() => {
                                    getChecklists(id)
                                    .then(checklists => {
                                        setChecklists(checklists);
                                    })
                                    .catch(err => {
                                        swal(Config.appName, err, "warning");
                                    });
                                })
                                .catch(err => {
                                    swal(Config.appName, err, "warning");
                                });
                            }
                        });
                    }}
                />
            </CardContent>
        </Card>
      </Container>
    </Box>
    <Dialog
        open={showModal}
        onClose={(event, reason) => {
            if (reason !== 'backdropClick') {
                
            }
        }}
        fullWidth
        maxWidth="sm"
    >
        <DialogTitle>
            <Typography variant="h4">
                {selectedChecklist !== null && selectedChecklist.id === 0 ? 'Добавление чеклиста' : 'Редактирование чеклиста'}
            </Typography>
            <IconButton
                aria-label="close"
                onClick={() => {
                    setShowModal(false);
                }}
                sx={{
                    position: 'absolute',
                    right: 8,
                    top: 8,
                    color: (theme) => theme.palette.grey[500],
                }}
                >
                <CloseIcon />
            </IconButton>
        </DialogTitle>
        <DialogContent>
            <FormControl fullWidth sx={{
                m: 0,
                marginTop: 2,
                marginBottom: 2,
            }}>
                <TextField
                    id="name"
                    size='small'
                    label="Название"
                    variant="outlined"
                    value={name}
                    onChange={event => {
                        setName(event.target.value);
                    }}
                />
            </FormControl>
            <FormControl sx={{
                m: 0,
                width: '100%',
                mt: 1,
            }}>
                <Select
                    multiple
                    size='small'
                    // autoWidth
                    displayEmpty
                    value={personName}
                    onChange={handleChange}
                    input={<OutlinedInput />}
                    renderValue={(selected) => {
                        if (selected.length === 0) {
                        return <em>Роли</em>;
                        }
                        
                        let str = '';

                        for (let i=0;i<roles.length;i++) {
                            if (selected.includes(roles[i].value)) {
                                str = str + roles[i].label + ', ';
                            }
                        }
                        return str;//selected.join(', ');
                    }}
                    MenuProps={MenuProps} 
                    inputProps={{ 'aria-label': 'Without label' }}
                >
                <MenuItem disabled value="">
                    <em>Роли</em>
                </MenuItem>
                {roles.map((name) => (
                    <MenuItem
                        key={name.value}
                        value={name.value}
                        style={getStyles(name, personName, theme)}
                    >
                        <Checkbox checked={personName.indexOf(name.value) > -1} />
                        <ListItemText primary={name.label} />
                    </MenuItem>
                ))}
                </Select>
            </FormControl>
        </DialogContent>
        <DialogActions>
            <Button variant="contained" color="primary" onClick={() => {
                if (name.length) {
                    if (personName === null || personName.length === 0) {
                        swal(Config.appName, 'Выберите роли!', "warning");
                        return;
                    }

                    if (clone) {
                        cloneChecklist(selectedChecklist.id, name, id, personName)
                        .then(() => {
                            getChecklists(id)
                            .then(checklists => {
                                setChecklists(checklists);
                                setShowModal(false);
                                setPersonName([]);
                            })
                            .catch(err => {
                                swal(Config.appName, err, "warning");
                            });
                        })
                        .catch(err => {
                            swal(Config.appName, err, "warning");
                        });
                    } else {
                        saveChecklist(selectedChecklist.id, name, id, personName)
                        .then(() => {
                            getChecklists(id)
                            .then(checklists => {
                                setChecklists(checklists);
                                setShowModal(false);
                                setPersonName([]);
                            })
                            .catch(err => {
                                swal(Config.appName, err, "warning");
                            });
                        })
                        .catch(err => {
                            swal(Config.appName, err, "warning");
                        });
                    }
                } else {
                    swal(Config.appName, 'Укажите название!', "warning");
                }
            }}>
                Сохранить
            </Button>
        </DialogActions>
    </Dialog>
  </div>);
};

export default Checklists;
