import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import { useTheme } from '@material-ui/core/styles';
import { Box, Container, Modal, Typography, FormControl, TextField, Autocomplete, FormControlLabel, Switch, Select, OutlinedInput, MenuItem, Grid } from '@material-ui/core';
import PermissionsTable from '../../components/Permissions/PermissionsTable';
import customers from '../../__mocks__/customers';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { getPermissions, updatePermission } from '../../helpers/Network';

const Permissions = ({}) => {

    const location = useLocation();

  const [permissions, setPermissions] = React.useState([]);
  
  useEffect(() => {
    getPermissions()
    .then(permissions => {
        setPermissions(permissions);
    })
    .catch(err => {

    });
  }, [location.pathname]);

  return (<div>
    <Helmet>
      <title>Разрешения</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
      <Container maxWidth={false}>
      <Card>
            <CardContent>
                <PermissionsTable
                    customers={permissions}
                    updatePermission={(role, permission, value) => {
                        updatePermission(role, permission, value)
                        .then(() => {
                            getPermissions()
                            .then(permissions => {
                                setPermissions(permissions);
                            })
                            .catch(err => {

                            });
                        })
                        .catch(err => {

                        });
                    }}
                />
            </CardContent>
        </Card>
      </Container>
    </Box>
  </div>);
};

export default Permissions;
