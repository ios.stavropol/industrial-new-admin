import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { Link as RouterLink, useLocation, useNavigate, useParams } from 'react-router-dom';
import { Box, Container } from '@material-ui/core';
import CustomerListResults from '../../components/customer/CustomerListResults';
import customers from '../../__mocks__/customers';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import { getCompany } from '../../helpers/Network';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import CommonInfo from '../../components/Company/CommonInfo';
import Units from '../../components/Company/Units';
import UsersView from '../../components/Company/UsersView';
import ProcessView from '../../components/Company/ProcessView';

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
  };
  
  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }

const Company = ({ onMobileClose, openMobile }) => {

  const location = useLocation();
  const navigate = useNavigate();

  let { id } = useParams();

  const [value, setValue] = React.useState(0);
  const [company, setCompany] = React.useState(null);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const [companies, setCompanies] = React.useState([]);

  useEffect(() => {
    if (openMobile && onMobileClose) {
      onMobileClose();
    }
    getCompany(id)
      .then(company => {
        setCompany(company);
      })
      .catch(err => {

      });
  }, [location.pathname]);

  return (<div>
    <Helmet>
      <title>Компания</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
      <Container maxWidth={false}>
      <Card>
            <CardHeader
            />
            <CardContent>
                <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                    <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                        <Tab label="Общая информация" {...a11yProps(0)} />
                        <Tab label="Подразделения" {...a11yProps(1)} />
                        <Tab label="Сотрудники" {...a11yProps(2)} />
                        <Tab label="Бизнес-процессы" {...a11yProps(3)} />
                    </Tabs>
                </Box>
                <TabPanel value={value} index={0}>
                    <CommonInfo company={company} id={id} />
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <Units id={id} />
                </TabPanel>
                <TabPanel value={value} index={2}>
                    <UsersView id={id} />
                </TabPanel>
                <TabPanel value={value} index={3}>
                    <ProcessView id={id} />
                </TabPanel>
            </CardContent>
        </Card>
      </Container>
    </Box>
  </div>);
};

export default Company;
