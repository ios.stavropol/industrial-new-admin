import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import { Box, 
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  InputAdornment,
  Container, FormControl, TextField, Modal, Typography } from '@material-ui/core';
import CustomerListResults from '../components/customer/CustomerListResults';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Network, { updateSelf, updatePassword, getProfile } from '../helpers/Network';
import ImageUploader from '../components/ImageUploader';
import Config from '../constants/Config';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import swal from 'sweetalert';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 600,
  bgcolor: 'background.paper',
  // border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const Account = ({}) => {

  const location = useLocation();
  const navigate = useNavigate();

  const [imagePreviewUrl, setImagePreviewUrl] = React.useState(null);
  const [imageChanged, setImageChanged] = React.useState(false);
  const [lastname, setLastname] = React.useState('');
  const [firstname, setFirstname] = React.useState('');
  const [patrname, setPatrname] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [open, setOpen] = React.useState(false);
  const [showModal, setShowModal] = React.useState(false);
  const [password, setPassword] = React.useState('');
  const [new_password, setNewpassword] = React.useState('');
  const [new_password2, setNewpassword2] = React.useState('');
  const [showPassword, setShowPassword] = React.useState(false);
  const handleClickShowPassword = () => setShowPassword(!showPassword);
  const handleMouseDownPassword = () => setShowPassword(!showPassword);

  useEffect(() => {
    getProfile()
    .then(profile => {
      setLastname(profile.lastname);
      setFirstname(profile.firstname);
      setPatrname(profile.patrname);
      setEmail(profile.email);
      setImageChanged(false);
      if (profile.avatar !== null && profile.avatar.length) {
        setImagePreviewUrl(Config.apiDomain + '/uploads/user/'+profile.avatar);
      }
    })
    .catch(err => {

    });
  }, [location.pathname]);

  return (<div>
    <Helmet>
      <title>Профиль</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
      <Container maxWidth={false}>
        <Card
          sx={{ height: '100%' }}
        >
          <CardContent>
              <ImageUploader 
                  onChangeImage={file => {
                      setImageChanged(true);
                      setImagePreviewUrl(file.result);
                  }}
                  onDeleteImage={() => {
                    setImageChanged(true);
                    setImagePreviewUrl(null);
                  }}
                  image={imagePreviewUrl}
              />
              <FormControl fullWidth sx={{
                      m: 0,
                      marginTop: 2,
                  }}>
                  <TextField
                      id="name"
                      size='small'
                      label="Фамилия"
                      variant="outlined"
                      value={lastname}
                      onChange={event => {
                          setLastname(event.target.value);
                      }}
                  />
              </FormControl>
              <FormControl fullWidth sx={{
                  m: 0,
                  marginTop: 2,
              }}>
                  <TextField
                      id="address"
                      size='small'
                      label="Имя"
                      variant="outlined"
                      value={firstname}
                      onChange={event => {
                          setFirstname(event.target.value);
                      }}
                  />
              </FormControl>
              <FormControl fullWidth sx={{
                  m: 0,
                  marginTop: 2,
              }}>
                  <TextField
                      id="email"
                      size='small'
                      label="Отчество"
                      variant="outlined"
                      value={patrname}
                      onChange={event => {
                          setPatrname(event.target.value);
                      }}
                  />
              </FormControl>
              <FormControl fullWidth sx={{
                      m: 0,
                      marginTop: 2,
                      mb: 2,
                  }}>
                  <TextField
                      id="name"
                      size='small'
                      label="E-mail"
                      variant="outlined"
                      value={email}
                      disabled
                  />
              </FormControl>
              <Button variant="contained" color="primary" onClick={() => {

                if (lastname === null || lastname.length === 0 || firstname === null || firstname.length === 0) {
                  swal(Config.appName, 'Фамилия и имя - обязательные поля!', "warning");
                  return;
                }
                  setOpen(true);
                  let avatar = null;

                  if(imageChanged) {
                      avatar = imagePreviewUrl;
                  }

                  updateSelf(lastname, firstname, patrname, avatar)
                  .then(() => {
                      setOpen(false);
                      getProfile()
                      .then(profile => {
                        Network.updateLayout = !Network.updateLayout;
                        swal(Config.appName, 'Изменения сохранены!', "success");
                      })
                      .catch(err => {

                      });
                  })
                  .catch(err => {
                      setOpen(false);
                      swal(Config.appName, err, "warning");
                  });
              }}>
                  Сохранить
              </Button>
              <Button sx={{
                ml: 2,
              }} variant="contained" color="primary" onClick={() => {
                setPassword('');
                setNewpassword('');
                setNewpassword2('');
                setShowModal(true);
              }}>
                Изменить пароль
              </Button>
          </CardContent>
          <Backdrop
              sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
              open={open}
              onClick={() => {

              }}
          >
              <CircularProgress color="inherit" />
          </Backdrop>
        </Card>
      </Container>
    </Box>
    <Dialog
      open={showModal}
      onClose={(event, reason) => {
          if (reason !== 'backdropClick') {
              
          }
      }}
      fullWidth
      maxWidth="sm"
    >
      <DialogTitle>
          <Typography variant="h4">
            Изменение пароля
          </Typography>
          <IconButton
              aria-label="close"
              onClick={() => {
                  setShowModal(false);
              }}
              sx={{
                  position: 'absolute',
                  right: 8,
                  top: 8,
                  color: (theme) => theme.palette.grey[500],
              }}
              >
              <CloseIcon />
          </IconButton>
      </DialogTitle>
      <DialogContent>
        <FormControl fullWidth sx={{
            m: 0,
            marginTop: 2,
            marginBottom: 2,
        }}>
            <TextField
              id="name"
              size='small'
              label="Старый пароль"
              variant="outlined"
              value={password}
              required={true}
              type={showPassword ? 'text' : 'password'}
              InputProps={{ // <-- This is where the toggle button is added.
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                    >
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                )
              }}
              onChange={event => {
                  setPassword(event.target.value);
              }}
            />
        </FormControl>
        <FormControl fullWidth sx={{
            m: 0,
            // marginTop: 2,
            marginBottom: 2,
        }}>
            <TextField
              id="name"
              size='small'
              label="Новый пароль"
              variant="outlined"
              value={new_password}
              required={true}
              onChange={event => {
                setNewpassword(event.target.value);
              }}
              type={showPassword ? 'text' : 'password'}
              InputProps={{ // <-- This is where the toggle button is added.
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                    >
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                )
              }}
            />
        </FormControl>
        <FormControl fullWidth sx={{
            m: 0,
            // marginTop: 2,
            // marginBottom: 2,
        }}>
            <TextField
              id="name"
              size='small'
              label="Еще раз новый пароль"
              variant="outlined"
              value={new_password2}
              required={true}
              type={showPassword ? 'text' : 'password'}
              InputProps={{ // <-- This is where the toggle button is added.
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                    >
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                )
              }}
              onChange={event => {
                setNewpassword2(event.target.value);
              }}
            />
        </FormControl>
      </DialogContent>
      <DialogActions>
        <Button variant="contained" color="primary" onClick={() => {
          if (password.length && new_password.length && new_password2.length) {
            if (new_password2 !== new_password) {
              swal(Config.appName, 'Поля с новым паролем должны совпадать!', "warning");
              return;
            }
            updatePassword(password, new_password)
            .then(() => {
              setShowModal(false);
              swal(Config.appName, 'Пароль изменен!', "success");
            })
            .catch(err => {
              swal(Config.appName, err, "warning");
            });
          } else {
            swal(Config.appName, 'Заполните все поля!', "warning");
          }
        }}>
          Сохранить
        </Button>
      </DialogActions>
    </Dialog>
  </div>);
};

export default Account;