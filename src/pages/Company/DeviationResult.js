import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  Container,
  Typography,
  Modal,
  FormControl,
  TextField,
  CardHeader,
  Tab,
  Tabs,
} from '@material-ui/core';
import Network, { getDeviation } from '../../helpers/Network';
import Config from '../../constants/Config';
import { Link as RouterLink, useLocation, useNavigate, useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import CommonInfo from './../../components/DeviationResult/CommonInfo';
import EventsView from './../../components/DeviationResult/EventsView';

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
  };
  
function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
};
  }

const DeviationResult = ({ onMobileClose, openMobile }) => {

    const location = useLocation();
    const navigate = useNavigate();
    let {id} = useParams();
    let {devId} = useParams();
    const [value, setValue] = React.useState(0);
	const [info, setInfo] = React.useState(null);
	const [events, setEvents] = React.useState([]);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    useEffect(() => {
        getDeviation(devId)
        .then(result => {
            setInfo(result.deviation);
			setEvents(result.events);
        })
        .catch(err => {

        });
    }, [id, devId]);

  return (<div>
    <Helmet>
      <title>Результаты аудита</title>
    </Helmet>
	<Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
		<Container maxWidth={false}>
			<Card sx={{
				mb: 2,
			}}>
				<CardContent>
					<Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
						<Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
							<Tab label="Описание" {...a11yProps(0)} />
							<Tab label="Мероприятия" {...a11yProps(1)} />
						</Tabs>
					</Box>
					<TabPanel value={value} index={0}>
						<CommonInfo
							data={info}
						/>
					</TabPanel>
					<TabPanel value={value} index={1}>
						<EventsView
							data={info}
							events={events}
							refresh={() => {
								getDeviation(devId)
								.then(result => {
									setInfo(result.deviation);
									setEvents(result.events);
								})
								.catch(err => {

								});
							}}
						/>
					</TabPanel>
				</CardContent>
			</Card>
		</Container>
	</Box>
    </div>);
};

export default DeviationResult;
