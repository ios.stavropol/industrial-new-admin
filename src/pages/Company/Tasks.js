import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  Container,
  Typography,
  Modal,
  FormControl,
  TextField,
  CardHeader,
} from '@material-ui/core';
import Network, { getTasks } from '../../helpers/Network';
import Config from '../../constants/Config';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Link as RouterLink, useLocation } from 'react-router-dom';
import TasksTable from './../../components/Tasks/TasksTable';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    // border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const Tasks = ({ onMobileClose, openMobile }) => {

    const location = useLocation();
    const [tasks, setTasks] = React.useState([]);

    useEffect(() => {
        getTasks()
        .then(tasks => {
            setTasks(tasks);
        })
        .catch(err => {

        });
    }, [location.pathname]);

  return (<div>
    <Helmet>
      <title>Задачи</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
        <Container maxWidth={false}>
            <Card>
                <CardContent>
                    <TasksTable
                        tasks={tasks}
                    />
                </CardContent>
            </Card>
        </Container>
    </Box>
    </div>);
};

export default Tasks;
