import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  Container,
} from '@material-ui/core';
import Network, { getAuditStatistic, getCompanyUnitsList, getProfile } from '../../helpers/Network';
import Config from '../../constants/Config';
import { Link as RouterLink, useLocation, useNavigate, useParams } from 'react-router-dom';
import ChecklistResult from './../../components/AuditResult/ChecklistResult';

const AuditResult = ({ onMobileClose, openMobile }) => {

    const location = useLocation();
    const navigate = useNavigate();
    let {id} = useParams();
    const [audits, setAudits] = React.useState([]);
    const [process, setProcess] = React.useState([]);
    const [cards, setCards] = React.useState([]);
    const [units, setUnits] = React.useState([]);

    useEffect(() => {
      getProfile()
      .then(() => {
        getCompanyUnitsList(Network.userProfile.company_id)
        .then(unitList => {
            setUnits(unitList);
            getAuditStatistic(Network.userProfile.company_id, id)
            .then(statistic => {
                let cardArray = [];
                for (let i = 0; i < statistic.length; i++) {
                    cardArray.push(<ChecklistResult
                        data={statistic[i]}
                        id={id}
                        units={units}
                    />);
                }
                setCards(cardArray);
            })
            .catch(err => {

            });
        })
        .catch(err => {

        });
      })
      .catch(err => {

      });
    }, [location.pathname]);

  return (<div>
    <Helmet>
      <title>Аудит</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
        <Container maxWidth={false}>
            {cards}
        </Container>
    </Box>
    </div>);
};

export default AuditResult;
