import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  Container,
  Typography,
  Modal,
  FormControl,
  TextField,
  CardHeader,
} from '@material-ui/core';
import Network, { getAudits, getCompanyProcessForAudit, deleteAudit } from '../../helpers/Network';
import Config from '../../constants/Config';
import swal from 'sweetalert';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import AuditsTable from './../../components/Audits/AuditsTable';

const Audits = ({ onMobileClose, openMobile }) => {

    const location = useLocation();
    const navigate = useNavigate();
    const [audits, setAudits] = React.useState([]);
    const [process, setProcess] = React.useState([]);

    useEffect(() => {
        getCompanyProcessForAudit(Network.userProfile.company_id)
        .then(process2 => {
            getAudits()
            .then(audits2 => {
                setAudits(audits2);
                setProcess(process2);
            })
            .catch(err => {

            });
        })
        .catch(err => {

        });
    }, [location.pathname]);

  return (<div>
    <Helmet>
      <title>Аудит</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
        <Container maxWidth={false}>
            <Card sx={{
                mt: 2,
            }}>
                <CardHeader
                    action={
                        <Button variant="contained" color="primary" onClick={() => {
                            navigate('/company/audits/0/edit', { replace: true });
                        }}>
                            Добавить аудит
                        </Button>
                    }
                />
                <CardContent>
                    <AuditsTable
                        audits={audits}
                        process={process}
                        onDeleteAudit={audit => {
                            swal({
                                title: Config.appName,
                                text: 'Вы хотите удалить аудит?',
                                icon: "warning",
                                buttons: ['Нет', 'Да'],
                                dangerMode: true,
                            })
                            .then((willDelete) => {
                                if (willDelete) {
                                    deleteAudit(audit.id)
                                    .then(() => {
                                        getCompanyProcessForAudit(Network.userProfile.company_id)
                                        .then(process2 => {
                                            getAudits()
                                            .then(audits2 => {
                                                setAudits(audits2);
                                                setProcess(process2);
                                            })
                                            .catch(err => {

                                            });
                                        })
                                        .catch(err => {

                                        });
                                    })
                                    .catch(err => {
                                        swal(Config.appName, err, "warning");
                                    });
                                }
                            });
                        }}
                        onEditAudit={audit => {
                            navigate('/company/audits/' + audit.id + '/edit', { replace: true });
                        }}
                        onAuditResult={audit => {
                            navigate('/company/audits/' + audit.id + '/result', { replace: true });
                        }}
                    />
                </CardContent>
            </Card>
        </Container>
    </Box>
    </div>);
};

export default Audits;
