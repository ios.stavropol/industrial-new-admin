import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Container,
  Typography,
  Modal,
  FormControl,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Stack,
  Chip
} from '@material-ui/core';
// import { styled } from '@mui/material/styles';
import {styled} from '@material-ui/core';
import Network, { getMyEvents, execEvent, endEvent } from '../../helpers/Network';
import Config from '../../constants/Config';
import swal from 'sweetalert';
import { Link as RouterLink, useLocation } from 'react-router-dom';
import EventsTable from './../../components/Events/EventsTable';
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import DatePicker from '@material-ui/lab/DatePicker';
import ruLocale from 'date-fns/locale/ru';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import DeleteIcon from '@material-ui/icons/Delete';

const Input = styled('input')({
    display: 'none',
});

const Events = ({ onMobileClose, openMobile }) => {

    const location = useLocation();
	const [events, setEvents] = React.useState([]);
	const [eventId, setEventId] = React.useState(null);
    const [devPlace, setDevPlace] = React.useState('');
    const [taskText, setTaskText] = React.useState('');
	const [execText, setExecText] = React.useState('');
    const [taskDate, setTaskDate] = React.useState(null);
	const [status, setStatus] = React.useState(null);
    const [showModal, setShowModal] = React.useState(false);
    const [photos, setPhotos] = React.useState([]);
    const [files, setFiles] = React.useState([]);
    const [photoBody, setPhotoBody] = React.useState(null);

    useEffect(() => {
		getMyEvents()
		.then(staff => {
			setEvents(staff);
		})
		.catch(err => {

		});
    }, [location.pathname]);

    useEffect(() => {
        console.warn('photos: ', photos);
        setPhotoBody(renderPhotos());
    }, [photos]);

    const onchange = event => {
        // console.warn('onchange: ', event);
        let url = URL.createObjectURL(event.target.files[0]);
        console.warn(url);
        let phs = JSON.parse(JSON.stringify(photos));
        phs.push(url);
        setPhotos(phs);
        phs = JSON.parse(JSON.stringify(files));
        phs.push(event.target.files[0]);
        setFiles(phs);
    }

    const renderPhotos = () => {
        let array = [];
        for (let i = 0; i < photos.length; i++) {
            array.push(<div style={{
                position: 'relative',
            }}>
                <Avatar src={photos[i]} variant="rounded" sx={{ width: 100, height: 100 }} />
                <DeleteIcon onClick={() => {
                    let phs = JSON.parse(JSON.stringify(photos));
                    phs.splice(i, 1);
                    setPhotos(phs);
                    phs = JSON.parse(JSON.stringify(files));
                    phs.splice(i, 1);
                    setFiles(phs);
                }} style={{
                    position: 'absolute',
                    top: 5,
                    right: 5,
                }} />
            </div>);
        }
        return (<Stack direction="row" spacing={1}>
            {array}
        </Stack>);
    }

  return (<div>
    <Helmet>
      <title>Мероприятия</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
      <Container maxWidth={false}>
        <Card>
            <CardContent>
                <EventsTable
                    customers={events}
                    onEdit={obj => {
                        setEventId(obj.id);
						setDevPlace(obj.dev_place);
						setTaskDate(obj.task_date);
						setTaskText(obj.task_text);
						setStatus(obj.status);
						setExecText('');
                        setShowModal(true);
                        // setName(staff.name);
                    }}
                />
            </CardContent>
        </Card>
      </Container>
    </Box>
    <Dialog
        open={showModal}
        onClose={(event, reason) => {
            if (reason !== 'backdropClick') {
                
            }
        }}
        fullWidth
        maxWidth="sm"
    >
        <DialogTitle>
            <Typography variant="h4">
                Редактирование мероприятия
            </Typography>
            <IconButton
                aria-label="close"
                onClick={() => {
                    setShowModal(false);
                }}
                sx={{
                    position: 'absolute',
                    right: 8,
                    top: 8,
                    color: (theme) => theme.palette.grey[500],
                }}
                >
                <CloseIcon />
            </IconButton>
        </DialogTitle>
        <DialogContent>
		<FormControl fullWidth sx={{
                m: 0,
                marginTop: 2,
                marginBottom: 1,
            }}>
                <TextField
                    id="email2"
                    label="Место"
                    variant="outlined"
                    value={devPlace}
                    size="small"
                    disabled
                />
            </FormControl>
            <FormControl fullWidth sx={{
                m: 0,
                // marginTop: 2,
                marginBottom: 1,
            }}>
                <TextField
                    id="patrname2"
                    label="Задача мероприятия"
                    variant="outlined"
                    value={taskText}
                    size="small"
					multiline
					rows={4}
					disabled
                />
            </FormControl>
			<FormControl fullWidth sx={{
				m: 0,
				// marginTop: 2,
				marginBottom: 1,
			}}>
				<LocalizationProvider dateAdapter={AdapterDateFns} locale={ruLocale}>
				<DatePicker
					label="Конечная дата"
					value={taskDate}
					size='small'
					minDate={new Date()}
					mask={'__.__.____'}
					disabled
					renderInput={(params) => <TextField size='small' {...params} />}
				/>
				</LocalizationProvider>
			</FormControl>
			{status === 'working' ? (<FormControl fullWidth sx={{
                m: 0,
                // marginTop: 2,
                marginBottom: 1,
            }}>
                <TextField
                    id="patrname2"
                    label="Комментарий к задаче"
                    variant="outlined"
                    value={execText}
                    size="small"
					multiline
					rows={4}
					onChange={event => {
                        setExecText(event.target.value);
                    }}
                />
            </FormControl>) : null}
            {status === 'working' ? (<label htmlFor="icon-button-file">
                <Input onChange={e => onchange(e)} accept="image/*" id="icon-button-file" type="file" />
                <IconButton color="primary" aria-label="upload picture" component="span">
                <PhotoCamera />
                </IconButton>
            </label>) : null}
            {status === 'working' ? photoBody : null}
        </DialogContent>
        <DialogActions>
            {status === 'started' ? (<Button variant="contained" color="primary" onClick={() => {
				execEvent(eventId)
				.then(() => {
					getMyEvents()
					.then(staff => {
						setEvents(staff);
						setShowModal(false);
					})
					.catch(err => {
						swal(Config.appName, err, "warning");
					});
				})
				.catch(err => {
					swal(Config.appName, err, "warning");
				});
            }}>
                Приступить к выполнению
            </Button>) : (<Button variant="contained" color="primary" onClick={() => {
                if (execText.length) {
                    endEvent(eventId, execText, files)
                    .then(() => {
                        getMyEvents()
                        .then(staff => {
                            setEvents(staff);
							setShowModal(false);
                        })
                        .catch(err => {
                            swal(Config.appName, err, "warning");
                        });
                    })
                    .catch(err => {
                        swal(Config.appName, err, "warning");
                    });
                } else {
                    swal(Config.appName, 'Укажите комментарий!', "warning");
                }
            }}>
                Завершить
            </Button>)}
        </DialogActions>
    </Dialog>
  </div>);
};

export default Events;
