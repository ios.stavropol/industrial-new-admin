import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  Container,
  OutlinedInput,
  MenuItem,
  FormControl,
  TextField,
  Select,
  Autocomplete
} from '@material-ui/core';
import Network, { getAudit, setReady, getCompanyUsers, getCompanyProcessForAudit, getAllCompanyUnits, getCompanyUnitsList, saveAudit } from '../../helpers/Network';
import Config from '../../constants/Config';
import swal from 'sweetalert';
import { Link as RouterLink, useLocation, useNavigate, useParams } from 'react-router-dom';
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import DatePicker from '@material-ui/lab/DatePicker';
import ruLocale from 'date-fns/locale/ru';
import { useTheme } from '@material-ui/core/styles';
import UsersTable from './../../components/AuditInfo/UsersTable';
import UsersFilterTable from './../../components/AuditInfo/UsersFilterTable';
import UserFilter from './../../components/AuditInfo/UserFilter';
import Helper from './../../helpers/Helper';

const statusOptions = [
    {value: '', label: 'Не создан'},
    {value: 'draft', label: 'Черновик'},
    {value: 'ready', label: 'Запланирован'},
    {value: 'working', label: 'В работе'},
    {value: 'ended', label: 'Завершен'},
];

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
    //   width: '100%',
    },
  },
};

const AuditInfo = ({ onMobileClose, openMobile }) => {

    const location = useLocation();
    const navigate = useNavigate();
    const theme = useTheme();
    let {id} = useParams();
    const [processes, setProcesses] = React.useState([]);
    const [processOption, setProcessOption] = React.useState(null);
    const [audit, setAudit] = React.useState(null);
    const [status, setStatus] = React.useState('Не создан');
    const [target, setTarget] = React.useState('');
    const [selectedDate, setSelectedDate] = React.useState(null);
    const [selectedProcess, setSelectedProcess] = React.useState(null);
    const [users, setUsers] = React.useState([]);
    const [usersAll, setUsersAll] = React.useState([]);
    const [usersFiltered, setUsersFiltered] = React.useState([]);
    const [units, setUnits] = React.useState([]);
    const [unitsTree, setUnitsTree] = React.useState([]);
    const [selectedUser, setSelectedUser] = React.useState([]);

    const [searchText, setSearchText] = React.useState('');
    const [categoryText, setCategoryText] = React.useState('');
    const [searchUnits, setSearchUnits] = React.useState([]);

    const handleDateChange = (date) => {
        setSelectedDate(date);
    };

    const filterUser = () => {
        let array = usersAll;
        if (searchText !== null && searchText.length) {
            array = array.filter(a => {
                return a.fio.toLowerCase().indexOf(searchText.toLowerCase()) !== -1;
            });
        }

        if (categoryText !== null && categoryText.length) {
            array = array.filter(a => {
                return a.roles.includes(categoryText);
            });
        }

        if (searchUnits.length) {
            array = array.filter(a => {
                return searchUnits.includes(a.unit_id);
            });
        }
        
        // console.warn(JSON.stringify(array));
        setUsersFiltered(array);
    }

    const getAllData = () => {
        getCompanyUnitsList(Network.userProfile.company_id)
        .then(unitList => {
            setUnits(unitList);
        })
        .catch(err => {

        });

        getCompanyUsers(Network.userProfile.company_id)
        .then(users2 => {
            if (id != 0) {
                let array = [];
                for (let i = 0; i < audit.users.length; i++) {
                    for (let y = 0; y < users2.length; y++) {
                        if (audit.users[i] === users2[y].id) {
                            array.push(users2[y]);
                            break;
                        }
                    }
                }
                setUsers(array);
                setUsersFiltered(users2);
                setUsersAll(users2);
            } else {
                setUsers(users2);
                setUsersFiltered(users2);
                setUsersAll(users2);
            }
        })
        .catch(err => {

        });

        getAllCompanyUnits(Network.userProfile.company_id)
        .then(units => {
            setUnitsTree(units);
        })
        .catch(err => {

        });

        getCompanyProcessForAudit(Network.userProfile.company_id)
        .then(process => {
            setProcesses(process);
            setProcessOption(process);
        })
        .catch(err => {

        });
    }

    useEffect(() => {
        Network.selectedUnit = [];
        if (id === 0) {
            setStatus('Не создан');
            setAudit({
                id: 0,
                users: [],
                process_id: 0,
            });
        } else {
            getAudit(id)
            .then(audit2 => {
                setSelectedUser(audit2.users);
                setAudit(audit2);
                setTarget(audit2.target);
                setSelectedDate(new Date(audit2.date));
                for (let i = 0; i < statusOptions.length; i++) {
                    if (statusOptions[i].value === audit2.status) {
                        setStatus(statusOptions[i].label);
                        break;
                    }
                }
            })
            .catch(err => {

            });
        }
    }, [location.pathname]);

    useEffect(() => {
        getAllData();
    }, [audit]);

    useEffect(() => {
        if (audit !== null) {
            for (let i = 0; i < processes.length; i++) {
                if (processes[i].id === audit.process_id) {
                    setSelectedProcess(processes[i]);
                    break;
                }
            }
        }
    }, [processes]);

    useEffect(() => {
        filterUser();
    }, [searchText, categoryText, searchUnits]);

  return (<div>
    <Helmet>
      <title>Аудит</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
        <Container maxWidth={false}>
            <Card>
                <CardContent>
                    <FormControl fullWidth sx={{
                        m: 0,
                        // marginTop: 2,
                        marginBottom: 1,
                    }}>
                        <TextField
                            id="name"
                            size='small'
                            label="Статус"
                            variant="outlined"
                            value={status}
                            disabled
                        />
                    </FormControl>
                    <FormControl fullWidth sx={{
                        m: 0,
                        // marginTop: 2,
                        marginBottom: 1,
                    }}>
                        <TextField
                            id="target"
                            size='small'
                            label="Цель"
                            variant="outlined"
                            value={target}
                            disabled={id == 0 || status === 'Черновик' ? false : true}
                            onChange={event => {
                                setTarget(event.target.value);
                            }}
                        />
                    </FormControl>
                    <FormControl fullWidth sx={{
                        m: 0,
                        // marginTop: 2,
                        marginBottom: 1,
                    }}>
                        <LocalizationProvider dateAdapter={AdapterDateFns} locale={ruLocale}>
                        <DatePicker
                            label="Конечная дата"
                            value={selectedDate}
                            size='small'
                            minDate={new Date()}
                            mask={'__.__.____'}
                            disabled={id == 0 || status === 'Черновик' ? false : true}
                            onChange={(newValue) => {
                                setSelectedDate(newValue);
                            }}
                            renderInput={(params) => <TextField size='small' {...params} />}
                        />
                        </LocalizationProvider>
                    </FormControl>
                    <FormControl sx={{
                        m: 0,
                        width: '100%',
                        // mt: 1,
                        mb: 1,
                    }}>
                        <Autocomplete
                            value={selectedProcess}
                            size="small"
                            onChange={(event, newValue) => {
                                // if (newValue && newValue.id === 0) {
                                //     setStaff({
                                //         id: newValue.id,
                                //         name: newValue.raw,
                                //     });
                                //     // setIsNew(true);
                                // } else {
                                //     // render2Answer(newValue);
                                setSelectedProcess(newValue);
                                    // setIsNew(false);
                                // }
                            }}
                            filterOptions={(options, params) => {
                                let array = options;
                                if (params.inputValue !== null) {
                                    array = options.filter(a => {
                                        return a.name.toLowerCase().indexOf(params.inputValue.toLowerCase()) !== -1;
                                    });
                                }
                                // for (let i=0;i<array.length;i++) {
                                //     filtered.push(array[i]);
                                // }
                                // // console.warn(JSON.stringify(filtered));
                                return array;
                            }}
                            selectOnFocus
                            clearOnBlur
                            handleHomeEndKeys
                            disabled={id == 0 || status === 'Черновик' ? false : true}
                            id="free-solo-with-text-demo"
                            options={processOption}
                            getOptionLabel={(option) => {
                                if (typeof option === 'string') {
                                    return option;
                                }
                                return option.name;
                            }}
                            style={{
                                zIndex: 1000,
                            }}
                            freeSolo
                            renderInput={(params) => (
                                <TextField {...params} label="Бизнес-процесс" variant="outlined" />
                            )}
                        />
                    </FormControl>
                    {id != 0 && status !== 'Черновик' ? (<UsersTable
                        customers={users}
                        units={units}
                    />) : (<div>
                        <UserFilter
                            searchText={searchText}
                            categoryText={categoryText}
                            unitsList={unitsTree}
                            unitsRawList={units}
                            onSearch={text => {
                                setSearchText(text);
                            }}
                            onCategorySelect={cat => {
                                setCategoryText(cat);
                            }}
                            onUnitSelect={units => {
                                setSearchUnits(units);
                            }}
                        />
                        <UsersFilterTable
                            customers={usersFiltered}
                            units={units}
                            selectedUsers={selectedUser}
                            onSelectUsers={selected => {
                                setSelectedUser(selected);
                            }}
                        />
                        <Box display="flex" flexDirection="row" justifyContent="space-between" p={1} m={1}>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={() => {
                                    // console.warn(selectedDate);
                                    // return;
                                    if (target !== null && target.length && selectedDate !== null && selectedProcess !== null && selectedUser.length) {
                                        saveAudit(id, target, Helper.getDateFromJSDateTime(selectedDate), selectedProcess.id, selectedUser)
                                        .then(() => {
                                            swal(Config.appName, 'Изменения сохранены!', "warning");
                                            navigate('/company/audits', { replace: true });
                                        })
                                        .catch(err => {
                                            swal(Config.appName, err, "warning");
                                        });
                                    } else {
                                        swal(Config.appName, 'Заполните поля и выберите сотрудников!', "warning");
                                    }
                                }}
                                style={{
                                    marginTop: 10,
                                }}
                            >
                                Сохранить
                            </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={() => {
                                    if (id == 0) {
                                        saveAudit(id, target, Helper.getDateFromJSDateTime(selectedDate), selectedProcess.id, selectedUser, true)
                                        .then(() => {
                                            swal(Config.appName, 'Аудит опубликован!', "success");
                                            navigate('/company/audits', { replace: true });
                                        })
                                        .catch(err => {
                                            swal(Config.appName, err, "warning");
                                        });
                                    } else {
                                        setReady(id)
                                        .then(() => {
                                            swal(Config.appName, 'Аудит опубликован!', "success");
                                            navigate('/company/audits', { replace: true });
                                        })
                                        .catch(err => {
                                            swal(Config.appName, err, "warning");
                                        });
                                    }
                                }}
                                style={{
                                    marginTop: 10,
                                }}
                            >
                                Опубликовать
                            </Button>
                        </Box>
                    </div>)}
                </CardContent>
            </Card>
        </Container>
    </Box>
    </div>);
};

export default AuditInfo;
