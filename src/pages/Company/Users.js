import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  Container,
  Typography,
  Modal,
  FormControl,
  CardHeader,
} from '@material-ui/core';
import Network, { setUserStatus, getCompanyUsers, updateCompanyUser, getCompanyStaff, getCompanyUnitsList, getAllCompanyUnits, deleteCompanyUser } from '../../helpers/Network';
import UserEditModalView from './../../components/Company/UserEditModalView';
import UnitSelectModalView from './../../components/Company/UnitSelectModalView';
import { Link as RouterLink, useLocation } from 'react-router-dom';
import UsersTable from './../../components/Users/UsersTable';
import UserFilter from './../../components/AuditInfo/UserFilter';
import swal from 'sweetalert';
import Config from '../../constants/Config';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    // border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const Users = ({ onMobileClose, openMobile }) => {

    const location = useLocation();

    const [users, setUsers] = React.useState([]);
    const [usersAll, setUsersAll] = React.useState([]);
    const [usersFiltered, setUsersFiltered] = React.useState([]);
    const [staff, setStaff] = React.useState([]);
    const [units, setUnits] = React.useState([]);
    const [unitRaw, setUnitRaw] = React.useState([]);
    const [selectedUser, setSelectedUser] = React.useState(null);
    const [showModal, setShowModal] = React.useState(false);
    const [showUnitModal, setShowUnitModal] = React.useState(false);

    const [searchText, setSearchText] = React.useState('');
    const [categoryText, setCategoryText] = React.useState('');
    const [searchUnits, setSearchUnits] = React.useState([]);

    const filterUser = () => {
        let array = usersAll;
        if (searchText !== null && searchText.length) {
            array = array.filter(a => {
                return a.fio.toLowerCase().indexOf(searchText.toLowerCase()) !== -1;
            });
        }

        if (categoryText !== null && categoryText.length) {
            array = array.filter(a => {
                return a.roles.includes(categoryText);
            });
        }

        if (searchUnits.length) {
            array = array.filter(a => {
                return searchUnits.includes(a.unit_id);
            });
        }
        
        // console.warn(JSON.stringify(array));
        setUsersFiltered(array);
    }

    useEffect(() => {
        filterUser();
    }, [searchText, categoryText, searchUnits]);

  useEffect(() => {
    getCompanyUsers(Network.userProfile.company_id)
    .then(users => {
        setUsers(users);
        setUsersFiltered(users);
        setUsersAll(users);
    })
    .catch(err => {

    });
    getCompanyStaff(Network.userProfile.company_id)
    .then(staff => {
        setStaff(staff);
    })
    .catch(err => {

    });
    getAllCompanyUnits(Network.userProfile.company_id)
    .then(units => {
        setUnits(units);
    })
    .catch(err => {

    });
    getCompanyUnitsList(Network.userProfile.company_id)
    .then(units => {
        setUnitRaw(units);
    })
    .catch(err => {

    });
  }, [location.pathname]);

  return (<div>
    <Helmet>
      <title>Сотрудники</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
      <Container maxWidth={false}>
        <Card>
            <CardHeader
                action={
                    <Button variant="contained" color="primary" onClick={() => {
                        setSelectedUser({
                            id: 0,
                            avatar: null,
                            firstname: null,
                            lastname: null,
                            patrname: null,
                            roles: [],
                            staff_id: null,
                            status: 10,
                            unit_id: null,
                            company_id: Network.userProfile.company_id,
                            email: null,
                        });
                        setShowModal(true);
                    }}>
                        Добавить сотрудника
                    </Button>
                }
            />
            <CardContent>
                <UserFilter
                    searchText={searchText}
                    categoryText={categoryText}
                    unitsList={units}
                    unitsRawList={unitRaw}
                    onSearch={text => {
                        setSearchText(text);
                    }}
                    onCategorySelect={cat => {
                        setCategoryText(cat);
                    }}
                    onUnitSelect={units => {
                        setSearchUnits(units);
                    }}
                />
                <UsersTable
                    customers={usersFiltered}
                    unitList={unitRaw}
                    onStatusChanged={customer => {
                        let status;
                        if (customer.status === 10) {
                            status = 9;
                        } else {
                            status = 10;
                        }

                        setUserStatus(customer.id, status)
                        .then(() => {
                            getCompanyUsers(Network.userProfile.company_id)
                            .then(users => {
                                setUsers(users);
                                setUsersAll(users);
                                filterUser();
                            })
                            .catch(err => {
                                
                            });
                        })
                        .catch(err => {
                            swal(Config.appName, err, "warning");
                        });
                    }}
                    onEditUser={customer => {
                        setSelectedUser(customer);
                        setShowModal(true);
                    }}
                    onDeleteUser={customer => {
                        swal({
                            title: Config.appName,
                            text: 'Вы хотите удалить сотрудника?',
                            icon: "warning",
                            buttons: ['Нет', 'Да'],
                            dangerMode: true,
                        })
                        .then((willDelete) => {
                            if (willDelete) {
                                deleteCompanyUser(customer.id)
                                .then(() => {
                                    getCompanyUsers(Network.userProfile.company_id)
                                    .then(users => {
                                        setUsers(users);
                                        setUsersAll(users);
                                        filterUser();
                                    })
                                    .catch(err => {
                                        
                                    });
                                })
                                .catch(err => {
                                    swal(Config.appName, err, "warning");
                                });
                            }
                        });
                    }}
                />
            </CardContent>
            <UserEditModalView
                open={showModal}
                staffList={staff}
                unitList={unitRaw}
                currentUser={selectedUser}
                onOpenUnit={() => {
                    setShowUnitModal(true);
                }}
                onSuccess={() => {
                    setShowModal(false);
                    getCompanyUsers(Network.userProfile.company_id)
                    .then(users => {
                    setUsers(users);
                    })
                    .catch(err => {

                    });
                    getCompanyStaff(Network.userProfile.company_id)
                    .then(staff => {
                        setStaff(staff);
                    })
                    .catch(err => {

                    });
                }}
                onClose={() => {
                    setShowModal(false);
                }}
            />
            <UnitSelectModalView
                open={showUnitModal}
                unitList={units}
                onSelectUnit={(unitId, unitText) => {
                    let user = JSON.parse(JSON.stringify(selectedUser));
                    user.unit_id = unitId;
                    setSelectedUser(user);
                    setShowUnitModal(false);
                }}
            />
        </Card>
      </Container>
    </Box>
  </div>);
};

export default Users;
