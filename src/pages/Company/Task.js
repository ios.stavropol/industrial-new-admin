import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { makeStyles } from '@material-ui/styles';
import {
  Box,
  Button,
  Card,
  CardContent,
  Container,
  Stepper,
  Step,
  StepLabel,
  StepContent,
  Radio,
  Checkbox,
  TextField,
  Chip,
  Paper,
  Stack,
} from '@material-ui/core';
import Network, { getTask, answerTask, endTask } from '../../helpers/Network';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Link as RouterLink, useLocation, useParams, useNavigate } from 'react-router-dom';
import swal from 'sweetalert';
import Config from '../../constants/Config';
import DeviationModalView from './../../components/Task/DeviationModalView';
import { styled } from '@material-ui/styles';

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
    },
    button: {
      marginTop: 20,//theme.spacing(1),
      marginRight: 20,//theme.spacing(1),
    },
    actionsContainer: {
      marginBottom: 2,//theme.spacing(2),
    },
    resetContainer: {
      padding: 30,//theme.spacing(3),
    },
  }));

const Task = ({ onMobileClose, openMobile }) => {

    const location = useLocation();
    const navigate = useNavigate();

    let { id } = useParams();
    const [task, setTask] = React.useState(null);
    const [currentQuestion, setCurrentQuestion] = React.useState(null);
    const [deviation, setDeviation] = React.useState(null);
    const [currentDeviation, setCurrentDeviation] = React.useState([]);
    const [selected, setSelected] = React.useState([]);
    const [comment, setComment] = React.useState('');
    const [open, setOpen] = React.useState(false);
    const [showModal, setShowModal] = React.useState(false);
    const [direction, setDirection] = React.useState(null);
    const [body, setBody] = React.useState(null);

    const classes = useStyles();
    const [activeStep, setActiveStep] = React.useState(-1);

    let count = 0;
    let tree = [];
    
    const handleDelete = (chipToDelete) => () => {
        setCurrentDeviation((chips) => chips.filter((chip) => chip.id !== chipToDelete.id));
    };

    const handleChipClick = clickedChip => () => {
        console.warn(clickedChip);
        setDeviation(clickedChip);
        setShowModal(true);
        // window.alert(clickedChip);
    }

    const handleNext = () => {
        if (selected !== null && selected.length) {
            console.warn(currentQuestion);
            // return;
            if (currentQuestion.fields.has_deviation) {
                let score = null;
                for (let y = 0; y < currentQuestion.fields.answers.length; y++) {
                    if (selected.includes(currentQuestion.fields.answers[y].id)) {
                        score = currentQuestion.fields.answers[y].price;
                    }
                }
                if (score == 0) {
                    if (!currentDeviation || !currentDeviation.length) {
                        swal(Config.appName, 'Укажите отклонение!', "warning");
                        return;
                    }
                }
            }
            let answers = task.answers;
            if (answers === null) {
                answers = [{
                    id: currentQuestion.id,
                    checklist_id: currentQuestion.checklist_id,
                    question: currentQuestion.question,
                    fields: currentQuestion.fields,
                    selected: selected,
                    comment: comment,
                    deviation: currentDeviation,
                }];
            } else {
                answers = JSON.parse(answers);
                let updated = false;
                for (let i = 0; i < answers.length; i++) {
                    if (currentQuestion.id === answers[i].id) {
                        console.warn(currentQuestion.id+' = '+answers[i].id);
                        updated = true;
                        answers[i] = {
                            id: currentQuestion.id,
                            checklist_id: currentQuestion.checklist_id,
                            question: currentQuestion.question,
                            fields: currentQuestion.fields,
                            selected: selected,
                            comment: comment,
                            deviation: currentDeviation,
                        };
                        break;
                    }
                }
                console.warn('updated: '+updated);
                if (!updated) {
                    answers.push({
                        id: currentQuestion.id,
                        checklist_id: currentQuestion.checklist_id,
                        question: currentQuestion.question,
                        fields: currentQuestion.fields,
                        selected: selected,
                        comment: comment,
                        deviation: currentDeviation,
                    });
                }
            }
            
            setOpen(true);
            answerTask(task.id, answers, 1)
            .then(newtask => {
                setOpen(false);
                let all = 0;
                let answs = JSON.parse(newtask.answers);
                if (answs) {
                    all = answs.length;
                }
                console.warn('tree.length: ', tree.length, ' all: ', all);
                if (tree.length === all) {
                    swal({
                        title: Config.appName,
                        text: 'Вы действительно хотите завершить аудит? После завершения аудита изменение ответов будет невозможно',
                        icon: "warning",
                        buttons: ['Нет', 'Да'],
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            endTask(task)
                            .then(() => {
                                swal(Config.appName, 'Задание выполнено!', "success");
                                navigate('/company/tasks', { replace: true });
                            })
                            .catch(err => {
                                swal(Config.appName, err, "warning");
                            });
                        } else {
                            setActiveStep((prevActiveStep) => prevActiveStep - 1);
                            setDirection('back');
                        }
                    });
                } else {
                    if (newtask !== null) {
                        setTask(newtask);
                    }
                }
            })
            .catch(err => {
                setOpen(false);
                swal(Config.appName, err, "warning");
            });
        } else {
            swal(Config.appName, 'Выберите ответ!', "warning");
        }
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
        setDirection('back');
    };

    const renderRadio = (index, data, action) => {
        return (<Box onClick={() => {
            if (action) {
                action()
            }
        }} key={index} display="flex" flexDirection="row" alignItems="center" justifyContent="space-between" p={1} m={1}>
            <Radio
                checked={selected.includes(data.id) ? true : false}
                onChange={event => {
                    
                }}
                name={"radio-" + data.id}
                inputProps={{ 'aria-label': 'A' }}
            />
            <Box sx={{
                width: '95%',
                fontFamily: 'Source Sans Pro',
            }}>
                {data.answer}
            </Box>
        </Box>);
    }

    const renderCheckbox = (index, data, action) => {
        return (<Box onClick={() => {
            if (action) {
                action()
            }
        }} key={index} display="flex" flexDirection="row" alignItems="center" justifyContent="space-between" p={1} m={1}>
            <Checkbox
                checked={selected.includes(data.id) ? true : false}
                onChange={event => {
                    
                }}
                name={"checkbox-" + data.id}
                inputProps={{ 'aria-label': 'A' }}
            />
            <Box sx={{
                width: '95%',
                fontFamily: 'Source Sans Pro',
            }}>
                {data.answer}
            </Box>
        </Box>);
    }

    const renderQuestion = (question, index) => {
        let rows = [];
        for (let i = 0; i < question.fields.answers.length; i++) {
            if (question.fields.type === 'radio') {
                rows.push(renderRadio(i, question.fields.answers[i], () => {
                    let current = selected;
                    current = [question.fields.answers[i].id];
                    setSelected(current);
                }));
            } else {
                rows.push(renderCheckbox(i, question.fields.answers[i], () => {
                    let current = JSON.parse(JSON.stringify(selected));
                    if (current.includes(question.fields.answers[i].id)) {
                        for(let y = 0; y < current.length; y++) {
                            if (current[y] === question.fields.answers[i].id) {
                                current.splice(y, 1);
                            }
                        }
                    } else {
                        current.push(question.fields.answers[i].id);
                    }
                    setSelected(current);
                    console.warn(JSON.stringify(selected));
                }));
            }
        }

        if (question.fields.has_comment) {
            rows.push(<TextField
                size='small'
                id="name"
                key={question.id}
                label="Ваш комментарий"
                variant="outlined"
                value={comment}
                style={{
                    width: '100%',
                    marginBottom: 20,
                }}
                onChange={event => {
                    setComment(event.target.value);
                }}
            />);
        }

        // console.warn('quest: ', question);
        let answers = JSON.parse(task.answers);
        let solved = 0;
        let subtitle = [];
        if (answers) {
            for (let i = 0; i < answers.length; i++) {
                if (answers[i].id == question.id) {
                    solved = 1;
                    for (let y = 0; y < answers[i].selected.length; y++) {
                        for (let z = 0; z < answers[i].fields.answers.length; z++) {
                            if (answers[i].fields.answers[z].id == answers[i].selected[y]) {
                                if (answers[i].fields.answers[z].price == 0) {
                                    solved = 2;
                                }
                                subtitle.push(<div key={i} style={{marginTop: '10px'}}>&#8226; {answers[i].fields.answers[z].answer}</div>);
                                break;
                            }
                        }
                    }
                    break;
                }
            }
        }

        let title = [];

        title.push(<h3 key={question.id}>{question.question}</h3>);
        title = title.concat(subtitle);
        return (<Step key={question.id}>
            <StepLabel StepIconComponent={() => {
                return (<div style={{
                    width: '24px',
                    height: '24px',
                    borderRadius: '12px',
                    backgroundColor: solved > 0 ? (solved === 2 ? 'red' : '#32CD32') : '#381a6c',
                    color: 'white',
                    textAlign: 'center',
                    padding: '2px',
                    fontFamily: 'Source Sans Pro',
                }}
                onClick={() => {
                    for (let i = 0; i < tree.length; i++) {
                        if (tree[i] == question.id) {
                            setActiveStep(i);
                            setCurrentQuestion(question);
                            let sel = [];
                            let dev = [];
                            let answs = JSON.parse(task.answers);
                            console.warn('answs: ', answs);
                            if (answs) {
                                for (let y = 0; y < answs.length; y++) {
                                    if (answs[y].id == question.id) {
                                        sel = answs[y].selected;
                                        dev = answs[y].deviation;
                                        break;
                                    }
                                }
                            }
                            
                            console.warn('sel: ', sel);
                            setSelected(sel);
                            setDeviation(dev);
                            setCurrentDeviation(dev);
                            break;
                        }
                    }
                }}>
                    {index}
                </div>);
            }}>{title}</StepLabel>
            <StepContent>
            {rows}
            <div className={classes.actionsContainer}>
                <div>
                    <Button
                        disabled={activeStep === 0}
                        onClick={handleBack}
                        className={classes.button}
                    >
                        Назад
                    </Button>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={handleNext}
                        className={classes.button}
                    >
                        Сохранить ответ
                    </Button>
                    {question.fields.has_deviation ? (<Button
                        style={{
                            marginLeft: '10px',
                        }}
                        color="primary"
                        variant="contained"
                        onClick={() => {
                            if (selected !== null && selected.length) {
                                for (let i = 0; i < currentQuestion.fields.answers.length; i++) {
                                    if (currentQuestion.fields.answers[i].id === selected[0]) {
                                        setDeviation({
                                            id: 0,
                                            text: '',
                                            place: '',
                                            description: currentQuestion.fields.answers[i].deviation,
                                        });
                                        setShowModal(true);
                                        break;
                                    }
                                }
                            } else {
                                swal(Config.appName, 'Выберите ответ!', "warning");
                            }
                        }}
                        className={classes.button}
                    >
                        Добавить отклонение
                    </Button>) : null}
                    <Stack direction="row" spacing={1} sx={{mt: 2}}>
                        {currentDeviation.map((data) => {
                            return (
                                <Chip
                                    key={data.id}
                                    color="primary"
                                    label={data.text}
                                    onDelete={handleDelete(data)}
                                    onClick={handleChipClick(data)}
                                />
                            );
                        })}
                    </Stack>
                </div>
            </div>
            </StepContent>
        </Step>);
    }

    useEffect(() => {
        setOpen(true);
        getTask(id)
        .then(task2 => {
            console.warn(task2);
            setTask(task2);
            setOpen(false);
        })
        .catch(err => {
            setOpen(false);
        });
    }, [location.pathname]);

    useEffect(() => {
        if (task) {

            bodyFun();
            openQuestion();
        }
    }, [task]);

    useEffect(() => {
        if (task) {
            bodyFun();
        }
    }, [selected, currentDeviation, deviation]);

    useEffect(() => {
        console.warn('activeStep: ', activeStep);
        if (task) {
            
            bodyFun();
            
            for (let i = 0; i < task.questions.length; i++) {
                if (task.questions[i].id == tree[activeStep]) {
                    let qq = task.questions[i];
                    console.warn('task.questions[activeStep]: ', qq);
                    setCurrentQuestion(qq);
                    if (task.answers !== null) {
                        let answers = JSON.parse(task.answers);
                        if (answers !== null) {
                            let sel = [];
                            let com = '';
                            let dev = [];
                            for (let y = 0; y < answers.length; y++) {
                                if (answers[y].id == qq.id) {
                                    console.warn('answers[y]: ', answers[y]);
                                    sel = answers[y].selected;
                                    dev = answers[y].deviation;
                                    com = answers[y].comment;
                                    break;
                                }
                            }
                            setSelected(sel);
                            setComment(com);
                            setCurrentDeviation(dev);
                        } else {
                            setSelected([]);
                            setComment('');
                            setCurrentDeviation([]);
                        }
                    } else {
                        setSelected([]);
                        setComment('');
                        setCurrentDeviation([]);
                    }
                    break;
                }
            }
        }
    }, [activeStep]);

    const bodyFun = () => {
        let array = task.questions.map((question, index) => {

            if (index === 0) {
                count = 0;
                tree = [];
            }
            if (question.prev_id !== null) {
                let answs = JSON.parse(task.answers);
                if (answs !== null) {
                    for (let i = 0; i < answs.length; i++) {
                        if (answs[i].id == question.prev_id) {
                            if (answs[i].selected.includes(question.answer_id)) {
                                count++;
                                tree.push(question.id);
                                return (renderQuestion(question, count));
                            }
                        }
                    }
                }
                return null;
            } else {
                count++;
                tree.push(question.id);
                return (renderQuestion(question, count));
            }
        });
        setBody(array);
    }

    const openQuestion = () => {
        if (task) {
            let answs = JSON.parse(task.answers);
            if (answs) {
                answs = answs.sort((a, b) => {
                    return a.id - b.id;
                });
                let array = [];
                for (let i = 0; i < answs.length; i++) {
                    array.push(answs[i].id);
                }
                for (let i = 0; i < tree.length; i++) {
                    if (!array.includes(tree[i])) {
                        console.warn('openQuestion: ', i);
                        setActiveStep(i);
                        break;
                    }
                }
            } else {
                setActiveStep(0);
            }
        } else {
            setActiveStep(0);
        }
    }

  return (<div>
    <Helmet>
      <title>Задача</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
        <Container maxWidth={false}>
            <Card>
                <CardContent>
                    <h2 style={{
                        fontFamily: 'Source Sans Pro',
                    }}>{task !== null ? (task.process + ' - ' + task.checklist) : ''}</h2>
                    <Stepper style={{marginTop: '10px'}} activeStep={activeStep} orientation="vertical">
                        {body}
                    </Stepper>
                </CardContent>
            </Card>
        </Container>
    </Box>
    <DeviationModalView
        open={showModal}
        deviation={deviation}
        onClose={() => {
            setShowModal(false);
        }}
        onSave={data => {
            let current = JSON.parse(JSON.stringify(currentDeviation));
            let exist = false;
            for (let i = 0; i < current.length; i++) {
                if (current[i].id == data.id) {
                    current[i] = data;
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                current.push(data);
            }
            
            setCurrentDeviation(current);
            setShowModal(false);
        }}
    />
    <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={open}
        onClick={() => {

        }}
    >
        <CircularProgress color="inherit" />
    </Backdrop>
    </div>);
};

export default Task;
