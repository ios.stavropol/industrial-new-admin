import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  Container,
  Typography,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Modal,
  FormControl,
  TextField,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem, { useTreeItem } from '@material-ui/lab/TreeItem';
import clsx from 'clsx';
import Network, { getAllCompanyUnits, createUnit, getCompanyUnits, deleteCompanyUnit } from '../../helpers/Network';
import Config from '../../constants/Config';
import swal from 'sweetalert';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Link as RouterLink, useLocation } from 'react-router-dom';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    // border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const CustomContent = React.forwardRef(function CustomContent(props, ref) {
    const {
      classes,
      className,
      label,
      nodeId,
      icon: iconProp,
      expansionIcon,
      displayIcon,
      children,
    } = props;
  
    const {
      disabled,
      expanded,
      selected,
      focused,
      handleExpansion,
      handleSelection,
      preventSelection,
    } = useTreeItem(nodeId);
  
    const icon = iconProp || expansionIcon || displayIcon;
  
    const handleMouseDown = (event) => {
      preventSelection(event);
    };
  
    const handleExpansionClick = (event) => {
      handleExpansion(event);
    };
  
    const handleSelectionClick = (event) => {
      handleSelection(event);
    };

    const deleteButton = (children) => {

        if (children !== undefined && children.children !== undefined && children.children.length) {
            return null;
        }
        return (<IconButton onClick={() => {
            let data = JSON.parse(nodeId);
            if (Network.onUnitDelete) {
                Network.onUnitDelete(data);
            }
        }}>
            <DeleteIcon />
        </IconButton>);
    }
  
    return (
      // eslint-disable-next-line jsx-a11y/no-static-element-interactions
      <div
        className={clsx(className, classes.root, {
          [classes.expanded]: expanded,
          [classes.selected]: selected,
          [classes.focused]: focused,
          [classes.disabled]: disabled,
        })}
        onMouseDown={handleMouseDown}
        ref={ref}
      >
        {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
        <div onClick={handleExpansionClick} className={classes.iconContainer}>
          {icon}
        </div>
        <Typography
          onClick={handleSelectionClick}
          component="div"
          className={classes.label}
        >
          {label}
        </Typography>
        <IconButton onClick={() => {
            let data = JSON.parse(nodeId);
            if (Network.onUnitEdit) {
                Network.onUnitEdit({
                    id: 0,
                    name: '',
                    parent_id: data.id,
                });
            }
        }}>
            <AddIcon />
        </IconButton>
        <IconButton onClick={() => {
            let data = JSON.parse(nodeId);
            if (Network.onUnitEdit) {
                Network.onUnitEdit(data);
            }
        }}>
            <EditIcon />
        </IconButton>
        {deleteButton(JSON.parse(nodeId))}
      </div>
    );
  });
  
  CustomContent.propTypes = {
    /**
     * Override or extend the styles applied to the component.
     */
    classes: PropTypes.object.isRequired,
    /**
     * className applied to the root element.
     */
    className: PropTypes.string,
    /**
     * The icon to display next to the tree node's label. Either a parent or end icon.
     */
    displayIcon: PropTypes.node,
    /**
     * The icon to display next to the tree node's label. Either an expansion or collapse icon.
     */
    expansionIcon: PropTypes.node,
    /**
     * The icon to display next to the tree node's label.
     */
    icon: PropTypes.node,
    /**
     * The tree node label.
     */
    label: PropTypes.node,
    /**
     * The id of the node.
     */
    nodeId: PropTypes.string.isRequired,
    /**
     * The data of the node.
     */
     onEdit: PropTypes.func.isRequired,
  };
  
  const CustomTreeItem = (props) => {
      return (
        <TreeItem ContentComponent={CustomContent} {...props} />
        )
    };

const Units = ({ onMobileClose, openMobile }) => {

    const location = useLocation();
    const [tree, setTree] = React.useState([]);
    const [units, setUnits] = React.useState([]);
    const [open, setOpen] = React.useState(false);
    const [showModal, setShowModal] = React.useState(false);
    const [name, setName] = React.useState('');
    const [parent_id, setParentId] = React.useState(0);
    const [selected_id, setSelectedId] = React.useState(0);

    const getChilds = (item) => {

        const isChildren = item.children !== null;
        if (isChildren) {
            return (
                <CustomTreeItem key={item.id} nodeId={JSON.stringify(item)} label={item.text} onEdit={() => {
                    
                }}>
                    {item.children.map((node, idx) => getChilds(node))}
                </CustomTreeItem>
            );
        }
        return <CustomTreeItem key={item.id} nodeId={JSON.stringify(item)} label={item.text}  onEdit={() => {
            
        }}/>;
    }

    const onUnitEdit = data => {
        setName(data.text);
        setParentId(data.parent_id);
        setSelectedId(data.id);
        setShowModal(true);
    }

    const onUnitDelete = data => {
        swal({
            title: Config.appName,
            text: 'Вы хотите удалить подразделение?',
            icon: "warning",
            buttons: ['Нет', 'Да'],
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                deleteCompanyUnit(data.id, Network.userProfile.company_id)
                .then(() => {
                    getAllCompanyUnits(Network.userProfile.company_id)
                    .then(units => {
                        setUnits(units);
                        let array = [];
                        for (let i = 0; i < units.length; i++) {
                            array.push(getChilds(units[i]));
                        }
                        setTree(array);
                    })
                    .catch(err => {

                    });
                })
                .catch(err => {
                    swal(Config.appName, err, "warning");
                });
            }
        });
    }

  useEffect(() => {
    Network.onUnitEdit = onUnitEdit;
    Network.onUnitDelete = onUnitDelete;
    getAllCompanyUnits(Network.userProfile.company_id)
    .then(units => {
        setUnits(units);
        let array = [];
        for (let i = 0; i < units.length; i++) {
            array.push(getChilds(units[i]));
        }
        setTree(array);
    })
    .catch(err => {

    });
  }, [location.pathname]);

  return (<div>
    <Helmet>
      <title>Подразделения</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
      <Container maxWidth={false}>
        <Card
            sx={{ height: '100%' }}
        >
            <CardContent>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => {
                        if (Network.onUnitEdit) {
                            Network.onUnitEdit({
                                id: 0,
                                name: '',
                                parent_id: 0,
                            });
                        }
                    }}
                    style={{
                        marginBottom: 10,
                    }}
                >
                    Добавить родительское подразделение
                </Button>
                <TreeView
                    aria-label="icon expansion"
                    defaultCollapseIcon={<ExpandMoreIcon />}
                    defaultExpandIcon={<ChevronRightIcon />}
                    sx={{ height: 240, flexGrow: 1, maxWidth: '100%', overflowY: 'auto' }}
                >
                    {tree}
                </TreeView>
            </CardContent>
            <Backdrop
                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open={open}
                onClick={() => {

                }}
            >
                <CircularProgress color="inherit" />
            </Backdrop>
            <Dialog
                open={showModal}
                onClose={(event, reason) => {
                    if (reason !== 'backdropClick') {
                        
                    }
                }}
                fullWidth
                maxWidth="sm"
            >
                <DialogTitle>
                    <Typography variant="h4">
                        {selected_id !== null && selected_id === 0 ? 'Добавление подразделения' : 'Редактирование подразделения'}
                    </Typography>
                    <IconButton
                        aria-label="close"
                        onClick={() => {
                            setShowModal(false);
                        }}
                        sx={{
                            position: 'absolute',
                            right: 8,
                            top: 8,
                            color: (theme) => theme.palette.grey[500],
                        }}
                        >
                        <CloseIcon />
                    </IconButton>
                </DialogTitle>
                <DialogContent>
                    <FormControl fullWidth sx={{
                        m: 0,
                        marginTop: 2,
                    }}>
                        <TextField
                            id="name"
                            size='small'
                            label="Название"
                            variant="outlined"
                            value={name}
                            onChange={event => {
                                setName(event.target.value);
                            }}
                        />
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button variant="contained" color="primary" onClick={() => {
                        if (name !== null && name !== undefined && name.length) {
                            createUnit(selected_id, name, Network.userProfile.company_id, parent_id)
                            .then(() => {
                                getAllCompanyUnits(Network.userProfile.company_id)
                                .then(units => {
                                    setUnits(units);
                                    let array = [];
                                    for (let i = 0; i < units.length; i++) {
                                        array.push(getChilds(units[i]));
                                    }
                                    setTree(array);
                                    setShowModal(false);
                                })
                                .catch(err => {

                                });
                            })
                            .catch(err => {
                                swal(Config.appName, err, "warning");
                            });
                        } else {
                            swal(Config.appName, 'Укажите название!', "warning");
                        }
                    }}>
                        Сохранить
                    </Button>
                </DialogActions>
            </Dialog>
        </Card>
      </Container>
    </Box>
  </div>);
};

export default Units;
