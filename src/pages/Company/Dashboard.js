import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  Container,
  Tabs,
  Tab,
  Typography,
} from '@material-ui/core';
import Network from '../../helpers/Network';
import Config from '../../constants/Config';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import Statuses from './../../components/dashboard/Statuses';
import AllProcess from './../../components/dashboard/AllProcess';
import Process from './../../components/dashboard/Process';

const useStyles = makeStyles({
    root: {
      width: 500,
    },
  });

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
  };
  
  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }

const Dashboard = ({ onMobileClose, openMobile }) => {

    const location = useLocation();
    const navigate = useNavigate();
    
    const [value, setValue] = React.useState(0);
    const [process, setProcess] = React.useState(null);

    const handleChange = (event, newValue) => {
      setValue(newValue);
    };

    const classes = useStyles();

    useEffect(() => {
       
    }, [location.pathname]);

  return (<div>
    <Helmet>
      <title>Статистика</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
        <Container maxWidth={false}>
            <Card sx={{
                mt: 2,
            }}>
                <CardContent>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                            <Tab label="Статусы аудитов" {...a11yProps(0)} />
                            <Tab label="Все бизнес-процессы" {...a11yProps(1)} />
                            <Tab label="Статистика бизнес-процесса" {...a11yProps(2)} />
                        </Tabs>
                    </Box>
                    <TabPanel value={value} index={0}>
                        <Statuses />
                    </TabPanel>
                    <TabPanel value={value} index={1}>
                        <AllProcess
                          onClickProcess={id => {
                            setValue(2);
                            setProcess(id);
                          }}
                        />
                    </TabPanel>
                    <TabPanel value={value} index={2}>
                        <Process
                          process={process}
                        />
                    </TabPanel>
                </CardContent>
            </Card>
        </Container>
    </Box>
    </div>);
};

export default Dashboard;
