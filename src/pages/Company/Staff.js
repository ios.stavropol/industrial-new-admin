import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Container,
  Typography,
  Modal,
  FormControl,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@material-ui/core';
import Network, { getCompanyStaff, deleteStaff, saveStaff } from '../../helpers/Network';
import Config from '../../constants/Config';
import swal from 'sweetalert';
import { Link as RouterLink, useLocation } from 'react-router-dom';
import CompanyStaffTable from './../../components/Staff/CompanyStaffTable';

import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const Staff = ({ onMobileClose, openMobile }) => {

    const location = useLocation();
    const [companies, setCompanies] = React.useState([]);
    const [staff, setStaff] = React.useState([]);
    const [selectedStaff, setSelectedStaff] = React.useState(null);
    const [showModal, setShowModal] = React.useState(false);
    const [name, setName] = React.useState('');

    useEffect(() => {
            getCompanyStaff(Network.userProfile.company_id)
            .then(staff => {
                setCompanies(companies);
                setStaff(staff);
            })
            .catch(err => {

            });
    }, [location.pathname]);

  return (<div>
    <Helmet>
      <title>Подразделения</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
      <Container maxWidth={false}>
        <Card>
            <CardHeader
                action={
                    <Button variant="contained" color="primary" onClick={() => {
                        setSelectedStaff({
                            id: 0,
                            name: '',
                            company_id: Network.userProfile.company_id,
                        });
                        setShowModal(true);
                        setName('');
                    }}>
                        Добавить должность
                    </Button>
                }
            />
            <CardContent>
                <CompanyStaffTable
                    customers={staff}
                    companies={companies}
                    onEditStaff={staff => {
                        setSelectedStaff(staff);
                        setShowModal(true);
                        setName(staff.name);
                    }}
                    onDeleteStaff={staff => {
                        swal({
                            title: Config.appName,
                            text: 'Вы хотите удалить должность?',
                            icon: "warning",
                            buttons: ['Нет', 'Да'],
                            dangerMode: true,
                        })
                        .then((willDelete) => {
                            if (willDelete) {
                                deleteStaff(staff.id)
                                .then(() => {
                                    getCompanyStaff(Network.userProfile.company_id)
                                    .then(staff => {
                                        setStaff(staff);
                                    })
                                    .catch(err => {
                                        swal(Config.appName, err, "warning");
                                    });
                                })
                                .catch(err => {
                                    swal(Config.appName, err, "warning");
                                });
                            }
                        });
                    }}
                />
            </CardContent>
        </Card>
      </Container>
    </Box>
    <Dialog
        open={showModal}
        onClose={(event, reason) => {
            if (reason !== 'backdropClick') {
                
            }
        }}
        fullWidth
        maxWidth="sm"
    >
        <DialogTitle>
            <Typography variant="h4">
                {selectedStaff !== null && selectedStaff.id === 0 ? 'Добавление должности' : 'Редактирование должности'}
            </Typography>
            <IconButton
                aria-label="close"
                onClick={() => {
                    setShowModal(false);
                }}
                sx={{
                    position: 'absolute',
                    right: 8,
                    top: 8,
                    color: (theme) => theme.palette.grey[500],
                }}
                >
                <CloseIcon />
            </IconButton>
        </DialogTitle>
        <DialogContent>
            <FormControl fullWidth sx={{
                m: 0,
                marginTop: 2,
            }}>
                <TextField
                    id="name"
                    size='small'
                    label="Название"
                    variant="outlined"
                    value={name}
                    onChange={event => {
                        setName(event.target.value);
                    }}
                />
            </FormControl>
        </DialogContent>
        <DialogActions>
            <Button variant="contained" color="primary" onClick={() => {
                if (name.length) {
                    saveStaff(selectedStaff.id, name)
                    .then(() => {
                        getCompanyStaff(Network.userProfile.company_id)
                        .then(staff => {
                            setStaff(staff);
                            setShowModal(false);
                        })
                        .catch(err => {
                            swal(Config.appName, err, "warning");
                        });
                    })
                    .catch(err => {
                        swal(Config.appName, err, "warning");
                    });
                } else {
                    swal(Config.appName, 'Укажите название!', "warning");
                }
            }}>
                Сохранить
            </Button>
        </DialogActions>
    </Dialog>
  </div>);
};

export default Staff;
