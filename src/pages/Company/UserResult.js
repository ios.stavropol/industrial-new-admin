import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { makeStyles } from '@material-ui/styles';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  Container,
  Typography,
  Modal,
  FormControl,
  TextField,
  Stepper,
  StepLabel,
  Step,
  CardHeader,
} from '@material-ui/core';
import Network, { getUserStatistic } from '../../helpers/Network';
import Config from '../../constants/Config';
import { Link as RouterLink, useLocation, useNavigate, useParams } from 'react-router-dom';
import UsersTable from './../../components/AuditResult/UsersTable';

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
    },
    button: {
      marginTop: 20,//theme.spacing(1),
      marginRight: 20,//theme.spacing(1),
    },
    actionsContainer: {
      marginBottom: 2,//theme.spacing(2),
    },
    resetContainer: {
      padding: 30,//theme.spacing(3),
    },
}));

const UserResult = ({ onMobileClose, openMobile }) => {

    const location = useLocation();
    const navigate = useNavigate();
    let {id} = useParams();
    let {userId} = useParams();
    const [fio, setFio] = React.useState('');
    const [answersRow, setAnswersRow] = React.useState([]);
    const [answers, setAnswers] = React.useState([]);
    const [body, setBody] = React.useState(null);
    const classes = useStyles();

    useEffect(() => {
        getUserStatistic(id, userId)
        .then(statistic => {
            setAnswers(statistic.answers);
            setFio(statistic.fio);
        })
        .catch(err => {

        });
    }, [id, userId]);

    const renderQuestion = (question, index) => {
        // console.warn('quest: ', question);
        let solved = 0;
        let subtitle = [];
        if (answers) {
            for (let i = 0; i < answers.length; i++) {
                if (answers[i].id == question.id) {
                    solved = 1;
                    for (let y = 0; y < answers[i].selected.length; y++) {
                        for (let z = 0; z < answers[i].fields.answers.length; z++) {
                            if (answers[i].fields.answers[z].id == answers[i].selected[y]) {
                                if (answers[i].fields.answers[z].price == 0) {
                                    solved = 2;
                                }
                                subtitle.push(<div key={i} style={{marginTop: '10px'}}>&#8226; {answers[i].fields.answers[z].answer}</div>);
                                break;
                            }
                        }
                    }
                    break;
                }
            }
        }

        let title = [];

        title.push(<h3 key={question.id}>{question.question}</h3>);
        title = title.concat(subtitle);
        return (<Step key={question.id}>
            <StepLabel StepIconComponent={() => {
                return (<div style={{
                    width: '24px',
                    height: '24px',
                    borderRadius: '12px',
                    backgroundColor: solved > 0 ? (solved === 2 ? 'red' : '#32CD32') : '#381a6c',
                    color: 'white',
                    textAlign: 'center',
                    padding: '2px',
                    fontFamily: 'Source Sans Pro',
                }}>
                    {index}
                </div>);
            }}>{title}</StepLabel>
        </Step>);
    }

    useEffect(() => {
        if (answers) {
            let array = answers.map((question, index) => {
                return (renderQuestion(question, index + 1));
            });
            setBody(array);
        }
    }, [answers]);

  return (<div>
    <Helmet>
      <title>Результаты аудита</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
        <Container maxWidth={false}>
            <Card sx={{
                mb: 2,
            }}>
                <CardHeader title={
                    (fio)
                }/>
                <CardContent>
                    <Stepper style={{marginTop: '10px'}} activeStep={-1} orientation="vertical">
                        {body}
                    </Stepper>
                </CardContent>
            </Card>
        </Container>
    </Box>
    </div>);
};

export default UserResult;
