import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { Link as RouterLink, useLocation, useNavigate, useParams } from 'react-router-dom';
import { Box, Container, FormControl, TextField } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Network, { getCompany, saveCompany } from '../../helpers/Network';
import Config from '../../constants/Config';
import ImageUploader from './../../components/ImageUploader';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import swal from 'sweetalert';

const Company = ({ onMobileClose, openMobile }) => {

  const location = useLocation();
  const navigate = useNavigate();

  let { id } = useParams();

    const [value, setValue] = React.useState(0);
    const [company, setCompany] = React.useState(null);
    const [imagePreviewUrl, setImagePreviewUrl] = React.useState(null);
    const [imageChanged, setImageChanged] = React.useState(false);
    const [name, setName] = React.useState('');
    const [address, setAddress] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [phone, setPhone] = React.useState('');
    const [open, setOpen] = React.useState(false);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const [companies, setCompanies] = React.useState([]);

  useEffect(() => {
    if (openMobile && onMobileClose) {
      onMobileClose();
    }
    getCompany(Network.userProfile.company_id)
      .then(company => {
        setCompany(company);
        setName(company.name);
        setAddress(company.address);
        setEmail(company.email);
        setPhone(company.phone);
        setImageChanged(false);
        setImagePreviewUrl(((company.avatar !== null && company.avatar.length) ? Config.apiDomain + '/uploads/company/' + company.avatar : null));
      })
      .catch(err => {

      });
  }, [location.pathname]);

  return (<div>
    <Helmet>
      <title>Компания</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
      <Container maxWidth={false}>
        <Card
            sx={{ height: '100%' }}
        >
            <CardContent>
                <ImageUploader 
                    onChangeImage={file => {
                        setImageChanged(true);
                        setImagePreviewUrl(file.result);
                    }}
                    onDeleteImage={() => {
                        setImageChanged(true);
                        setImagePreviewUrl(null);
                    }}
                    image={imagePreviewUrl}
                />
                <FormControl fullWidth sx={{
                        m: 0,
                        marginTop: 2,
                    }}>
                    <TextField
                        id="name"
                        size='small'
                        label="Название"
                        variant="outlined"
                        value={name}
                        onChange={event => {
                            setName(event.target.value);
                        }}
                    />
                </FormControl>
                <FormControl fullWidth sx={{
                    m: 0,
                    marginTop: 2,
                }}>
                    <TextField
                        id="address"
                        size='small'
                        label="Адрес"
                        variant="outlined"
                        value={address}
                        onChange={event => {
                            setAddress(event.target.value);
                        }}
                    />
                </FormControl>
                <FormControl fullWidth sx={{
                    m: 0,
                    marginTop: 2,
                }}>
                    <TextField
                        id="email"
                        size='small'
                        label="E-mail"
                        variant="outlined"
                        value={email}
                        onChange={event => {
                            setEmail(event.target.value);
                        }}
                    />
                </FormControl>
                <FormControl fullWidth sx={{
                    m: 0,
                    marginTop: 2,
                    marginBottom: 2,
                }}>
                    <TextField
                        id="phone"
                        size='small'
                        label="Телефон"
                        variant="outlined"
                        value={phone}
                        onChange={event => {
                            setPhone(event.target.value);
                        }}
                    />
                </FormControl>
                <Button variant="contained" color="primary" onClick={() => {
                    setOpen(true);
                    let avatar = null;

                    if(imageChanged) {
                        avatar = imagePreviewUrl;
                    }

                    saveCompany(Network.userProfile.company_id, name, address, email, phone, avatar)
                    .then(() => {
                        setOpen(false);
                        swal(Config.appName, "Изменения сохранены!", "success");
                    })
                    .catch(err => {
                        setOpen(false);
                        swal(Config.appName, err, "warning");
                    });
                }}>
                    Сохранить
                </Button>
            </CardContent>
            <Backdrop
                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open={open}
                onClick={() => {

                }}
            >
                <CircularProgress color="inherit" />
            </Backdrop>
        </Card>
      </Container>
    </Box>
  </div>);
};

export default Company;
