import React, { useEffect } from 'react';
import { Link as RouterLink, useLocation, useNavigate, useParams } from 'react-router-dom';
import { useTheme } from '@material-ui/core/styles';
import { Box, 
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Avatar,
    Stack,
    Container, Modal, Typography, FormControl, TextField, Autocomplete, FormControlLabel, Switch, Select, OutlinedInput, MenuItem, Grid } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { saveQuestion } from '../../helpers/Network';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import swal from 'sweetalert';
import Config from '../../constants/Config';
import Helper from './../../helpers/Helper';
import {styled} from '@material-ui/core';
import PhotoCamera from '@material-ui/icons/PhotoCamera';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
    //   width: '100%',
    },
  },
};

const Input = styled('input')({
    display: 'none',
});

const DeviationModalView = ({open, deviation, onClose, onSave}) => {

  let { id, checklist } = useParams();

  const [deviationId, setDeviationId] = React.useState(0);
  const [deviationText, setDeviationText] = React.useState('');
  const [deviationPlace, setDeviationPlace] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [showModal, setShowModal] = React.useState(false);

  useEffect(() => {
    setShowModal(open);
  }, [open]);

  useEffect(() => {
		if (deviation) {
            if (deviation.id == 0) {
                setDeviationId(Helper.generateUUID());
            } else {
                setDeviationId(deviation.id);
            }
			setDeviationText(deviation.text);
			setDeviationPlace(deviation.place);
			setDescription(deviation.description);
		} else {
            setDeviationId(0);
            setDeviationText('');
			setDeviationPlace('');
			setDescription('');
        }
  }, [deviation]);

  return (
    <Dialog
        open={showModal}
        onClose={(event, reason) => {
            if (reason !== 'backdropClick') {
                
            }
        }}
        fullWidth
        maxWidth="sm"
    >
        <DialogTitle>
            <Typography variant="h4">
                {deviationId === 0 ? 'Добавление отклонения' : 'Редактирование отклонения'}
            </Typography>
            <IconButton
                aria-label="close"
                onClick={() => {
                    if (onClose) {
                        onClose();
                    }
                }}
                sx={{
                    position: 'absolute',
                    right: 8,
                    top: 8,
                    color: (theme) => theme.palette.grey[500],
                }}
                >
                <CloseIcon />
            </IconButton>
        </DialogTitle>
        <DialogContent>
            <FormControl sx={{
				width: '100%',
                m: 0,
                mt: 2,
                marginBottom: 1,
            }}>
				<TextField
                    style={{
                        // height: 30,
						width: '100%'
                    }}
                    id="deviation"
                    label="Описание отклонения от требований"
                    variant="outlined"
                    value={description}
                    size="small"
					disabled
					multiline
					rows={4}
                    onChange={event => {
                        // let obj = JSON.parse(JSON.stringify(value));
                        // obj.label = event.target.value;
                        // console.warn(obj);
                        // setValue(obj);
                    }}
                />
            </FormControl>
			<FormControl sx={{
				width: '100%',
                m: 0,
                marginBottom: 1,
            }}>
                <TextField
                    style={{
                        // height: 30,
						width: '100%'
                    }}
                    id="deviation"
                    label="Описание отклонения"
                    variant="outlined"
                    value={deviationText}
                    size="small"
					multiline
					rows={4}
                    onChange={event => {
                        setDeviationText(event.target.value);
                    }}
                />
            </FormControl>
			<FormControl sx={{
				width: '100%',
                m: 0,
                marginBottom: 1,
            }}>
                <TextField
                    style={{
                        // height: 30,
						width: '100%'
                    }}
                    id="deviation"
                    label="Место отклонения"
                    variant="outlined"
                    value={deviationPlace}
                    size="small"
                    onChange={event => {
                        setDeviationPlace(event.target.value);
                    }}
                />
            </FormControl>
        </DialogContent>
        <DialogActions>
            <Button variant="contained" sx={{
                mt: 1,
            }} color="primary" onClick={() => {
				if (deviationText === null || deviationText.length === 0) {
					swal(Config.appName, 'Укажите описание отклонения!', "warning");
					return;
				}

				// if (deviationPlace === null || deviationPlace.length === 0) {
				// 	swal(Config.appName, 'Укажите место отклонения!', "warning");
				// 	return;
				// }
				if (onSave) {
					onSave({
						id: deviationId,
                        place: deviationPlace,
                        text: deviationText,
                        description: description,
					});
				}
            }}>
                Сохранить
            </Button>
        </DialogActions>
    </Dialog>);
};

export default DeviationModalView;
