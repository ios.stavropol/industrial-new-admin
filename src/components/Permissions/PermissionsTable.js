import React, { useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Avatar,
  Box,
  Card,
  Switch,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from '@material-ui/core';

const PermissionsTable = ({ customers, updatePermission, ...rest }) => {
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Card {...rest}>
        <Box>
          <Table size='small'>
            <TableHead>
              <TableRow>
                <TableCell>
                  Разрешение
                </TableCell>
                <TableCell>
                  Описание
                </TableCell>
                <TableCell>
                  {`Супер\nадминистратор`}
                </TableCell>
                <TableCell>
                  Администратор компании
                </TableCell>
                <TableCell>
                  Аудитор
                </TableCell>
                <TableCell>
                  Статист
                </TableCell>
                <TableCell>
                  Топ-менеджер
                </TableCell>
                <TableCell>
                  Средний менеджер
                </TableCell>
                <TableCell>
                  Специалист
                </TableCell>
                <TableCell>
                  Рабочий
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {customers.slice(page*limit, page*limit + limit).map((customer) => {
                return (
                    <TableRow
                        hover
                        key={customer.id}
                        selected={selectedCustomerIds.indexOf(customer.id) !== -1}
                    >
                        <TableCell style={{
                            // width: '5%'
                        }}>
                            {customer.name}
                        </TableCell>
                        <TableCell>
                            {customer.description}
                        </TableCell>
                        <TableCell style={{
                            // width: '15%'
                        }}>
                            <Switch
                                checked={customer.admin}
                                disabled
                                onChange={checked => {
                                    if (updatePermission) {
                                        updatePermission('admin', customer.name, checked.target.checked ? 1 : 0);
                                    }
                                }}
                                defaultChecked={customer.admin} 
                            />
                        </TableCell>
                        <TableCell style={{
                            // width: '15%'
                        }}>
                            <Switch
                                checked={customer.admin_company}
                                onChange={checked => {
                                    if (updatePermission) {
                                        updatePermission('admin_company', customer.name, checked.target.checked ? 1 : 0);
                                    }
                                }}
                                defaultChecked={customer.admin_company} 
                            />
                        </TableCell>
                        <TableCell style={{
                            // width: '15%'
                        }}>
                            <Switch
                                checked={customer.auditor}
                                onChange={checked => {
                                    if (updatePermission) {
                                        updatePermission('auditor', customer.name, checked.target.checked ? 1 : 0);
                                    }
                                }}
                                defaultChecked={customer.auditor} 
                            />
                        </TableCell>
                        <TableCell style={{
                            // width: '15%'
                        }}>
                            <Switch
                                checked={customer.statist}
                                onChange={checked => {
                                    if (updatePermission) {
                                        updatePermission('statist', customer.name, checked.target.checked ? 1 : 0);
                                    }
                                }}
                                defaultChecked={customer.statist} 
                            />
                        </TableCell>
                        <TableCell style={{
                            // width: '15%'
                        }}>
                            <Switch
                                checked={customer['top-manager']}
                                onChange={checked => {
                                    if (updatePermission) {
                                        updatePermission('top-manager', customer.name, checked.target.checked ? 1 : 0);
                                    }
                                }}
                                defaultChecked={customer['top-manager']} 
                            />
                        </TableCell>
                        <TableCell style={{
                            // width: '15%'
                        }}>
                            <Switch
                                checked={customer['middle-manager']}
                                onChange={checked => {
                                    if (updatePermission) {
                                        updatePermission('middle-manager', customer.name, checked.target.checked ? 1 : 0);
                                    }
                                }}
                                defaultChecked={customer['middle-manager']} 
                            />
                        </TableCell>
                        <TableCell style={{
                            // width: '15%'
                        }}>
                            <Switch
                                checked={customer.specialist}
                                onChange={checked => {
                                    if (updatePermission) {
                                        updatePermission('specialist', customer.name, checked.target.checked ? 1 : 0);
                                    }
                                }}
                                defaultChecked={customer.specialist} 
                            />
                        </TableCell>
                        <TableCell style={{
                            // width: '15%'
                        }}>
                            <Switch
                                checked={customer.worker}
                                onChange={checked => {
                                    if (updatePermission) {
                                        updatePermission('worker', customer.name, checked.target.checked ? 1 : 0);
                                    }
                                }}
                                defaultChecked={customer.worker} 
                            />
                        </TableCell>
                    </TableRow>)
              })}
            </TableBody>
          </Table>
        </Box>
      <TablePagination
        component="div"
        labelRowsPerPage={'Показывать'}
        labelDisplayedRows={({ from, to, count }) => {
          return (from + '-' + to + ' из ' + count);
        }}
        count={customers.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

PermissionsTable.propTypes = {
  customers: PropTypes.array.isRequired
};

export default PermissionsTable;
