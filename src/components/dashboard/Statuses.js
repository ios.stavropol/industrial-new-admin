import React, { useEffect } from 'react';
import {
  Box,
  Card,
  CardContent,
  useTheme,
  colors,
} from '@material-ui/core';
import Network, { getAudits } from '../../helpers/Network';
import Config from '../../constants/Config';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import BarChart from '@material-ui/icons/BarChart';
import PieChart from '@material-ui/icons/PieChart';
import { Doughnut, Bar } from 'react-chartjs-2';

const useStyles = makeStyles({
    root: {
      width: 500,
    },
  });

const Statuses = ({ onMobileClose, openMobile }) => {

    const theme = useTheme();

    const options = {
        animation: false,
        cutoutPercentage: 80,
        layout: { padding: 0 },
        legend: {
          display: false
        },
        maintainAspectRatio: false,
        responsive: true,
        tooltips: {
          backgroundColor: theme.palette.background.paper,
          bodyFontColor: theme.palette.text.secondary,
          borderColor: theme.palette.divider,
          borderWidth: 1,
          enabled: true,
          footerFontColor: theme.palette.text.secondary,
          intersect: false,
          mode: 'index',
          titleFontColor: theme.palette.text.primary
        }
    };
    
    const options2 = {
        animation: false,
        cornerRadius: 20,
        layout: { padding: 0 },
        legend: { display: false },
        maintainAspectRatio: false,
        responsive: true,
        scales: {
          xAxes: [
            {
              barThickness: 12,
              maxBarThickness: 10,
              barPercentage: 0.5,
              categoryPercentage: 0.5,
              ticks: {
                fontColor: theme.palette.text.secondary
              },
              gridLines: {
                display: false,
                drawBorder: false
              }
            }
          ],
          yAxes: [
            {
              ticks: {
                fontColor: theme.palette.text.secondary,
                beginAtZero: true,
                min: 0
              },
              gridLines: {
                borderDash: [2],
                borderDashOffset: [2],
                color: theme.palette.divider,
                drawBorder: false,
                zeroLineBorderDash: [2],
                zeroLineBorderDashOffset: [2],
                zeroLineColor: theme.palette.divider
              }
            }
          ]
        },
        tooltips: {
          backgroundColor: theme.palette.background.paper,
          bodyFontColor: theme.palette.text.secondary,
          borderColor: theme.palette.divider,
          borderWidth: 1,
          enabled: true,
          footerFontColor: theme.palette.text.secondary,
          intersect: false,
          mode: 'index',
          titleFontColor: theme.palette.text.primary
        }
    };

    const renderGraph = () => {
        if (value2 === 'recents') {
            setGraphView(<Bar
                data={{
                    datasets: [
                    {
                        backgroundColor: colors.indigo[500],
                        data: [ready, working, ended],
                        backgroundColor: [
                            colors.red[500],
                            colors.orange[500],
                            colors.green[500],
                        ],
                        borderColor: [
                            colors.red[500],
                            colors.orange[500],
                            colors.green[500],
                        ],
                    },
                    ],
                    labels: ['Запланирован', 'В работе', 'Завершен']
                }}
                options={options2}
            />);
        } else {
            setGraphView(<Doughnut
                data={{
                    datasets: [
                    {
                        data: [ready, working, ended],
                        backgroundColor: [
                        colors.red[500],
                        colors.orange[500],
                        colors.green[500],
                        ],
                        borderWidth: 8,
                        borderColor: colors.common.white,
                        hoverBorderColor: colors.common.white
                    }
                    ],
                    labels: ['Запланирован', 'В работе', 'Завершен']
                }}
                options={options}
            />);
        }
    }

    const location = useLocation();
    const navigate = useNavigate();

    const classes = useStyles();
    const [value2, setValue2] = React.useState('recents');
    const [graphView, setGraphView] = React.useState(null);

    const [ready, setReady] = React.useState(0);
    const [working, setWorking] = React.useState(0);
    const [ended, setEnded] = React.useState(0);

    const handleChange2 = (event, newValue) => {
        setValue2(newValue);
    };

    useEffect(() => {
        getAudits()
        .then(audits => {
    
          let ready = 0;
          let working = 0;
          let ended = 0;
          for (let i = 0; i < audits.length; i++) {
              if (audits[i].status === 'ready') {
                  ready++;
              } else if (audits[i].status === 'working') {
                  working++;
              } else if (audits[i].status === 'ended') {
                  ended++;
              }
          }
          setReady(ready);
          setWorking(working);
          setEnded(ended);
          renderGraph();
        })
        .catch(err => {

        });
    }, [location.pathname]);

    useEffect(() => {
        renderGraph();
    }, [value2, ready, ended, working]);

    return (<Card sx={{
            mt: 2,
        }}>
            <CardContent>
                <Box display="flex" flexDirection="row" justifyContent="center" p={1} m={1}>
                    <BottomNavigation value={value2} onChange={handleChange2} className={classes.root}>
                        <BottomNavigationAction label="Столбчатый" value="recents" icon={<BarChart />} />
                        <BottomNavigationAction label="Круговой" value="favorites" icon={<PieChart />} />
                    </BottomNavigation>
                </Box>
                {graphView}
            </CardContent>
        </Card>);
};

export default Statuses;
