import React, { useEffect } from 'react';
import {
  Box,
  Card,
  CardContent,
  useTheme,
  colors,
} from '@material-ui/core';
import Network, { getProcessesStat } from '../../helpers/Network';
import Config from '../../constants/Config';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Bar } from 'react-chartjs-2';
import FilterView from './FilterView';

const Process = ({ process, onMobileClose, openMobile }) => {

    const theme = useTheme();
    
    let options2 = {
        animation: false,
        cornerRadius: 20,
        layout: { padding: 0 },
        legend: { display: false },
        maintainAspectRatio: false,
        responsive: true,
        onClick: (evt, element) => {
          if(element.length > 0) {
            if (rawData) {
              console.warn(rawData.statistic[element[0]._index]);
              window.location.href = '/company/audits/' + rawData.statistic[element[0]._index].id + '/result';
            }
          }
        },
        scales: {
          xAxes: [
            {
              barThickness: 12,
              maxBarThickness: 10,
              barPercentage: 0.5,
              categoryPercentage: 0.5,
              ticks: {
                fontColor: theme.palette.text.secondary
              },
              gridLines: {
                display: false,
                drawBorder: false
              }
            }
          ],
          yAxes: [
            {
              ticks: {
                fontColor: theme.palette.text.secondary,
                beginAtZero: true,
                min: 0
              },
              gridLines: {
                borderDash: [2],
                borderDashOffset: [2],
                color: theme.palette.divider,
                drawBorder: false,
                zeroLineBorderDash: [2],
                zeroLineBorderDashOffset: [2],
                zeroLineColor: theme.palette.divider
              }
            }
          ]
        },
        tooltips: {
          backgroundColor: theme.palette.background.paper,
          bodyFontColor: theme.palette.text.secondary,
          borderColor: theme.palette.divider,
          borderWidth: 1,
          enabled: true,
          footerFontColor: theme.palette.text.secondary,
          intersect: false,
          mode: 'index',
          titleFontColor: theme.palette.text.primary
        }
    };

    const location = useLocation();
    const navigate = useNavigate();

    const [allData, setAllData] = React.useState(null);
    const [rawData, setRawData] = React.useState(null);
    const [statistic, setStatistic] = React.useState(null);
    const [processList, setProcessList] = React.useState([]);
    const [processSelected, setProcessSelected] = React.useState(null);
    const [unitList, setUnitList] = React.useState([]);
    const [unitSelected, setUnitSelected] = React.useState(null);
    const [minDate, setMinDate] = React.useState(new Date('2020-02-02'));
    const [fromDate, setFromDate] = React.useState(new Date('2020-02-02'));
    const [toDate, setToDate] = React.useState(new Date());
    const [graphView, setGraphView] = React.useState(null);
    const [categoryText, setCategoryText] = React.useState('');

    useEffect(() => {
      getProcessesStat(null, null, null, null)
        .then(data => {
          // window.alert(JSON.stringify(data));
          setRawData(data);
          let processes = [];
          let units = [];
          let ids = [];
          for (let i = 0; i < data.processes.length; i++) {
            processes.push({
              id: data.processes[i].id,
              name: data.processes[i].name,
            });
          }
          setProcessList(processes);
          if (processes.length) {
            if (process == null) {
              setProcessSelected(processes[0].id);
            } else {
              setProcessSelected(process);
            }
            units = data.processes[0].units;
            setMinDate(new Date(data.processes[0].min_date));
            setFromDate(new Date(data.processes[0].min_date));
            setToDate(new Date());
          }
          setUnitList(units);
          setUnitSelected(null);
        })
        .catch(err => {

        });
    }, [location.pathname]);

    useEffect(() => {
      if (process) {
        setProcessSelected(process);
        getProcessesStat(process, null, null, null, null)
        .then(data => {
          setRawData(data);
        })
        .catch(err => {

        });
      }
    }, [process, processList]);

    useEffect(() => {
      if (rawData) {
        setStatistic(rawData.statistic);
        setAllData(rawData.processes);
        let dataset = [];
        let labels = [];
        for (let i = 0; i < rawData.statistic.length; i++) {
          dataset.push(rawData.statistic[i].score);
          labels.push(rawData.statistic[i].date);
        }

        setGraphView(<Bar
          // height={300}
          // width={'100%'}
          data={{
              datasets: [
              {
                  backgroundColor: colors.indigo[500],
                  data: dataset,
                  backgroundColor: colors.green[500],
                  borderColor: colors.green[500],
              },
              ],
              labels: labels
          }}
          options={options2}
        />);
      }
    }, [rawData]);

    return (<Card sx={{
            mt: 2,
        }}>
            <CardContent>
              <FilterView
                from={fromDate}
                to={toDate}
                minDate={minDate}
                processList={processList}
                processSelected={processSelected}
                unitList={unitList}
                unitSelected={unitSelected}
                categoryText={categoryText}
                onCategorySelect={cat => {
                  setCategoryText(cat);
                }}
                onSelectProcess={id => {
                  setUnitSelected(null);
                  for (let i = 0; i < allData.length; i++) {
                    if (allData[i].id == id) {
                      setUnitList(allData[i].units);
                      setMinDate(new Date(allData[i].min_date));
                      setToDate(new Date());
                      setFromDate(new Date(allData[i].min_date));
                      break;
                    }
                  }
                }}
                onFilterClick={data => {
                  setProcessSelected(data.process_id);
                  setFromDate(data.from);
                  setToDate(data.to);
                  setUnitSelected(data.unit_id);
                  getProcessesStat(data.process_id, data.from, data.to, data.unit_id, data.category)
                  .then(data => {
                    setRawData(data);
                  })
                  .catch(err => {

                  });
                }}
              />
                {graphView}
            </CardContent>
        </Card>);
};

export default Process;
