import React, { useEffect } from 'react';
import {
  Box,
  Card,
  CardContent,
  useTheme,
  colors,
} from '@material-ui/core';
import Network, { getLastStatistic } from '../../helpers/Network';
import Config from '../../constants/Config';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import BarChart from '@material-ui/icons/BarChart';
import PieChart from '@material-ui/icons/PieChart';
import { Radar, Polar } from 'react-chartjs-2';

const useStyles = makeStyles({
    root: {
      width: 500,
    },
});

const AllProcess = ({ onClickProcess, onMobileClose, openMobile }) => {

    const theme = useTheme();

    const renderGraph = () => {
        if (value2 === 'recents') {
            setGraphView(<Radar
                data={{
                    datasets: [
                    {
                        data: data,
                        backgroundColor: colors,
                        borderColor: colors
                    },
                    ],
                    labels: labels
                }}
                options={{
                    elements: {
                        line: {
                            borderWidth: 3
                        }
                    },
                    legend: {
                        display: false,
                    }
                }}
            />);
        } else {
            setGraphView(<Polar
                data={{
                    datasets: [
                    {
                        data: data,
                        backgroundColor: colors,
                        // borderWidth: 8,
                        // borderColor: colors.common.white,
                        // hoverBorderColor: colors.common.white
                    }
                    ],
                    labels: labels
                }}
                options={{
                    legend: {
                        display: false,
                    },
                    onClick: (evt, element) => {
                        if(element.length > 0) {
                            console.warn(element);
                            if (process) {
                                console.warn(process[element[0]._index]);
                                if (onClickProcess) {
                                    onClickProcess(process[element[0]._index].process_id);
                                }
                                // window.location.href = '/company/audits/' + rawData.statistic[element[0]._index].id + '/result';
                            }
                        }
                    },
                }}
            />);
        }
    }

    const location = useLocation();
    const navigate = useNavigate();

    const classes = useStyles();
    const [value2, setValue2] = React.useState('recents');
    const [graphView, setGraphView] = React.useState(null);

    const [data, setData] = React.useState([]);
    const [labels, setLabels] = React.useState([]);
    const [colors, setColors] = React.useState([]);
    const [process, setProcess] = React.useState([]);

    const handleChange2 = (event, newValue) => {
        setValue2(newValue);
    };

    useEffect(() => {
        getLastStatistic()
        .then(process => {
    
            let data = [];
            let labels = [];
            let colors = [];
            for (let i = 0; i < process.length; i++) {
                let name = process[i].name.substring(0, 15) + '...' + process[i].name.substring(process[i].name.length - 15);
                data.push(process[i].score);
                labels.push(name);
                colors.push("#"+(((1+Math.random())*(1<<24)|0).toString(16)).substr(-6));
            }
            setProcess(process);
            setData(data);
            setLabels(labels);
            setColors(colors);
            renderGraph();
        })
        .catch(err => {

        });
    }, [location.pathname]);

    useEffect(() => {
        renderGraph();
    }, [value2, data, labels, colors]);

    return (<Card sx={{
            mt: 2,
    }}>
        <CardContent>
            <Box display="flex" flexDirection="row" justifyContent="center" p={1} m={1}>
                <BottomNavigation value={value2} onChange={handleChange2} className={classes.root}>
                    <BottomNavigationAction label="Radar" value="recents" icon={<BarChart />} />
                    <BottomNavigationAction label="Polar" value="favorites" icon={<PieChart />} />
                </BottomNavigation>
            </Box>
            {graphView}
        </CardContent>
    </Card>);
};

export default AllProcess;
