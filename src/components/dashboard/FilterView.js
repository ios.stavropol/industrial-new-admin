import React, { useEffect } from 'react';
import {
  Box,
  Button,
  Card,
  CardContent,
  TextField,
  InputAdornment,
  SvgIcon,
  FormControl,
  Select,
  MenuItem,
  OutlinedInput,
} from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import DatePicker from '@material-ui/lab/DatePicker';
import ruLocale from 'date-fns/locale/ru';
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import Helper from '../../helpers/Helper';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
    //   width: '100%',
    },
  },
};

const categoryOptions = [
    {value: '',                 label: 'Все'},
    {value: 'top-manager',      label: 'Топ менеджер'},
    {value: 'middle-manager',   label: 'Менеджер среднего звена'},
    {value: 'specialist',       label: 'Специалист'},
    {value: 'worker',           label: 'Рабочий'},
];

const FilterView = ({props, minDate, to ,from, processList, processSelected, unitList, unitSelected, onCategorySelect, onFilterClick, onSelectProcess, onSelectUnit}) => {

    const theme = useTheme();

    const [min, setMin] = React.useState(new Date('2020-02-02'));
    const [fromDate, setFromDate] = React.useState(new Date('2020-02-02'));
    const [toDate, setToDate] = React.useState(new Date());
    const [processId, setProcessId] = React.useState(0);
    const [unitId, setUnitId] = React.useState(0);
    const [processOptions, setProcessOptions] = React.useState([]);
    const [unitOptions, setUnitOptions] = React.useState([]);
    const [category, setCategory] = React.useState(null);

    const handleChange = (event) => {
        const {
          target: { value },
        } = event;
        for (let i = 0; i < categoryOptions.length; i++) {
            if (value === categoryOptions[i].value) {
                setCategory(categoryOptions[i]);
                onCategorySelect(categoryOptions[i].value);
                break;
            }
        }
    };

    useEffect(() => {
        setMin(minDate);
    }, [minDate]);

    useEffect(() => {
        setProcessOptions(processList);
    }, [processList]);

    useEffect(() => {
        setProcessId(processSelected);
        // window.alert(processSelected);
    }, [processSelected]);

    useEffect(() => {
        setUnitOptions(unitList);
    }, [unitList]);

    useEffect(() => {
        setUnitId(unitSelected);
    }, [unitSelected]);

    useEffect(() => {
        setToDate(to);
    }, [to]);

    useEffect(() => {
        setFromDate(from);
    }, [from]);

    const processChange = event => {
        const {
          target: { value },
        } = event;
        setProcessId(value);
        if (onSelectProcess) {
            onSelectProcess(value);
        }
    };

    const unitChange = event => {
        const {
          target: { value },
        } = event;
        setUnitId(value);
        // if (onSelectUnit) {
        //     onSelectUnit(value);
        // }
    };

    return (
    <Box {...props}>
        <Box sx={{ mt: 3, mb: 3 }}>
        <Card>
            <CardContent>
            <Box display="flex" flexDirection="row" justifyContent="space-between">
                <FormControl sx={{
                    m: 0,
                    width: '20%',
                    // mt: 1,
                    // mb: 1,
                }}>
                    <Select
                        displayEmpty
                        size="small"
                        placeholder={'Бизнес-процесс'}
                        value={processId}
                        onChange={processChange}
                        input={<OutlinedInput />}
                        renderValue={(selected) => {
                            if (selected === null || selected.length === 0) {
                                return 'Бизнес-процесс';
                            }
                            for (let i = 0; i < processOptions.length; i++) {
                                if (processOptions[i].id == selected) {
                                    return processOptions[i].name;
                                }
                            }
                            return selected;
                        }}
                        MenuProps={MenuProps}
                        inputProps={{ 'aria-label': 'Without label' }}
                    >
                    {processOptions.map((name) => (
                        <MenuItem
                            key={name.id}
                            value={name.id}
                            label={name.name}
                            style={{
                                fontWeight: theme.typography.fontWeightMedium,
                            }}
                        >
                            {name.name}
                        </MenuItem>
                    ))}
                    </Select>
                </FormControl>
                <FormControl fullWidth sx={{
                    m: 0,
                    width: '15%',
                    marginBottom: 1,
                }}>
                    <LocalizationProvider dateAdapter={AdapterDateFns} locale={ruLocale}>
                    <DatePicker
                        label="Начальная дата"
                        value={fromDate}
                        size='small'
                        // minDate={min}
                        // maxDate={new Date()}
                        mask={'__.__.____'}
                        onChange={(newValue) => {
                            setFromDate(newValue);
                        }}
                        renderInput={(params) => <TextField size='small' {...params} />}
                    />
                    </LocalizationProvider>
                </FormControl>
                <FormControl fullWidth sx={{
                    m: 0,
                    width: '15%',
                    marginBottom: 1,
                }}>
                    <LocalizationProvider dateAdapter={AdapterDateFns} locale={ruLocale}>
                    <DatePicker
                        label="Конечная дата"
                        value={toDate}
                        size='small'
                        // minDate={min}
                        // maxDate={new Date()}
                        mask={'__.__.____'}
                        onChange={(newValue) => {
                            setToDate(newValue);
                        }}
                        renderInput={(params) => <TextField size='small' {...params} />}
                    />
                    </LocalizationProvider>
                </FormControl>
                <FormControl sx={{
                    m: 0,
                    width: '20%',
                    // mt: 1,
                    // mb: 1,
                }}>
                    <Select
                        displayEmpty
                        size="small"
                        placeholder={'Подразделение'}
                        value={unitId}
                        onChange={unitChange}
                        input={<OutlinedInput />}
                        renderValue={(selected) => {
                            if (selected === null || selected.length === 0) {
                                return 'Все подразделения';
                            }
                            for (let i = 0; i < unitOptions.length; i++) {
                                if (unitOptions[i].id == selected) {
                                    return unitOptions[i].name;
                                }
                            }
                            return selected;
                        }}
                        MenuProps={MenuProps}
                        inputProps={{ 'aria-label': 'Without label' }}
                    >
                    {unitOptions.map((name) => (
                        <MenuItem
                            key={name.id}
                            value={name.id}
                            style={{
                                fontWeight: theme.typography.fontWeightMedium,
                            }}
                        >
                            {name.name}
                        </MenuItem>
                    ))}
                    </Select>
                </FormControl>
                <FormControl sx={{
                    m: 0,
                    width: '20%',
                    // mt: 1,
                    // mb: 1,
                }}>
                    <Select
                        displayEmpty
                        size="small"
                        placeholder={'Категория'}
                        value={category}
                        onChange={handleChange}
                        input={<OutlinedInput />}
                        renderValue={(selected) => {
                            if (selected === null || selected.length === 0) {
                                return 'Категория';
                            }
                            return selected.label;
                        }}
                        MenuProps={MenuProps}
                        inputProps={{ 'aria-label': 'Without label' }}
                    >
                    <MenuItem disabled value="">
                        <em>Категория</em>
                    </MenuItem>
                    {categoryOptions.map((name) => (
                        <MenuItem
                            key={name.value}
                            value={name.value}
                            style={{
                                fontWeight: theme.typography.fontWeightMedium,
                            }}
                        >
                            {name.label}
                        </MenuItem>
                    ))}
                    </Select>
                </FormControl>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => {
                        let from = fromDate;
                        let to = toDate;
                        if (!(from instanceof Date)) {
                            from = new Date(from);
                        }
                        if (!(to instanceof Date)) {
                            to = new Date(to);
                        }
                        if (onFilterClick) {
                            onFilterClick({
                                process_id: processId,
                                from: Helper.getDateFromJSDateTime(from),
                                to: Helper.getDateFromJSDateTime(to),
                                unit_id: unitId,
                                category: category ? category.value : null,
                            });
                        }
                    }}
                    style={{
                        // marginTop: 10,
                    }}
                >
                    Применить
                </Button>
            </Box>
            </CardContent>
        </Card>
        </Box>
    </Box>);
}

export default FilterView;
