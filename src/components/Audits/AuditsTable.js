import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import UsersIcon from '@material-ui/icons/Group';
import IconButton from '@material-ui/core/IconButton';

const AuditsTable = ({ audits, process, onDeleteAudit, onAuditResult, onEditAudit, ...rest }) => {
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);
  const [processes, setProcesses] = useState([]);

  useEffect(() => {
    setProcesses(process);
  }, [process]);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const getDate = input => {
    let date = new Date(input);
    let dd = date.getDate();
    dd = dd < 10 ? '0' + dd : dd;

    let mm = date.getMonth() + 1;
    mm = mm < 10 ? '0' + mm : mm;

    let yyyy = date.getFullYear();

    return dd + '.' + mm + '.' + yyyy;
  }

  return (
    <Card {...rest}>
        <Box>
          <Table size='small'>
            <TableHead>
              <TableRow>
                <TableCell>
                  ID
                </TableCell>
                <TableCell>
                  Дата
                </TableCell>
                <TableCell>
                  Цель
                </TableCell>
                <TableCell>
                  Бизнес-процесс
                </TableCell>
                <TableCell>
                  Статус
                </TableCell>
                <TableCell>
                  Операции
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {audits.slice(page*limit, page*limit + limit).map((audit, i) => {
                let processName = '';
                for (let i = 0; i < processes.length; i++) {
                    if (processes[i].id === audit.process_id) {
                        processName = processes[i].name;
                        break;
                    }
                }

                let status = '';
                if (audit.status === 'ready') {
                    status = 'Запланирован';
                } else if (audit.status === 'draft') {
                    status = 'Черновик';
                } else if (audit.status === 'working') {
                    status = 'В работе';
                } else {
                    status = 'Завершен';
                }

                return (
                    <TableRow
                        hover
                        key={audit.id}
                        selected={selectedCustomerIds.indexOf(audit.id) !== -1}
                    >
                    <TableCell style={{
                        width: '5%'
                    }}>
                        {page*limit + i + 1}
                    </TableCell>
                    <TableCell>
                        {getDate(audit.date)}
                    </TableCell>
                    <TableCell>
                        {(<a href={'/company/audits/'+audit.id+'/result'}>{audit.target}</a>)}
                    </TableCell>
                    <TableCell>
                        {processName}
                    </TableCell>
                    <TableCell>
                        {status}
                    </TableCell>
                    <TableCell style={{
                        minWidth: '10rem',
                    }}>
                        {audit.status === 'draft' ? (<IconButton onClick={() => {
                            if (onEditAudit) {
                                onEditAudit(audit);
                            }
                        }}>
                            <EditIcon />
                        </IconButton>) : null}
                        <IconButton onClick={() => {
                            if (onDeleteAudit) {
                                onDeleteAudit(audit);
                            }
                        }}>
                            <DeleteIcon />
                        </IconButton>
                    </TableCell>
                </TableRow>)
              })}
            </TableBody>
          </Table>
        </Box>
      <TablePagination
        component="div"
        labelRowsPerPage={'Показывать'}
        labelDisplayedRows={({ from, to, count }) => {
          return (from + '-' + to + ' из ' + count);
        }}
        count={audits.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

AuditsTable.propTypes = {
    audits: PropTypes.array.isRequired,
    process: PropTypes.array.isRequired
};

export default AuditsTable;
