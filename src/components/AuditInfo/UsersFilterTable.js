import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  Switch
} from '@material-ui/core';
import Config from './../../constants/Config';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';

const UsersFilterTable = ({ customers, selectedUsers, units, onSelectUsers, ...rest }) => {
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);
  const [unitArray, setUnitArray] = useState([]);

  const handleSelectAll = (event) => {
    let newSelectedCustomerIds;

    if (event.target.checked) {
      newSelectedCustomerIds = customers.map((customer) => customer.id);
    } else {
      newSelectedCustomerIds = [];
    }

    setSelectedCustomerIds(newSelectedCustomerIds);

    if (onSelectUsers) {
        onSelectUsers(newSelectedCustomerIds);
    }
  };

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedCustomerIds.indexOf(id);
    let newSelectedCustomerIds = [];

    if (selectedIndex === -1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(selectedCustomerIds, id);
    } else if (selectedIndex === 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(selectedCustomerIds.slice(1));
    } else if (selectedIndex === selectedCustomerIds.length - 1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(selectedCustomerIds.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, selectedIndex),
        selectedCustomerIds.slice(selectedIndex + 1)
      );
    }

    setSelectedCustomerIds(newSelectedCustomerIds);

    if (onSelectUsers) {
        onSelectUsers(newSelectedCustomerIds);
    }
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  useEffect(() => {
    setUnitArray(units);
  }, [units]);

  useEffect(() => {
    setSelectedCustomerIds(selectedUsers);
  }, [selectedUsers]);

  return (
    <Card {...rest}>
        <Box sx={{
          // minWidth: 1050,
        }}>
          <Table size='small'>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                    <Checkbox
                        checked={selectedCustomerIds.length === customers.length}
                        color="primary"
                        indeterminate={
                        selectedCustomerIds.length > 0
                        && selectedCustomerIds.length < customers.length
                        }
                        onChange={handleSelectAll}
                    />
                </TableCell>
                <TableCell>
                  ID
                </TableCell>
                <TableCell>
                  ФИО
                </TableCell>
                <TableCell>
                  Фото
                </TableCell>
                <TableCell>
                  Подразделение
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {customers.slice(page*limit, page*limit + limit).map((customer) => {
                  let unit = '';
                  for (let i = 0; i < unitArray.length; i++) {
                    if (customer.unit_id === unitArray[i].id) {
                        unit = unitArray[i].name;
                        break;
                    }
                  }
                  return (
                <TableRow
                  hover
                  key={customer.id}
                  selected={selectedCustomerIds.indexOf(customer.id) !== -1}
                >
                    <TableCell padding="checkbox">
                        <Checkbox
                        checked={selectedCustomerIds.indexOf(customer.id) !== -1}
                        onChange={(event) => handleSelectOne(event, customer.id)}
                        value="true"
                        />
                    </TableCell>
                    <TableCell style={{
                        width: '5%'
                    }}>
                        {customer.id}
                    </TableCell>
                    <TableCell>
                        {customer.fio}
                    </TableCell>
                    <TableCell style={{
                        // width: '5%'
                    }}>
                        {customer.avatar !== null ? (
                        <Avatar
                            sx={{
                            width: 50,
                            height: 50,
                            bgcolor: '#5664d2',
                            }}
                            src={Config.apiDomain + '/uploads/user/' + customer.avatar}
                        />
                        ) : (
                        <Avatar
                            sx={{
                            width: 50,
                            height: 50,
                            bgcolor: '#5664d2',
                            }}
                            children={`${customer.fio.split(' ')[0][0]}${customer.fio.split(' ')[1][0]}`}
                        />
                        )}
                    </TableCell>
                    <TableCell style={{
                        // width: '5%'
                    }}>
                        {unit}
                    </TableCell>
                </TableRow>
              )})}
            </TableBody>
          </Table>
        </Box>
      <TablePagination
        component="div"
        labelRowsPerPage={'Показывать'}
        labelDisplayedRows={({ from, to, count }) => {
          return (from + '-' + to + ' из ' + count);
        }}
        count={customers.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

UsersFilterTable.propTypes = {
  customers: PropTypes.array.isRequired
};

export default UsersFilterTable;
