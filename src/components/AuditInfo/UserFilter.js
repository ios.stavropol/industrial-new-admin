import React, { useEffect } from 'react';
import {
  Box,
  Button,
  Card,
  CardContent,
  TextField,
  InputAdornment,
  SvgIcon,
  FormControl,
  Select,
  MenuItem,
  OutlinedInput,
} from '@material-ui/core';
import { Search as SearchIcon } from 'react-feather';
import { useTheme } from '@material-ui/core/styles';
import UnitSelectModalView from './UnitSelectModalView';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
    //   width: '100%',
    },
  },
};

const categoryOptions = [
    {value: '',                 label: 'Все'},
    {value: 'top-manager',      label: 'Топ менеджер'},
    {value: 'middle-manager',   label: 'Менеджер среднего звена'},
    {value: 'specialist',       label: 'Специалист'},
    {value: 'worker',           label: 'Рабочий'},
];

const UserFilter = ({props, onUnitSelect, unitsList, unitsRawList, onSearch, onCategorySelect, searchText, categoryText}) => {

    const theme = useTheme();

    const [category, setCategory] = React.useState(null);
    const [search, setSearch] = React.useState('');

    const [units, setUnits] = React.useState([]);
    const [unitRaw, setUnitRaw] = React.useState([]);
    const [selectedList, setSelectedList] = React.useState([]);
    const [unitText, setUnitText] = React.useState('Все подразделения');
    const [showUnitModal, setShowUnitModal] = React.useState(false);

    const handleChange = (event) => {
        const {
          target: { value },
        } = event;
        for (let i = 0; i < categoryOptions.length; i++) {
            if (value === categoryOptions[i].value) {
                setCategory(categoryOptions[i]);
                onCategorySelect(categoryOptions[i].value);
                break;
            }
        }
    };

    useEffect(() => {
        setSearch(searchText);
    }, [searchText]);

    useEffect(() => {
        setUnitRaw(unitsRawList);
    }, [unitsRawList]);

    useEffect(() => {
        setUnits(unitsList);
    }, [unitsList]);

    return (
    <Box {...props}>
        <Box sx={{ mt: 3 }}>
        <Card>
            <CardContent>
            <Box display="flex" flexDirection="row" justifyContent="space-between" p={1} m={1}>
                <TextField
                    style={{
                        width: '40%',
                    }}
                    size='small'
                    value={search}
                    InputProps={{
                        startAdornment: (
                        <InputAdornment position="start">
                            <SvgIcon
                            fontSize="small"
                            color="action"
                            >
                            <SearchIcon />
                            </SvgIcon>
                        </InputAdornment>
                        )
                    }}
                    onChange={event => {
                        if (onSearch) {
                            onSearch(event.target.value);
                        }
                    }}
                    placeholder="Поиск"
                    variant="outlined"
                />
                <FormControl sx={{
                    m: 0,
                    width: '30%',
                    // mt: 1,
                    // mb: 1,
                }}>
                    <TextField
                        id="unit2"
                        label="Подразделение"
                        variant="outlined"
                        value={unitText}
                        size="small"
                        onChange={event => {
                            // setUnitText(event.target.value);
                        }}
                        onFocus={() => {
                            setShowUnitModal(true);
                        }}
                    />
                    <div style={{
                        position: 'absolute',
                        left: 0,
                        top: 0,
                        right: 0,
                        bottom: 0,
                    }}
                    onClick={() => {
                        setShowUnitModal(true);
                    }}
                    />
                </FormControl>
                <FormControl sx={{
                    m: 0,
                    width: '20%',
                    // mt: 1,
                    // mb: 1,
                }}>
                    <Select
                        displayEmpty
                        size="small"
                        placeholder={'Категория'}
                        value={category}
                        onChange={handleChange}
                        input={<OutlinedInput />}
                        renderValue={(selected) => {
                            if (selected === null || selected.length === 0) {
                                return 'Категория';
                            }
                            return selected.label;
                        }}
                        MenuProps={MenuProps}
                        inputProps={{ 'aria-label': 'Without label' }}
                    >
                    <MenuItem disabled value="">
                        <em>Категория</em>
                    </MenuItem>
                    {categoryOptions.map((name) => (
                        <MenuItem
                            key={name.value}
                            value={name.value}
                            style={{
                                fontWeight: theme.typography.fontWeightMedium,
                            }}
                        >
                            {name.label}
                        </MenuItem>
                    ))}
                    </Select>
                </FormControl>
            </Box>
            </CardContent>
        </Card>
        </Box>
        <UnitSelectModalView
            open={showUnitModal}
            unitList={units}
            selectedList={selectedList}
            onSelectUnit={unitIds => {
                if (unitIds.length === 0) {
                    setUnitText('Все подразделения');
                } else {
                    let str = '';
                    for (let i = 0; i < unitRaw.length; i++) {
                        if (unitIds.includes(unitRaw[i].id)) {
                            str = str + unitRaw[i].name + ', ';
                        }
                    }
                    setUnitText(str);
                }
                setShowUnitModal(false);
                if (onUnitSelect) {
                    onUnitSelect(unitIds);
                }
            }}
        />
    </Box>);
}

export default UserFilter;
