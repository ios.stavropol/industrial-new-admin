import React, { useEffect } from 'react';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import { Box, Typography, Modal, Checkbox, Select, OutlinedInput, MenuItem } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem, { useTreeItem } from '@material-ui/lab/TreeItem';
import clsx from 'clsx';
import Network from '../../helpers/Network';
import { useTheme } from '@material-ui/core/styles';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 700,
    bgcolor: 'background.paper',
    border: '2px solid white',
    boxShadow: 24,
    p: 4,
};

const CustomContent = React.forwardRef(function CustomContent(props, ref) {
    const {
      classes,
      className,
      label,
      nodeId,
      icon: iconProp,
      expansionIcon,
      displayIcon,
      children,
    } = props;
  
    const {
      disabled,
      expanded,
      selected,
      focused,
      handleExpansion,
      handleSelection,
      preventSelection,
    } = useTreeItem(nodeId);
  
    const icon = iconProp || expansionIcon || displayIcon;
  
    const handleMouseDown = (event) => {
      preventSelection(event);
    };
  
    const handleExpansionClick = (event) => {
      handleExpansion(event);
    };
  
    const handleSelectionClick = (event) => {
      handleSelection(event);
    };
  
    return (
      // eslint-disable-next-line jsx-a11y/no-static-element-interactions
      <div
        className={clsx(className, classes.root, {
          [classes.expanded]: expanded,
          [classes.selected]: selected,
          [classes.focused]: focused,
          [classes.disabled]: disabled,
        })}
        onMouseDown={handleMouseDown}
        ref={ref}
      >
        {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
        <div onClick={handleExpansionClick} className={classes.iconContainer}>
          {icon}
        </div>
        <Typography
          onClick={handleSelectionClick}
          component="div"
          className={classes.label}
        >
          {label}
        </Typography>
        <Checkbox
            checked={JSON.parse(nodeId).selected}
            onChange={event => {
                if (Network.selectUnitFilter) {
                    let data = JSON.parse(nodeId);
                    Network.selectUnitFilter(event.target.checked, data.id, data.ids);
                }
            }}
            inputProps={{ 'aria-label': 'primary checkbox' }}
        />
      </div>
    );
  });
  
  CustomContent.propTypes = {
    /**
     * Override or extend the styles applied to the component.
     */
    classes: PropTypes.object.isRequired,
    /**
     * className applied to the root element.
     */
    className: PropTypes.string,
    /**
     * The icon to display next to the tree node's label. Either a parent or end icon.
     */
    displayIcon: PropTypes.node,
    /**
     * The icon to display next to the tree node's label. Either an expansion or collapse icon.
     */
    expansionIcon: PropTypes.node,
    /**
     * The icon to display next to the tree node's label.
     */
    icon: PropTypes.node,
    /**
     * The tree node label.
     */
    label: PropTypes.node,
    /**
     * The id of the node.
     */
    nodeId: PropTypes.string.isRequired,
    /**
     * The data of the node.
     */
     onEdit: PropTypes.func.isRequired,
  };
  
  const CustomTreeItem = (props) => {
      return (
        <TreeItem ContentComponent={CustomContent} {...props} />
        )
};

const UnitSelectModalView = ({open, onSelectUnit, unitList, selectedList}) => {

  const location = useLocation();
  const navigate = useNavigate();

  const theme = useTheme();
  const [showModal, setShowModal] = React.useState(false);

  const [tree, setTree] = React.useState([]);
  const [units, setUnits] = React.useState([]);
  const [selectedUnitId, setSelectedUnitId] = React.useState([]);

    const getChilds = (item) => {
        if (Network.selectedUnit.includes(item.id)) {
            item.selected = true;
        } else {
            item.selected = false;
        }
        const isChildren = item.children !== null;
        if (isChildren) {
            return (
                <CustomTreeItem key={item.id} nodeId={JSON.stringify(item)} label={item.text} >
                    {item.children.map((node, idx) => getChilds(node))}
                </CustomTreeItem>
            );
        }
        return <CustomTreeItem key={item.id} nodeId={JSON.stringify(item)} label={item.text} />;
    }

    const selectUnit = (checked, id, ids) => {
        console.warn('checked: '+checked+' id: '+id+' ids: '+JSON.stringify(ids));
        let selectedIds = Network.selectedUnit;
        console.warn('selectedIds before: '+JSON.stringify(selectedIds));
        selectedIds = JSON.parse(JSON.stringify(selectedIds));
        if (checked) {
            selectedIds.push(id);
            selectedIds = selectedIds.concat(ids);
        } else {
            for(let i = 0; i < selectedIds.length; i++) {
                if (selectedIds[i] === id) {
                    selectedIds.splice(i, 1);
                    break;
                }
            }

            for (let y = 0; y < ids.length; y++) {
                for(let i = 0; i < selectedIds.length; i++) {
                    if (selectedIds[i] === ids[y]) {
                        selectedIds.splice(i, 1);
                        break;
                    }
                }
            }
        }
        // selectedIds = selectedIds.filter((v, i, a) => a.indexOf(v) === i);
        console.warn('selectedIds after: '+JSON.stringify(selectedIds));
        Network.selectedUnit = JSON.parse(JSON.stringify(selectedIds));
        let array = [];
        for (let i = 0; i < unitList.length; i++) {
            array.push(getChilds(unitList[i]));
        }
        setTree(array);
    }

  useEffect(() => {
    console.warn('useEffect(() => { open');
    setShowModal(open);
    Network.selectUnitFilter = selectUnit;
    // setUnits(unitList);
    let array = [];
    for (let i = 0; i < unitList.length; i++) {
        array.push(getChilds(unitList[i]));
    }
    setTree(array);
  }, [open]);

  useEffect(() => {
    console.warn('useEffect(() => { selectedList');
    // Network.selectedUnit = JSON.parse(JSON.stringify(selectedList));
  }, [selectedList]);

  useEffect(() => {
    setUnits(unitList);
    let array = [];
    for (let i = 0; i < unitList.length; i++) {
        array.push(getChilds(unitList[i]));
    }
    setTree(array);
  }, [unitList, selectedUnitId]);

  return (
        <Modal
            open={showModal}
            onClose={() => {
                // if (onClose) {
                //     onClose();
                // }
            }}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <Typography sx={{
                    mb: 2,
                }} id="modal-modal-title" variant="h4" component="h2">
                    Выберите подразделения
                </Typography>
                <TreeView
                    aria-label="icon expansion"
                    defaultCollapseIcon={<ExpandMoreIcon />}
                    defaultExpandIcon={<ChevronRightIcon />}
                    sx={{ height: 240, flexGrow: 1, maxWidth: '100%', overflowY: 'auto' }}
                >
                    {tree}
                </TreeView>
                <Button variant="contained" color="primary" onClick={() => {
                  if (onSelectUnit) {
                      onSelectUnit(Network.selectedUnit);
                  }
                }}>
                    ОК
                </Button>
            </Box>
        </Modal>);
};

export default UnitSelectModalView;
