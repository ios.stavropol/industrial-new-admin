import React, { useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from '@material-ui/core';

const TasksTable = ({ tasks, ...rest }) => {
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const getDate = input => {
    let date = new Date(input);
    let dd = date.getDate();
    dd = dd < 10 ? '0' + dd : dd;

    let mm = date.getMonth() + 1;
    mm = mm < 10 ? '0' + mm : mm;

    let yyyy = date.getFullYear();

    return dd + '.' + mm + '.' + yyyy;
  }

  return (
    <Card {...rest}>
        <Box>
          <Table size='small'>
            <TableHead>
              <TableRow>
                <TableCell>
                  ID
                </TableCell>
                <TableCell>
                  Дата
                </TableCell>
                <TableCell>
                  Бизнес-процесс
                </TableCell>
                <TableCell>
                  Цель
                </TableCell>
                <TableCell>
                  Статус
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {tasks.slice(page*limit, page*limit + limit).map((task, i) => {
                return (
                    <TableRow
                        hover
                        key={task.id}
                        selected={selectedCustomerIds.indexOf(task.id) !== -1}
                    >
                      <TableCell style={{
                          width: '5%'
                      }}>
                          {page*limit + i + 1}
                      </TableCell>
                      <TableCell>
                          {getDate(task.date)}
                      </TableCell>
                      <TableCell>
                          {task.status === 'ended' ? task.process + ' - ' + task.checklist : (<a href={'/company/tasks/'+task.id}>{task.process + ' - ' + task.checklist}</a>)}
                      </TableCell>
                      <TableCell>
                          {task.target}
                      </TableCell>
                      <TableCell>
                          {task.status === 'ended' ? 'Завершен' : (task.status === 'working' ? 'В работе' : 'Не пройден')}
                      </TableCell>
                    </TableRow>)
              })}
            </TableBody>
          </Table>
        </Box>
      <TablePagination
        component="div"
        labelRowsPerPage={'Показывать'}
        labelDisplayedRows={({ from, to, count }) => {
          return (from + '-' + to + ' из ' + count);
        }}
        count={tasks.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

TasksTable.propTypes = {
    tasks: PropTypes.array.isRequired
};

export default TasksTable;
