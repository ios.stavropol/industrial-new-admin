import React, { useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from '@material-ui/core';
import getInitials from '../../utils/getInitials';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';

const CompanyStaffTable = ({ customers, companies, onEditStaff, onDeleteStaff, ...rest }) => {
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Card {...rest}>
        <Box>
          <Table size='small'>
            <TableHead>
              <TableRow>
                <TableCell>
                  ID
                </TableCell>
                <TableCell>
                  Название
                </TableCell>
                <TableCell>
                  Операции
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {customers.slice(page*limit, page*limit + limit).map((customer, i) => {
                return (
                    <TableRow
                    hover
                    key={customer.id}
                    selected={selectedCustomerIds.indexOf(customer.id) !== -1}
                    >
                    <TableCell style={{
                        width: '5%'
                    }}>
                        {page*limit + i + 1}
                    </TableCell>
                    <TableCell>
                        {customer.name}
                    </TableCell>
                    <TableCell style={{
                      minWidth: '10rem',
                    }}>
                        <IconButton onClick={() => {
                            if (onEditStaff) {
                                onEditStaff(customer);
                            }
                        }}>
                            <EditIcon />
                        </IconButton>
                        <IconButton onClick={() => {
                            if (onDeleteStaff) {
                                onDeleteStaff(customer);
                            }
                        }}>
                            <DeleteIcon />
                        </IconButton>
                    </TableCell>
                    </TableRow>)
              })}
            </TableBody>
          </Table>
        </Box>
      <TablePagination
        component="div"
        labelRowsPerPage={'Показывать'}
        labelDisplayedRows={({ from, to, count }) => {
          return (from + '-' + to + ' из ' + count);
        }}
        count={customers.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

CompanyStaffTable.propTypes = {
  customers: PropTypes.array.isRequired
};

export default CompanyStaffTable;
