import React, { useEffect } from 'react';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import UsersTable from './UsersTable';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import { setUserStatus, getCompanyUsers, updateCompanyUser, getCompanyStaff, getCompanyUnitsList, getAllCompanyUnits, deleteCompanyUser } from '../../helpers/Network';
import UserEditModalView from './UserEditModalView';
import UnitSelectModalView from './UnitSelectModalView';
import UserFilter from './../AuditInfo/UserFilter';
import swal from 'sweetalert';
import Config from '../../constants/Config';

const UsersView = (props) => {

  const location = useLocation();
  const navigate = useNavigate();

  const [users, setUsers] = React.useState([]);
  const [usersAll, setUsersAll] = React.useState([]);
  const [usersFiltered, setUsersFiltered] = React.useState([]);
  const [staff, setStaff] = React.useState([]);
  const [units, setUnits] = React.useState([]);
  const [unitRaw, setUnitRaw] = React.useState([]);
  const [selectedUser, setSelectedUser] = React.useState(null);
  const [showModal, setShowModal] = React.useState(false);
  const [showUnitModal, setShowUnitModal] = React.useState(false);

  const [searchText, setSearchText] = React.useState('');
  const [categoryText, setCategoryText] = React.useState('');
  const [searchUnits, setSearchUnits] = React.useState([]);

  const filterUser = () => {
    setUsersFiltered([]);
    let array = usersAll;
    if (searchText !== null && searchText.length) {
        array = array.filter(a => {
            return a.fio.toLowerCase().indexOf(searchText.toLowerCase()) !== -1;
        });
    }

    if (categoryText !== null && categoryText.length) {
        array = array.filter(a => {
            return a.roles.includes(categoryText);
        });
    }

    if (searchUnits.length) {
        array = array.filter(a => {
            return searchUnits.includes(a.unit_id);
        });
    }
    
    setUsersFiltered(array);
}

    useEffect(() => {
        filterUser();
    }, [searchText, categoryText, searchUnits]);

    useEffect(() => {
        filterUser();
    }, [usersAll]);

  useEffect(() => {
    getCompanyUsers(props.id)
    .then(users => {
      setUsers(users);
      setUsersFiltered(users);
        setUsersAll(users);
    })
    .catch(err => {

    });
    getCompanyStaff(props.id)
    .then(staff => {
        setStaff(staff);
    })
    .catch(err => {

    });
    getAllCompanyUnits(props.id)
    .then(units => {
        setUnits(units);
    })
    .catch(err => {

    });
    getCompanyUnitsList(props.id)
    .then(units => {
        setUnitRaw(units);
    })
    .catch(err => {

    });
  }, [location.pathname]);

  return (
    <Card>
        <CardHeader
            action={
                <Button variant="contained" color="primary" onClick={() => {
                    setSelectedUser({
                        id: 0,
                        avatar: null,
                        firstname: null,
                        lastname: null,
                        patrname: null,
                        roles: [],
                        staff_id: null,
                        status: 10,
                        unit_id: null,
                        company_id: props.id,
                        email: null,
                    });
                    setShowModal(true);
                }}>
                    Добавить сотрудника
                </Button>
            }
        />
        <CardContent>
            <UserFilter
                searchText={searchText}
                categoryText={categoryText}
                unitsList={units}
                unitsRawList={unitRaw}
                onSearch={text => {
                    setSearchText(text);
                }}
                onCategorySelect={cat => {
                    setCategoryText(cat);
                }}
                onUnitSelect={units => {
                    setSearchUnits(units);
                }}
            />
            <UsersTable
                customers={usersFiltered}
                unitList={unitRaw}
                onStatusChanged={customer => {
                    let status;
                    if (customer.status === 10) {
                        status = 9;
                    } else {
                        status = 10;
                    }

                    setUserStatus(customer.id, status)
                    .then(() => {
                        getCompanyUsers(props.id)
                        .then(users => {
                            setUsers(users);
                            setUsersAll(users);
                            // filterUser();
                        })
                        .catch(err => {
                            
                        });
                    })
                    .catch(err => {
                        swal(Config.appName, err, "warning");
                    });
                }}
                onEditUser={customer => {
                    setSelectedUser(customer);
                    setShowModal(true);
                }}
                onDeleteUser={customer => {
                    swal({
                        title: Config.appName,
                        text: 'Вы хотите удалить сотрудника?',
                        icon: "warning",
                        buttons: ['Нет', 'Да'],
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            deleteCompanyUser(customer.id, customer.company_id)
                            .then(() => {
                                getCompanyUsers(props.id)
                                .then(users => {
                                    setUsers(users);
                                    setUsersAll(users);
                                    // filterUser();
                                })
                                .catch(err => {
                                    
                                });
                            })
                            .catch(err => {
                                swal(Config.appName, err, "warning");
                            });
                        }
                    });
                }}
            />
        </CardContent>
        <UserEditModalView
            open={showModal}
            staffList={staff}
            unitList={unitRaw}
            currentUser={selectedUser}
            onOpenUnit={user => {
                setSelectedUser(user);
                setShowUnitModal(true);
            }}
            onSuccess={() => {
                setShowModal(false);
                getCompanyUsers(props.id)
                .then(users => {
                    setUsers(users);
                    setUsersAll(users);
                    // filterUser();
                })
                .catch(err => {

                });
                getCompanyStaff(props.id)
                .then(staff => {
                    setStaff(staff);
                })
                .catch(err => {

                });
            }}
            onClose={() => {
                setShowModal(false);
            }}
        />
        <UnitSelectModalView
            open={showUnitModal}
            unitList={units}
            onSelectUnit={(unitId, unitText) => {
                let user = JSON.parse(JSON.stringify(selectedUser));
                user.unit_id = unitId;
                setSelectedUser(user);
                setShowUnitModal(false); 
            }}
        />
    </Card>);
};

export default UsersView;
