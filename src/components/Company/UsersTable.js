import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  Switch
} from '@material-ui/core';
import Config from './../../constants/Config';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';

const UsersTable = ({ customers, unitList, onDeleteUser, onEditUser, onStatusChanged, ...rest }) => {
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);
  const [units, setUnits] = useState([]);

  useEffect(() => {
      setUnits(unitList);
  }, [unitList]);

  const handleSelectAll = (event) => {
    let newSelectedCustomerIds;

    if (event.target.checked) {
      newSelectedCustomerIds = customers.map((customer) => customer.id);
    } else {
      newSelectedCustomerIds = [];
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedCustomerIds.indexOf(id);
    let newSelectedCustomerIds = [];

    if (selectedIndex === -1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(selectedCustomerIds, id);
    } else if (selectedIndex === 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(selectedCustomerIds.slice(1));
    } else if (selectedIndex === selectedCustomerIds.length - 1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(selectedCustomerIds.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, selectedIndex),
        selectedCustomerIds.slice(selectedIndex + 1)
      );
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Card {...rest}>
        <Box sx={{
          // minWidth: 1050,
        }}>
          <Table size='small'>
            <TableHead>
              <TableRow>
                <TableCell>
                  ID
                </TableCell>
                <TableCell>
                  ФИО
                </TableCell>
                <TableCell>
                  Подразделение
                </TableCell>
                <TableCell>
                  Роли
                </TableCell>
                <TableCell>
                  Активный
                </TableCell>
                <TableCell>
                  Операции
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {customers.slice(page*limit, page*limit + limit).map((customer) => {
                return (<TableRow
                  hover
                  key={customer.id}
                  selected={selectedCustomerIds.indexOf(customer.id) !== -1}
                >
                  <TableCell style={{
                    width: '5%'
                  }}>
                    {customer.id}
                  </TableCell>
                  <TableCell>
                    {customer.fio}
                  </TableCell>
                  <TableCell style={{
                    // width: '5%'
                  }}>
                    {units.find(el => el.id === customer.unit_id) !== undefined ? units.find(el => el.id === customer.unit_id).name : ''}
                  </TableCell>
                  <TableCell style={{
                    // width: '5%'
                  }}>
                    {(customer.roles.includes('admin_company') ? 'Администратор ' : '') + (customer.roles.includes('auditor') ? 'Аудитор ' : '') + (customer.roles.includes('statist') ? 'Статист ' : '')}
                  </TableCell>
                  <TableCell style={{
                    // width: '5%'
                  }}>
                    <Switch
                        checked={customer.status === 10 ? true : false}
                        onChange={checked => {
                            if (onStatusChanged) {
                                onStatusChanged(customer);
                            }
                        }}
                        defaultChecked={customer.status === 10 ? true : false} 
                    />
                  </TableCell>
                  <TableCell style={{
                    width: '15%'
                  }}>
                    <IconButton onClick={() => {
                        if (onEditUser) {
                            onEditUser(customer);
                        }
                    }}>
                        <EditIcon />
                    </IconButton>
                    <IconButton onClick={() => {
                        if (onDeleteUser) {
                            onDeleteUser(customer);
                        }
                    }}>
                        <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              )})}
            </TableBody>
          </Table>
        </Box>
      <TablePagination 
        component="div"
        labelRowsPerPage={'Показывать'}
        labelDisplayedRows={({ from, to, count }) => {
          return (from + '-' + to + ' из ' + count);
        }}
        count={customers.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

UsersTable.propTypes = {
  customers: PropTypes.array.isRequired
};

export default UsersTable;
