import React, { useEffect } from 'react';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import { Box, 
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Typography, Switch, Modal, Autocomplete, FormControl, TextField, Select, OutlinedInput, MenuItem } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { updateCompanyUser } from '../../helpers/Network';
import Config from '../../constants/Config';
import swal from 'sweetalert';
import ImageUploader from '../ImageUploader';
import { useTheme } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    border: '2px solid white',
    boxShadow: 24,
    p: 4,
};

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
    //   width: '100%',
    },
  },
};

const categoryOptions = [
    {value: 'top-manager',      label: 'Топ менеджер'},
    {value: 'middle-manager',   label: 'Менеджер среднего звена'},
    {value: 'specialist',       label: 'Специалист'},
    {value: 'worker',           label: 'Рабочий'},
];

const rolesOptions = [
    {value: 'admin_company',    label: 'Администратор компании'},
    {value: 'auditor',          label: 'Аудитор'},
    {value: 'statist',          label: 'Статист'},
];

const UserEditModalView = ({open, onClose, onSuccess, onOpenUnit, currentUser, unitList, staffList}) => {

  const location = useLocation();
  const navigate = useNavigate();

  const theme = useTheme();
  const [users, setUsers] = React.useState([]);
  const [showModal, setShowModal] = React.useState(false);

  const [userId, setUserId] = React.useState(0);
  const [lastname, setLastname] = React.useState('');
  const [firstname, setFirstname] = React.useState('');
  const [patrname, setPatrname] = React.useState('');
  const [imagePreviewUrl, setImagePreviewUrl] = React.useState(null);
  const [imageChanged, setImageChanged] = React.useState(false);
  const [email, setEmail] = React.useState('');
  const [unitText, setUnitText] = React.useState('');
  const [shrink, setShrink] = React.useState(false);
  const [unitId, setUnitId] = React.useState(null);
  const [category, setCategory] = React.useState(null);
  const [unit, setUnit] = React.useState(null);
  const [staff, setStaff] = React.useState(null);
  const [staffOption, setStaffOption] = React.useState(null);
  const [unitOption, setUnitOption] = React.useState(null);
  const [role, setRole] = React.useState([]);
  const [status, setStatus] = React.useState(10);

  useEffect(() => {
    setShowModal(open);
    setImageChanged(false);
    setImagePreviewUrl(null);
  }, [open]);

  useEffect(() => {
    setStaffOption(staffList);
  }, [staffList]);

  useEffect(() => {
    if (currentUser !== null && currentUser !== undefined) {
        setUserId(currentUser.id);
        setStatus(currentUser.status);
        setLastname(currentUser.lastname);
        setFirstname(currentUser.firstname);
        setPatrname(currentUser.patrname);
        setEmail(currentUser.email);

        if (currentUser.avatar !== null && currentUser.avatar.length) {
            setImagePreviewUrl(Config.apiDomain + '/uploads/user/' + currentUser.avatar);
        } else {
            setImagePreviewUrl(null);
        }

        setUnitId(null);
        setUnitText(null);

        for (let i = 0; i < unitOption.length; i++) {
            if (currentUser.unit_id === unitOption[i].id) {
                setUnitId(unitOption[i].id);
                setUnitText(unitOption[i].name);
                setShrink(true);
                break;
            }
        }

        setStaff(null);
        for (let i = 0; i < staffOption.length; i++) {
            if (currentUser.staff_id === staffOption[i].id) {
                setStaff(staffOption[i]);
                break;
            }
        }

        setCategory(null);
        for (let i = 0; i < categoryOptions.length; i++) {
            if (currentUser.roles.includes(categoryOptions[i].value)) {
                setCategory(categoryOptions[i]);
                break;
            }
        }

        setRole([]);
        let array = [];
        for (let i = 0; i < rolesOptions.length; i++) {
            if (currentUser.roles.includes(rolesOptions[i].value)) {
                array.push(rolesOptions[i].value);
            }
        }
        setRole(array);
    }
  }, [currentUser]);

  useEffect(() => {
    setUnitOption(unitList);
  }, [unitList]);

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    for (let i = 0; i < categoryOptions.length; i++) {
        if (value === categoryOptions[i].value) {
            setCategory(categoryOptions[i]);
            break;
        }
    }
  };

  const roleChange = (event) => {
    const {
        target: { value },
    } = event;
    setRole(
        typeof value === 'string' ? value.split(',') : value,
    );
  };

  return (
    <Dialog
        open={showModal}
        onClose={(event, reason) => {
            if (reason !== 'backdropClick') {
                
            }
        }}
        fullWidth
        maxWidth="sm"
    >
        <DialogTitle>
            <Typography variant="h4">
                {userId !== null && userId === 0 ? 'Добавление сотрудника' : 'Редактирование сотрудника'}
            </Typography>
            <IconButton
                aria-label="close"
                onClick={() => {
                    if (onClose) {
                        onClose();
                    }
                }}
                sx={{
                    position: 'absolute',
                    right: 8,
                    top: 8,
                    color: (theme) => theme.palette.grey[500],
                }}
                >
                <CloseIcon />
            </IconButton>
        </DialogTitle>
        <DialogContent>
            <Box display="flex" flexDirection="row" alignItems="flex-end" justifyContent="space-between" p={0} m={0}>
                <ImageUploader 
                onChangeImage={file => {
                    setImageChanged(true);
                    setImagePreviewUrl(file.result);
                }}
                onDeleteImage={() => {
                    setImageChanged(true);
                    setImagePreviewUrl(null);
                }}
                image={imagePreviewUrl}
                />
                <Box display="flex" flexDirection="row" alignItems="center" justifyContent="space-between" p={0} m={0}>
                    <div>Активный</div>
                    <Switch
                        checked={status === 10 ? true : false}
                        onChange={checked => {
                            if (checked.target.checked) {
                                setStatus(10);
                            } else {
                                setStatus(9);
                            }
                        }}
                        defaultChecked={status === 10 ? true : false} 
                    />
                </Box>
            </Box>
            <FormControl fullWidth sx={{
                m: 0,
                marginTop: 2,
                marginBottom: 1,
            }}>
                <TextField
                    style={{
                        // height: 30,
                    }}
                    id="lastname2"
                    label="Фамилия"
                    variant="outlined"
                    value={lastname}
                    size="small"
                    onChange={event => {
                        setLastname(event.target.value);
                    }}
                />
            </FormControl>
            <FormControl fullWidth sx={{
                m: 0,
                // marginTop: 2,
                marginBottom: 1,
            }}>
                <TextField
                    id="firstname2"
                    label="Имя"
                    variant="outlined"
                    value={firstname}
                    size="small"
                    onChange={event => {
                        setFirstname(event.target.value);
                    }}
                />
            </FormControl>
            <FormControl fullWidth sx={{
                m: 0,
                // marginTop: 2,
                marginBottom: 1,
            }}>
                <TextField
                    id="patrname2"
                    label="Отчество"
                    variant="outlined"
                    value={patrname}
                    size="small"
                    onChange={event => {
                        setPatrname(event.target.value);
                    }}
                />
            </FormControl>
            <FormControl fullWidth sx={{
                m: 0,
                // marginTop: 2,
                marginBottom: 1,
            }}>
                <TextField
                    id="email2"
                    label="E-mail"
                    variant="outlined"
                    value={email}
                    size="small"
                    onChange={event => {
                        setEmail(event.target.value);
                    }}
                />
            </FormControl>
            <FormControl fullWidth sx={{
                m: 0,
                // marginTop: 2,
                marginBottom: 1,
            }}>
                <TextField
                    id="unit2"
                    label="Подразделение"
                    variant="outlined"
                    value={unitText}
                    InputLabelProps={{ shrink: shrink }}
                    size="small"
                    onChange={event => {
                        // setUnitText(event.target.value);
                    }}
                />
                <div style={{
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    right: 0,
                    bottom: 0,
                    backgroundColor: 'transparent',
                }}
                onClick={() => {
                    if (onOpenUnit) {
                        let roles = [];
                        if (category !== null) {
                            roles.push(category.value);
                        }
                        if (role !== null) {
                            roles.push(role.value);
                        }
                        onOpenUnit({
                            id: userId,
                            avatar: imagePreviewUrl,
                            firstname: firstname,
                            lastname: lastname,
                            patrname: patrname,
                            roles: roles,
                            staff_id: staff !== null ? staff.id : null,
                            status: 10,
                            unit_id: unitId,
                            company_id: currentUser.company_id,
                            email: email,
                        });
                    }
                }}
                />
            </FormControl>
            <FormControl fullWidth sx={{
                m: 0,
                // marginTop: 2,
                marginBottom: 1,
            }}>
                <Autocomplete
                    value={staff}
                    size="small"
                    onChange={(event, newValue) => {
                        if (newValue && newValue.id === 0) {
                            setStaff({
                                id: newValue.id,
                                name: newValue.raw,
                            });
                            // setIsNew(true);
                        } else {
                            // render2Answer(newValue);
                            setStaff(newValue);
                            // setIsNew(false);
                        }
                    }}
                    filterOptions={(options, params) => {
                        let array = options.filter(a => {
                            return a.name.toLowerCase().indexOf(params.inputValue.toLowerCase()) !== -1;
                        });

                        let filtered = [];
                        for (let i=0;i<array.length;i++) {
                            filtered.push(array[i]);
                        }
                        if (params.inputValue !== '') {
                            filtered.push({
                                id: 0,
                                name: `Создать "${params.inputValue}"`,
                                raw: params.inputValue,
                            });
                        }
                        // console.warn(JSON.stringify(filtered));
                        return filtered;
                    }}
                    selectOnFocus
                    clearOnBlur
                    handleHomeEndKeys
                    id="free-solo-with-text-demo"
                    options={staffOption}
                    getOptionLabel={(option) => {
                        if (typeof option === 'string') {
                            return option;
                        }
                        return option.name;
                    }}
                    style={{
                        zIndex: 1000,
                    }}
                    freeSolo
                    renderInput={(params) => (
                        <TextField {...params} label="Должность" variant="outlined" />
                    )}
                />
            </FormControl>
            <FormControl sx={{
                m: 0,
                width: '100%',
                // mt: 1,
                mb: 1,
            }}>
                <Select
                    displayEmpty
                    size="small"
                    placeholder={'Категория'}
                    value={category}
                    onChange={handleChange}
                    input={<OutlinedInput />}
                    renderValue={(selected) => {
                        if (selected === null || selected.length === 0) {
                            return 'Категория';
                        }
                        return selected.label;
                        let str = '';

                        for (let i=0;i<categoryOptions.length;i++) {
                            if (selected.includes(categoryOptions[i].value)) {
                                str = str + categoryOptions[i].label;
                            }
                        }
                        return str;//selected.join(', ');
                    }}
                    MenuProps={MenuProps}
                    inputProps={{ 'aria-label': 'Without label' }}
                >
                <MenuItem disabled value="">
                    <em>Категория</em>
                </MenuItem>
                {categoryOptions.map((name) => (
                    <MenuItem
                        key={name.value}
                        value={name.value}
                        style={{
                            fontWeight: theme.typography.fontWeightMedium,
                        }}
                    >
                        {name.label}
                    </MenuItem>
                ))}
                </Select>
            </FormControl>
            <FormControl sx={{
                m: 0,
                width: '100%',
                // mt: 1,
                // mb: 1,
            }}>
                <Select
                    displayEmpty
                    multiple
                    size="small"
                    value={role}
                    onChange={roleChange}
                    input={<OutlinedInput />}
                    renderValue={(selected) => {
                        if (selected.length === 0) {
                            return <em>Роли</em>;
                        }
                        
                        let str = '';

                        for (let i=0;i<rolesOptions.length;i++) {
                            if (selected.includes(rolesOptions[i].value)) {
                                str = str + rolesOptions[i].label + ', ';
                            }
                        }
                        return str;
                    }}
                    MenuProps={MenuProps}
                    inputProps={{ 'aria-label': 'Without label' }}
                >
                <MenuItem disabled value="">
                    <em>Роли</em>
                </MenuItem>
                {rolesOptions.map((name) => (
                    <MenuItem
                        key={name.value}
                        value={name.value}
                        style={{
                            fontWeight: theme.typography.fontWeightMedium,
                        }}
                    >
                        {name.label}
                    </MenuItem>
                ))}
                </Select>
            </FormControl>
        </DialogContent>
        <DialogActions>
            <Button variant="contained" color="primary" onClick={() => {
                if (lastname === null || lastname.length === 0) {
                    swal(Config.appName, 'Укажите фамилию!', "warning");
                    return;
                }
                if (firstname === null || firstname.length === 0) {
                    swal(Config.appName, 'Укажите имя!', "warning");
                    return;
                }
                if (email === null || email.length === 0) {
                    swal(Config.appName, 'Укажите почту!', "warning");
                    return;
                }
                if (unitId === null) {
                    swal(Config.appName, 'Укажите подразделение!', "warning");
                    return;
                }
                if (staff === null) {
                    swal(Config.appName, 'Укажите должность!', "warning");
                    return;
                }
                if (category === null) {
                    swal(Config.appName, 'Укажите категорию!', "warning");
                    return;
                }

                let avatar = null;

                if (imageChanged) {
                    avatar = imagePreviewUrl;
                }

                let roles = [category.value];

                if (role !== null) {
                    roles = roles.concat(role);
                }

                updateCompanyUser(userId, lastname, firstname, patrname, email, avatar, roles, currentUser.company_id, staff.id, staff.name, unitId, status)
                .then(() => {
                    if (onSuccess) {
                        onSuccess();
                    }
                })
                .catch(err => {
                    swal(Config.appName, err, "warning");
                });
            }}>
                Сохранить
            </Button>
        </DialogActions>
    </Dialog>);
};

export default UserEditModalView;
