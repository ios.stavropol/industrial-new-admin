import React, { useEffect } from 'react';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  Typography
} from '@material-ui/core';
import ImageUploader from '../ImageUploader';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import { getCompany, saveCompany } from '../../helpers/Network';
import Config from '../../constants/Config';
import swal from 'sweetalert';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Link as RouterLink, useNavigate } from 'react-router-dom';

const CommonInfo = (props) => {

    const navigate = useNavigate();

    const [imagePreviewUrl, setImagePreviewUrl] = React.useState(null);
    const [imageChanged, setImageChanged] = React.useState(false);
    const [name, setName] = React.useState(props.company !== null ? props.company.name : '');
    const [address, setAddress] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [phone, setPhone] = React.useState('');
    const [open, setOpen] = React.useState(false);

    useEffect(() => {
    // //   getCompany(props.id)
    // //   .then(company => {
        if (props.company !== null) {
            setName(props.company.name);
            setAddress(props.company.address);
            setEmail(props.company.email);
            setPhone(props.company.phone);
            setImageChanged(false);
            setImagePreviewUrl(((props.company.avatar !== null && props.company.avatar.length) ? Config.apiDomain + '/uploads/company/' + props.company.avatar : null));
        }
        
    //   })
    //   .catch(err => {

    //   });
    }, [props.company]);

    return (<Card
        sx={{ height: '100%' }}
      >
        <CardContent>
            <ImageUploader 
                onChangeImage={file => {
                    setImageChanged(true);
                    setImagePreviewUrl(file.result);
                }}
                onDeleteImage={() => {
                    setImageChanged(true);
                    setImagePreviewUrl(null);
                }}
                image={imagePreviewUrl}
            />
            <FormControl fullWidth sx={{
                    m: 0,
                    marginTop: 2,
                }}>
                <TextField
                    id="name"
                    size='small'
                    label="Название"
                    variant="outlined"
                    value={name}
                    onChange={event => {
                        setName(event.target.value);
                    }}
                />
            </FormControl>
            <FormControl fullWidth sx={{
                m: 0,
                marginTop: 2,
            }}>
                <TextField
                    id="address"
                    size='small'
                    label="Адрес"
                    variant="outlined"
                    value={address}
                    onChange={event => {
                        setAddress(event.target.value);
                    }}
                />
            </FormControl>
            <FormControl fullWidth sx={{
                m: 0,
                marginTop: 2,
            }}>
                <TextField
                    id="email"
                    size='small'
                    label="E-mail"
                    variant="outlined"
                    value={email}
                    onChange={event => {
                        setEmail(event.target.value);
                    }}
                />
            </FormControl>
            <FormControl fullWidth sx={{
                m: 0,
                marginTop: 2,
                marginBottom: 2,
            }}>
                <TextField
                    id="phone"
                    size='small'
                    label="Телефон"
                    variant="outlined"
                    value={phone}
                    onChange={event => {
                        setPhone(event.target.value);
                    }}
                />
            </FormControl>
            <Button variant="contained" color="primary" onClick={() => {

                setOpen(true);
                let avatar = null;

                if(imageChanged) {
                    avatar = imagePreviewUrl;
                }

                saveCompany(props.id, name, address, email, phone, avatar)
                .then(() => {
                    setOpen(false);
                    if (props.id == 0) {
                        // window.history.back();
                        navigate('/admin/companies', { replace: true });
                    }
                })
                .catch(err => {
                    setOpen(false);
                    swal(Config.appName, err, "warning");
                });
            }}>
                Сохранить
            </Button>
        </CardContent>
        <Backdrop
            sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
            open={open}
            onClick={() => {

            }}
        >
            <CircularProgress color="inherit" />
        </Backdrop>
      </Card>);
};

export default CommonInfo;
