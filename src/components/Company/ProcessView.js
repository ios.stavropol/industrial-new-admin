import React, { useEffect } from 'react';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  Typography,
  Modal,
  FormControl,
  Checkbox,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem, { useTreeItem } from '@material-ui/lab/TreeItem';
import clsx from 'clsx';
import Network, { getCompanyProcess, setCompanyProcess } from '../../helpers/Network';
import Config from '../../constants/Config';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    // border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const CustomContent = React.forwardRef(function CustomContent(props, ref) {
    const {
      classes,
      className,
      label,
      nodeId,
      icon: iconProp,
      expansionIcon,
      displayIcon,
      children,
    } = props;
  
    const {
      disabled,
      expanded,
      selected,
      focused,
      handleExpansion,
      handleSelection,
      preventSelection,
    } = useTreeItem(nodeId);
  
    const icon = iconProp || expansionIcon || displayIcon;
  
    const handleMouseDown = (event) => {
      preventSelection(event);
    };
  
    const handleExpansionClick = (event) => {
      // console.warn('handleExpansionClick: ', event);
      let array = JSON.parse(JSON.stringify(Network.expanded));
      if (array.includes(event)) {
        console.warn('handleExpansionClick: includes', event);
        for (let i = 0; i < array.length; i++) {
          if (array[i] == event) {
            array.splice(i ,1);
          }
        }
      } else {
        console.warn('handleExpansionClick: not includes', event);
        array.push(event);
      }
      // setExpanded(array);
      if (Network.onExpandedChange) {
        Network.onExpandedChange(array);
      }
      
      handleExpansion(event);
    };
  
    const handleSelectionClick = (event) => {
      handleSelection(event);
    };

    let checked = JSON.parse(nodeId).selected;
  
    return (
      // eslint-disable-next-line jsx-a11y/no-static-element-interactions
      <div
        className={clsx(className, classes.root, {
          [classes.expanded]: expanded,
          [classes.selected]: selected,
          [classes.focused]: focused,
          [classes.disabled]: disabled,
        })}
        onMouseDown={handleMouseDown}
        ref={ref}
      >
        {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
        <div onClick={() => {
          // let data = JSON.parse(nodeId);
          handleExpansionClick(nodeId);
        }} className={classes.iconContainer}>
          {icon}
        </div>
        <Typography
          onClick={handleSelectionClick}
          component="div"
          className={classes.label}
        >
          {label}
        </Typography>
        <Checkbox checked={checked}
            onChange={event => {
                let data = JSON.parse(nodeId);
                if (Network.onSelectProcess) {
                    Network.onSelectProcess(data, event.target.checked);
                }
            }}
        />
      </div>
    );
  });
  
  CustomContent.propTypes = {
    /**
     * Override or extend the styles applied to the component.
     */
    classes: PropTypes.object.isRequired,
    /**
     * className applied to the root element.
     */
    className: PropTypes.string,
    /**
     * The icon to display next to the tree node's label. Either a parent or end icon.
     */
    displayIcon: PropTypes.node,
    /**
     * The icon to display next to the tree node's label. Either an expansion or collapse icon.
     */
    expansionIcon: PropTypes.node,
    /**
     * The icon to display next to the tree node's label.
     */
    icon: PropTypes.node,
    /**
     * The tree node label.
     */
    label: PropTypes.node,
    /**
     * The id of the node.
     */
    nodeId: PropTypes.string.isRequired,
    /**
     * The data of the node.
     */
     onEdit: PropTypes.func.isRequired,
  };
  
  const CustomTreeItem = (props) => {
      return (
        <TreeItem ContentComponent={CustomContent} {...props} />
        )
    };

const ProcessView = (props) => {

    const navigate = useNavigate();

    const [tree, setTree] = React.useState([]);
    const [expanded, setExpanded] = React.useState([]);
    const [process, setProcess] = React.useState([]);
    const [open, setOpen] = React.useState(false);
    const [showModal, setShowModal] = React.useState(false);
    const [name, setName] = React.useState('');
    const [parent_id, setParentId] = React.useState(0);
    const [selected_id, setSelectedId] = React.useState(0);

    const getChilds = (item) => {

        const isChildren = item.children !== null;
        if (isChildren) {
            return (
                <CustomTreeItem key={item.id} nodeId={JSON.stringify(item)} label={item.text + ' (' + item.checklists + ')'} onEdit={() => {
                    
                }}>
                    {item.children.map((node, idx) => getChilds(node))}
                </CustomTreeItem>
            );
        }
        return <CustomTreeItem key={item.id} nodeId={JSON.stringify(item)} label={item.text + ' (' + item.checklists + ')'}  onEdit={() => {
            
        }}/>;
    }

    const onExpandedChange = data => {
      Network.expanded = data;
      setTimeout(() => {
        setExpanded(data);
      }, 300);
    }

    const onSelectProcess = (data, checked) => {
        setCompanyProcess(props.id, data.id, checked)
        .then(() => {
            getCompanyProcess(props.id)
            .then(process => {
                setProcess(process);
                let array = [];
                let ids = [];
                for (let i = 0; i < process.length; i++) {
                    array.push(getChilds(process[i]));
                    ids.push(JSON.stringify(process[i]));
                }
                setTree(array);
                Network.expanded = ids;
                setExpanded(ids);
            })
            .catch(err => {

            });
        })
        .catch(err => {

        });
    }

    useEffect(() => {
        Network.onSelectProcess = onSelectProcess;
        Network.onExpandedChange = onExpandedChange;
        getCompanyProcess(props.id)
        .then(process => {
            setProcess(process);
            let array = [];
            let ids = [];
            for (let i = 0; i < process.length; i++) {
                array.push(getChilds(process[i]));
                ids.push(JSON.stringify(process[i]));
            }
            setTree(array);
            Network.expanded = ids;
            setExpanded(ids);
        })
        .catch(err => {

        });
    }, [props.id]);

    return (<Card
        sx={{ height: '100%' }}
      >
        <CardContent>
            <TreeView
                aria-label="icon expansion"
                defaultCollapseIcon={<ExpandMoreIcon />}
                defaultExpandIcon={<ChevronRightIcon />}
                expanded={expanded}
                sx={{ height: '400px', flexGrow: 1, maxWidth: '100%', overflowY: 'auto' }}
            >
                {tree}
            </TreeView>
        </CardContent>
        <Backdrop
            sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
            open={open}
            onClick={() => {

            }}
        >
            <CircularProgress color="inherit" />
        </Backdrop>
      </Card>);
};

export default ProcessView;
