import React, { useEffect } from 'react';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import { Box, Typography, Modal, TextField, Select, OutlinedInput, MenuItem } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem, { useTreeItem } from '@material-ui/lab/TreeItem';
import clsx from 'clsx';
import Network from '../../helpers/Network';
import { useTheme } from '@material-ui/core/styles';
import swal from 'sweetalert';
import Config from '../../constants/Config';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 700,
    bgcolor: 'background.paper',
    border: '2px solid white',
    boxShadow: 24,
    p: 4,
};

const CustomContent = React.forwardRef(function CustomContent(props, ref) {
    const {
      classes,
      className,
      label,
      nodeId,
      icon: iconProp,
      expansionIcon,
      displayIcon,
      children,
    } = props;
  
    const {
      disabled,
      expanded,
      selected,
      focused,
      handleExpansion,
      handleSelection,
      preventSelection,
    } = useTreeItem(nodeId);
  
    const icon = iconProp || expansionIcon || displayIcon;
  
    const handleMouseDown = (event) => {
      preventSelection(event);
    };
  
    const handleExpansionClick = (event) => {
      handleExpansion(event);
    };
  
    const handleSelectionClick = (event) => {
        if (Network.selectUnit) {
            Network.selectUnit(JSON.parse(nodeId));
        }
      handleSelection(event);
    };
  
    return (
      // eslint-disable-next-line jsx-a11y/no-static-element-interactions
      <div
        className={clsx(className, classes.root, {
          [classes.expanded]: expanded,
          [classes.selected]: selected,
          [classes.focused]: focused,
          [classes.disabled]: disabled,
        })}
        onMouseDown={handleMouseDown}
        ref={ref}
      >
        {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
        <div onClick={handleExpansionClick} className={classes.iconContainer}>
          {icon}
        </div>
        <Typography
          onClick={handleSelectionClick}
          component="div"
          className={classes.label}
        >
          {label}
        </Typography>
      </div>
    );
  });
  
  CustomContent.propTypes = {
    /**
     * Override or extend the styles applied to the component.
     */
    classes: PropTypes.object.isRequired,
    /**
     * className applied to the root element.
     */
    className: PropTypes.string,
    /**
     * The icon to display next to the tree node's label. Either a parent or end icon.
     */
    displayIcon: PropTypes.node,
    /**
     * The icon to display next to the tree node's label. Either an expansion or collapse icon.
     */
    expansionIcon: PropTypes.node,
    /**
     * The icon to display next to the tree node's label.
     */
    icon: PropTypes.node,
    /**
     * The tree node label.
     */
    label: PropTypes.node,
    /**
     * The id of the node.
     */
    nodeId: PropTypes.string.isRequired,
    /**
     * The data of the node.
     */
     onEdit: PropTypes.func.isRequired,
  };
  
  const CustomTreeItem = (props) => {
      return (
        <TreeItem ContentComponent={CustomContent} {...props} />
        )
};

const UnitSelectModalView = ({open, onSelectUnit, unitList}) => {

  const location = useLocation();
  const navigate = useNavigate();

  const theme = useTheme();
  const [showModal, setShowModal] = React.useState(false);

  const [tree, setTree] = React.useState([]);
  const [units, setUnits] = React.useState([]);
  const [selectUnitId, setSelectUnitId] = React.useState(null);
  const [selectUnitText, setSelectUnitText] = React.useState(null);

  const getChilds = (item) => {

    const isChildren = item.children !== null;
        if (isChildren) {
            return (
                <CustomTreeItem key={item.id} nodeId={JSON.stringify(item)} label={item.text} onEdit={() => {
                    
                }}>
                    {item.children.map((node, idx) => getChilds(node))}
                </CustomTreeItem>
            );
        }
        return <CustomTreeItem key={item.id} nodeId={JSON.stringify(item)} label={item.text}  onEdit={() => {
            
        }}/>;
    }

    const selectUnit = data => {
        setSelectUnitId(data.id);
        setSelectUnitText(data.text);
    }

  useEffect(() => {
    setShowModal(open);
    Network.selectUnit = selectUnit;
    setSelectUnitId(null);
    setSelectUnitText(null);
  }, [open]);

  useEffect(() => {
    setUnits(unitList);
    let array = [];
    for (let i = 0; i < unitList.length; i++) {
        array.push(getChilds(unitList[i]));
    }
    setTree(array);
  }, [unitList]);

  return (
        <Modal
            open={showModal}
            onClose={() => {
                // if (onClose) {
                //     onClose();
                // }
            }}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <Typography sx={{
                    mb: 2,
                }} id="modal-modal-title" variant="h4" component="h2">
                    Выберите подразделение
                </Typography>
                <TreeView
                    aria-label="icon expansion"
                    defaultCollapseIcon={<ExpandMoreIcon />}
                    defaultExpandIcon={<ChevronRightIcon />}
                    sx={{ height: 240, flexGrow: 1, maxWidth: '100%', overflowY: 'auto' }}
                >
                    {tree}
                </TreeView>
                <Button variant="contained" color="primary" onClick={() => {
                    if (selectUnitId !== null && selectUnitText !== null) {
                        if (onSelectUnit) {
                            onSelectUnit(selectUnitId, selectUnitText);
                        }
                    } else {
                      swal(Config.appName, 'Выберите подразделение!', "warning");
                    }
                }}>
                    ОК
                </Button>
            </Box>
        </Modal>);
};

export default UnitSelectModalView;
