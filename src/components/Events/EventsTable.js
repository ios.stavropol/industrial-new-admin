import React, { useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from '@material-ui/core';
import getInitials from '../../utils/getInitials';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import Helper from './../../helpers/Helper';

const statuses = {
	'started': 'К выполнению',
	'working': 'Выполняется',
	'ended': 'Завершено',
};

const EventsTable = ({ customers, onEdit, ...rest }) => {
  
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Card {...rest}>
        <Box>
          <Table size='small'>
            <TableHead>
              <TableRow>
                <TableCell>
                  ID
                </TableCell>
                <TableCell>
                  Крайняя дата
                </TableCell>
				<TableCell>
                  Задача
                </TableCell>
				<TableCell>
                  Место
                </TableCell>
				<TableCell>
                  Статус
                </TableCell>
                <TableCell>
                  Операции
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {customers.slice(page*limit, page*limit + limit).map((customer, i) => {
                return (
                    <TableRow
						hover
						key={customer.id}
                    >
                    <TableCell style={{
                        width: '5%'
                    }}>
                        {page*limit + i + 1}
                    </TableCell>
                    <TableCell>
                        {Helper.fromDBDateToRusDateTime(customer.task_date)}
                    </TableCell>
					<TableCell>
                        {customer.task_text}
                    </TableCell>
					<TableCell>
                        {customer.dev_place}
                    </TableCell>
					<TableCell>
                        {statuses[customer.status]}
                    </TableCell>
                    <TableCell style={{
                      minWidth: '10rem',
                    }}>
                        {customer.status !== 'ended' ? (<IconButton onClick={() => {
                            if (onEdit) {
                                onEdit(customer);
                            }
                        }}>
                            <EditIcon />
                        </IconButton>) : null}
                    </TableCell>
                    </TableRow>)
              })}
            </TableBody>
          </Table>
        </Box>
      <TablePagination
        component="div"
        labelRowsPerPage={'Показывать'}
        labelDisplayedRows={({ from, to, count }) => {
          return (from + '-' + to + ' из ' + count);
        }}
        count={customers.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

EventsTable.propTypes = {
  customers: PropTypes.array.isRequired
};

export default EventsTable;
