import React, { useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import CopyAllIcon from '@material-ui/icons/CopyAll';
import IconButton from '@material-ui/core/IconButton';

const roleOptions = [
    {value: 'top-manager', label: 'Топ менеджер'},
    {value: 'middle-manager', label: 'Менеджер среднего звена'},
    {value: 'specialist', label: 'Специалист'},
    {value: 'worker', label: 'Рабочий'},
];

const ChecklistTable = ({ customers, process_id, onEditStaff, onCloneStaff, onDeleteStaff, ...rest }) => {
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);

  const handleSelectAll = (event) => {
    let newSelectedCustomerIds;

    if (event.target.checked) {
      newSelectedCustomerIds = customers.map((customer) => customer.id);
    } else {
      newSelectedCustomerIds = [];
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedCustomerIds.indexOf(id);
    let newSelectedCustomerIds = [];

    if (selectedIndex === -1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(selectedCustomerIds, id);
    } else if (selectedIndex === 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(selectedCustomerIds.slice(1));
    } else if (selectedIndex === selectedCustomerIds.length - 1) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(selectedCustomerIds.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelectedCustomerIds = newSelectedCustomerIds.concat(
        selectedCustomerIds.slice(0, selectedIndex),
        selectedCustomerIds.slice(selectedIndex + 1)
      );
    }

    setSelectedCustomerIds(newSelectedCustomerIds);
  };

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Card {...rest}>
        <Box sx={{ minWidth: 1050 }}>
          <Table size='small'>
            <TableHead>
              <TableRow>
                <TableCell>
                  ID
                </TableCell>
                <TableCell>
                  Название
                </TableCell>
                <TableCell>
                  Роль
                </TableCell>
                <TableCell>
                  Операции
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {customers.slice(page*limit, page*limit + limit).map((customer) => {
                let roles = '';
                let role = JSON.parse(customer.role);
                for (let i=0; i<roleOptions.length;i++) {
                    if (role.includes(roleOptions[i].value)) {
                        roles = roles + roleOptions[i].label + ', ';
                    }
                }
                return (
                    <TableRow
                    hover
                    key={customer.id}
                    selected={selectedCustomerIds.indexOf(customer.id) !== -1}
                    >
                    <TableCell style={{
                        width: '5%'
                    }}>
                        {customer.id}
                    </TableCell>
                    <TableCell>
                        <a href={'/admin/process/' + process_id + '/' + customer.id}>{customer.name}</a>
                    </TableCell>
                    <TableCell>
                        {roles}
                    </TableCell>
                    <TableCell style={{
                        // width: '15%'
                        minWidth: '10rem',
                    }}>
                        <IconButton onClick={() => {
                            if (onCloneStaff) {
                              onCloneStaff(customer);
                            }
                        }}>
                          <CopyAllIcon />
                        </IconButton>
                        <IconButton onClick={() => {
                            if (onEditStaff) {
                                onEditStaff(customer);
                            }
                        }}>
                          <EditIcon />
                        </IconButton>
                        <IconButton onClick={() => {
                            if (onDeleteStaff) {
                                onDeleteStaff(customer);
                            }
                        }}>
                            <DeleteIcon />
                        </IconButton>
                    </TableCell>
                  </TableRow>)
              })}
            </TableBody>
          </Table>
        </Box>
      <TablePagination
        component="div"
        labelRowsPerPage={'Показывать'}
        labelDisplayedRows={({ from, to, count }) => {
          return (from + '-' + to + ' из ' + count);
        }}
        count={customers.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

ChecklistTable.propTypes = {
  customers: PropTypes.array.isRequired
};

export default ChecklistTable;
