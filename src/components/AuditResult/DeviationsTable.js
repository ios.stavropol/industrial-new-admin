import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  Switch
} from '@material-ui/core';
import Config from './../../constants/Config';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';

const DeviationsTable = ({ id, customers, units, ...rest }) => {
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);
  const [unitArray, setUnitArray] = useState([]);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  useEffect(() => {
    setUnitArray(units);
  }, [units]);

  return (
    <Card {...rest}>
        <Box sx={{
          // minWidth: 1050,
        }}>
          <Table size='small'>
            <TableHead>
              <TableRow>
                <TableCell>
                  ID
                </TableCell>
                <TableCell>
                  Сотрудник
                </TableCell>
                <TableCell>
                  Отклонение
                </TableCell>
				<TableCell>
                  Описание
                </TableCell>
				<TableCell>
                  Место
                </TableCell>
                <TableCell>
                  Статус
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {customers.slice(page*limit, page*limit + limit).map((customer, i) => {
                  return (
                <TableRow
                  hover
                  key={customer.id}
                  selected={selectedCustomerIds.indexOf(customer.id) !== -1}
                >
                  <TableCell style={{
                    width: '5%'
                  }}>
                    {page*limit + i + 1}
                  </TableCell>
                  <TableCell>
                    <a href={'/company/audits/' + id + '/deviation/' + customer.dev_id}>{customer.fio}</a>
                  </TableCell>
                  <TableCell style={{
                    // width: '5%'
                  }}>
                    {customer.deviation}
                  </TableCell>
				  <TableCell style={{
                    // width: '5%'
                  }}>
                    {customer.description}
                  </TableCell>
				  <TableCell style={{
                    // width: '5%'
                  }}>
                    {customer.place}
                  </TableCell>
                  <TableCell style={{
                    // width: '5%'
                  }}>
                    {customer.status === 'ended' ? 'Завершено' : 'Зафиксировано'}
                  </TableCell>
                </TableRow>
              )})}
            </TableBody>
          </Table>
        </Box>
      <TablePagination
        component="div"
        labelRowsPerPage={'Показывать'}
        labelDisplayedRows={({ from, to, count }) => {
          return (from + '-' + to + ' из ' + count);
        }}
        count={customers.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

DeviationsTable.propTypes = {
  customers: PropTypes.array.isRequired
};

export default DeviationsTable;
