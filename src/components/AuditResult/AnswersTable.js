import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  Switch
} from '@material-ui/core';
import Config from './../../constants/Config';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';

const AnswersTable = ({ id, data, ...rest }) => {
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);
  const [unitArray, setUnitArray] = useState([]);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Card {...rest}>
        <Box sx={{
          // minWidth: 1050,
        }}>
          <Table size='small'>
            <TableHead>
              <TableRow>
                <TableCell>
                  ID
                </TableCell>
                <TableCell>
                  Вопрос
                </TableCell>
                <TableCell>
                  Всего
                </TableCell>
				<TableCell>
                  Положительные
                </TableCell>
				<TableCell>
                  Отрицательные
                </TableCell>
                <TableCell>
                  Без ответа
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.slice(page*limit, page*limit + limit).map((customer, i) => {
                  return (
                <TableRow
                  hover
                  key={customer.id}
                  selected={selectedCustomerIds.indexOf(customer.id) !== -1}
                >
                  <TableCell style={{
                    width: '5%',
					backgroundColor: customer.positive / customer.all <= 0.35 ? 'red' : (customer.positive / customer.all > 0.7 ? 'green' : 'yellow'),
                  }}>
                    {page*limit + i + 1}
                  </TableCell>
                  <TableCell>
                   {customer.question}
                  </TableCell>
                  <TableCell style={{
                    // width: '5%'
                  }}>
                    {customer.all}
                  </TableCell>
				  <TableCell style={{
                    // width: '5%'
                  }}>
                    {customer.positive}
                  </TableCell>
				  <TableCell style={{
                    // width: '5%'
                  }}>
                    {customer.negative}
                  </TableCell>
                  <TableCell style={{
                    // width: '5%'
                  }}>
                    {customer.non}
                  </TableCell>
                </TableRow>
              )})}
            </TableBody>
          </Table>
        </Box>
      <TablePagination
        component="div"
        labelRowsPerPage={'Показывать'}
        labelDisplayedRows={({ from, to, count }) => {
          return (from + '-' + to + ' из ' + count);
        }}
        count={data.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

AnswersTable.propTypes = {
  data: PropTypes.array.isRequired
};

export default AnswersTable;
