import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  Container,
  Typography,
  Modal,
  FormControl,
  TextField,
  CardHeader,
  Tab,
  Tabs,
} from '@material-ui/core';
import Network, { getAuditStatistic, getCompanyUnitsList } from '../../helpers/Network';
import Config from '../../constants/Config';
import { Link as RouterLink, useLocation, useNavigate, useParams } from 'react-router-dom';
import UsersTable from './../../components/AuditResult/UsersTable';
import PrescriptionTable from './../../components/AuditResult/PrescriptionTable';
import DeviationsTable from './../../components/AuditResult/DeviationsTable';
import EventsTable from './../../components/AuditResult/EventsTable';
import Dashboard from './../../components/AuditResult/Dashboard';
import PropTypes from 'prop-types';

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
  };
  
  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }

const ChecklistResult = ({ data, id, units }) => {

    const location = useLocation();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    useEffect(() => {
        console.warn(data);
    }, [location.pathname]);

  return (<Card sx={{
	mb: 2,
}}>
	<CardHeader title={
		(<Box sx={{
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      width: '100%',
    }}>
      <b>{data.name}</b>
      <Button variant="contained" color="primary" onClick={() => {
        window.open(Config.apiDomain + '/v1/audit/get-csv?id=' + data.checklist_id, "_blank");
      }}>Скачать вопросы</Button>
    </Box>)
	}/>
	<CardContent>
		<Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
			<Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
				<Tab label="Сотрудники" {...a11yProps(0)} />
				<Tab label="Статистика" {...a11yProps(1)} />
        <Tab label="Предписания" {...a11yProps(2)} />
        <Tab label="Отклонения" {...a11yProps(3)} />
        <Tab label="Мероприятия" {...a11yProps(4)} />
			</Tabs>
		</Box>
		<TabPanel value={value} index={0}>
			<UsersTable
				id={id}
				units={units}
				customers={data.users}
			/>
		</TabPanel>
    <TabPanel value={value} index={1}>
			<Dashboard
        id={id}
        data={data.questions}
        score={data.score}
        answers={data.answers}
      />
		</TabPanel>
    <TabPanel value={value} index={2}>
      <PrescriptionTable
        id={data.checklist_id}
				customers={data.prescriptions}
			/>
		</TabPanel>
    <TabPanel value={value} index={3}>
      <DeviationsTable
				id={id}
				customers={data.deviations}
			/>
		</TabPanel>
    <TabPanel value={value} index={4}>
      <EventsTable
				id={id}
				customers={data.events}
			/>
		</TabPanel>
	</CardContent>
</Card>);
};

export default ChecklistResult;
