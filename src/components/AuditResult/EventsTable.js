import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  Switch
} from '@material-ui/core';
import Helper from './../../helpers/Helper';

const EventsTable = ({ id, customers, units, ...rest }) => {
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);
  const [unitArray, setUnitArray] = useState([]);

  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  useEffect(() => {
    setUnitArray(units);
  }, [units]);

  return (
    <Card {...rest}>
        <Box sx={{
          // minWidth: 1050,
        }}>
          <Table size='small'>
            <TableHead>
              <TableRow>
                <TableCell>
                  ID
                </TableCell>
                <TableCell>
                  Ответственный
                </TableCell>
                <TableCell>
                  Задача
                </TableCell>
				<TableCell>
                  Крайняя дата
                </TableCell>
                <TableCell>
                  Статус
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {customers.slice(page*limit, page*limit + limit).map((customer, i) => {
                  return (
                <TableRow
                  hover
                  key={customer.id}
                  selected={selectedCustomerIds.indexOf(customer.id) !== -1}
                >
                  <TableCell style={{
                    width: '5%'
                  }}>
                    {page*limit + i + 1}
                  </TableCell>
                  <TableCell>
                    <a href={'/company/audits/' + id + '/deviation/' + customer.dev_id}>{customer.user.fio}</a>
                  </TableCell>
                  <TableCell style={{
                    // width: '5%'
                  }}>
                    {customer.dev_text}
                  </TableCell>
				  <TableCell style={{
                    // width: '5%'
                  }}>
                    {Helper.fromDBDateToRusDateTime(customer.task_date)}
                  </TableCell>
                  <TableCell style={{
                    // width: '5%'
                  }}>
                    {customer.status === 'ended' ? 'Завершено' : 'Зафиксировано'}
                  </TableCell>
                </TableRow>
              )})}
            </TableBody>
          </Table>
        </Box>
      <TablePagination
        component="div"
        labelRowsPerPage={'Показывать'}
        labelDisplayedRows={({ from, to, count }) => {
          return (from + '-' + to + ' из ' + count);
        }}
        count={customers.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

EventsTable.propTypes = {
  customers: PropTypes.array.isRequired
};

export default EventsTable;
