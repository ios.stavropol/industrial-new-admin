import React, { useEffect } from 'react';
import {
  Card,
  CardContent,
  Box,
  useTheme,
  colors,
} from '@material-ui/core';
import { Link as RouterLink, useLocation, useNavigate, useParams } from 'react-router-dom';
import { Bar, Doughnut } from 'react-chartjs-2';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import { makeStyles } from '@material-ui/styles';
import BarChart from '@material-ui/icons/BarChart';
import PieChart from '@material-ui/icons/PieChart';
import ContentPaste from '@material-ui/icons/ContentPaste';
import AnswersTable from './AnswersTable';

const useStyles = makeStyles({
    root: {
      width: 500,
    },
});

const Dashboard = ({id, data, score, answers }) => {

	const theme = useTheme();
    const location = useLocation();
    const [data2, setData2] = React.useState({
		labels: [],
		datasets: [],
	});

	const classes = useStyles();
    const [value2, setValue2] = React.useState('recents');
    const [graphView, setGraphView] = React.useState(null);
	const [dataScore, setDataScore] = React.useState({});
	const [answerData, setAnswerData] = React.useState([]);

	const handleChange2 = (event, newValue) => {
        setValue2(newValue);
    };

	const options = {
		legend: {
            display: true
		},
		tooltips: {
            enabled: true
		},
		scales: {
			 xAxes: [{
				 stacked: true
			 }],
			 yAxes: [{
				 stacked: true
			 }]
		 }
	}

	const options2 = {
        animation: false,
        cutoutPercentage: 80,
        layout: { padding: 0 },
        legend: {
          display: false
        },
        maintainAspectRatio: false,
        responsive: true,
        tooltips: {
          backgroundColor: theme.palette.background.paper,
          bodyFontColor: theme.palette.text.secondary,
          borderColor: theme.palette.divider,
          borderWidth: 1,
          enabled: true,
          footerFontColor: theme.palette.text.secondary,
          intersect: false,
          mode: 'index',
          titleFontColor: theme.palette.text.primary
        }
    };

	const renderGraph = () => {
		if (value2 === 'recents') {
			setGraphView(<Bar data={data2} options={options} />);
		} else if (value2 === 'favorites') {
			setGraphView(<Doughnut
                data={dataScore}
                options={options2}
            />);
		} else {
			setGraphView(<AnswersTable data={answerData} />);
		}
	}

	useEffect(() => {

		const ordered = Object.keys(score).sort().reduce(
			(obj, key) => { 
			  obj[key] = score[key]; 
			  return obj;
			}, 
			{}
		);

		let labels = [];
		let data = [];
		let colors2 = [];

		for (let i = 0; i < Object.keys(ordered).length; i++) {
			labels.push(Object.keys(ordered)[i]);
			data.push(ordered[Object.keys(ordered)[i]]);
			let backgroundColor = 'rgb(' + Math.floor(Math.random() * 255) + ',' + Math.floor(Math.random() * 255) + ',' + Math.floor(Math.random() * 255) + ')';
			colors2.push(backgroundColor);
		}

		console.warn('data: '+JSON.stringify(data));
		console.warn('labels: '+JSON.stringify(labels));
		setDataScore({
			datasets: [
				{
					data,
					backgroundColor: colors2,
					borderWidth: 8,
					borderColor: colors.common.white,
					hoverBorderColor: colors.common.white
				}
			],
			labels: labels
		});
		
    }, [score]);

    useEffect(() => {

		let datasets = [];
		let labels = [];
		let min = 0;
		let max = 0;
		let rows = {};
		for (let i = 0; i < Object.keys(data).length; i++) {
			let name = data[Object.keys(data)[i]]['text'].substring(0, 15) + '...' + data[Object.keys(data)[i]]['text'].substring(data[Object.keys(data)[i]]['text'].length - 15);
			labels.push(name);
			for (let y = 0; y < Object.keys(data[Object.keys(data)[i]]['price']).length; y++) {
				if (max < parseInt(Object.keys(data[Object.keys(data)[i]]['price'])[y])) {
					max = parseInt(Object.keys(data[Object.keys(data)[i]]['price'])[y]);
				}
			}
		}

		for (let y = 0; y <= max; y++) {
			rows[y] = {};
			for (let i = 0; i < Object.keys(data).length; i++) {
				rows[y][Object.keys(data)[i]] = 0;
			}
		}

		for (let y = 0; y <= max; y++) {
			for (let i = 0; i < Object.keys(data).length; i++) {
				if (data[Object.keys(data)[i]]['price'][y] !== undefined && data[Object.keys(data)[i]]['price'][y] !== null) {
					rows[y.toString()][Object.keys(data)[i]] = data[Object.keys(data)[i]]['price'][y];
				}
			}
		}

		for (let i = 0; i < Object.keys(rows).length; i++) {
			let backgroundColor;
			if (Object.keys(data)[i] == 1) {
				backgroundColor = 'green';
			} else {
				backgroundColor = 'rgb(' + Math.floor(Math.random() * 255) + ',' + Math.floor(Math.random() * 255) + ',' + Math.floor(Math.random() * 255) + ')';
			}

			let array = [];

			for (let y = 0; y < Object.keys(rows[Object.keys(rows)[i]]).length; y++) {
				array.push(rows[Object.keys(rows)[i]][Object.keys(rows[Object.keys(rows)[i]])[y]]);
			}
			datasets.push({
				label: Object.keys(rows)[i] + ' бал',
				data: array,
				backgroundColor,
			});
		}
		setData2({
			labels,
			datasets,
		});
		
    }, [data]);

	useEffect(() => {
        renderGraph();
    }, [value2, data2, dataScore]);

	useEffect(() => {
		let keys = Object.keys(answers);
		let array = [];
		for (let i = 0; i < keys.length; i++) {
			array.push(answers[keys[i]]);
		}
		setAnswerData(array);
	}, [answers]);

  return (<Card sx={{
		mb: 2,
	}}>
		<Box display="flex" flexDirection="row" justifyContent="center" p={1} m={1}>
			<BottomNavigation value={value2} onChange={handleChange2} className={classes.root}>
				<BottomNavigationAction label="Столбчатый" value="recents" icon={<BarChart />} />
				<BottomNavigationAction label="Круговой" value="favorites" icon={<PieChart />} />
				<BottomNavigationAction label="Все вопросы" value="questions" icon={<ContentPaste />} />
			</BottomNavigation>
		</Box>
		{graphView}
	</Card>);
};

export default Dashboard;
