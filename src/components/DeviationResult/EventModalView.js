import React, { useEffect } from 'react';
import { Link as RouterLink, useLocation, useNavigate } from 'react-router-dom';
import { Box, 
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Avatar,
    Stack,
    Typography, Switch, Modal, Autocomplete, FormControl, TextField, Select, OutlinedInput, MenuItem } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { addEvent } from '../../helpers/Network';
import Helper from './../../helpers/Helper';
import Config from '../../constants/Config';
import swal from 'sweetalert';
import ImageUploader from '../ImageUploader';
import { useTheme } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import DatePicker from '@material-ui/lab/DatePicker';
import ruLocale from 'date-fns/locale/ru';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    border: '2px solid white',
    boxShadow: 24,
    p: 4,
};

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
    //   width: '100%',
    },
  },
};

const EventModalView = ({open, onClose, onSave, current, userList}) => {

  const location = useLocation();
  const navigate = useNavigate();

  const theme = useTheme();
  const [users, setUsers] = React.useState([]);
  const [showModal, setShowModal] = React.useState(false);

  const [eventId, setEventId] = React.useState(0);
  const [devId, setDevId] = React.useState(0);
  const [devText, setDevText] = React.useState('');
  const [execText, setExecText] = React.useState('');
  const [devPlace, setDevPlace] = React.useState('');
  const [taskText, setTaskText] = React.useState('');
  const [taskDate, setTaskDate] = React.useState(null);
  const [execDate, setExecDate] = React.useState(null);
  const [userOption, setUserOption] = React.useState([]);
  const [userId, setUserId] = React.useState(null);
  const [status, setStatus] = React.useState(null);
  const [photos, setPhotos] = React.useState([]);
  const [photoBody, setPhotoBody] = React.useState(null);

  useEffect(() => {
    setShowModal(open);
  }, [open]);

  useEffect(() => {
    setUserOption(userList);
  }, [userList]);

  useEffect(() => {
      console.warn(current);
    if (current !== null && current !== undefined) {
        setEventId(current.id);
        setUserId(current.user);
        setDevId(current.dev_id);
		setDevText(current.dev_text);
        setDevPlace(current.dev_place);
        setTaskText(current.task_text);
        setExecText(current.exec_text);
        setPhotos(current.photos);
        setStatus(current.status);
		if (current.task_date) {
			let date = current.task_date;
			date = date.split(' ');
			date = date[0] + 'T' + date[1];
			date = new Date(date);
			setTaskDate(date);
		} else {
			setTaskDate(current.task_date);
		}
        if (current.exec_date) {
			let date = current.exec_date;
			date = date.split(' ');
			date = date[0] + 'T' + date[1];
			date = new Date(date);
			setExecDate(date);
		} else {
			setExecDate(current.exec_date);
		}
    }
  }, [current]);

  useEffect(() => {
    setUsers(userList);
  }, [userList]);

    useEffect(() => {
        console.warn('photos: ', photos);
        if (photos) {
            setPhotoBody(renderPhotos());
        }
    }, [photos]);

    const renderPhotos = () => {
        let array = [];
        for (let i = 0; i < photos.length; i++) {
            array.push(<a style={{
                position: 'relative',
            }} target={'_blank'} href={Config.apiDomain + '/uploads/deviations/events/' + photos[i].file}>
                <Avatar src={Config.apiDomain + '/uploads/deviations/events/' + photos[i].file} variant="rounded" sx={{ width: 100, height: 100 }} />
            </a>);
        }
        return (<Stack direction="row" spacing={1}>
            {array}
        </Stack>);
    }

  return (
    <Dialog
        open={showModal}
        onClose={(event, reason) => {
            if (reason !== 'backdropClick') {
                
            }
        }}
        fullWidth
        maxWidth="sm"
    >
        <DialogTitle>
            <Typography variant="h4">
                {eventId !== null && eventId === 0 ? 'Добавление мероприятия' : 'Редактирование мероприятия'}
            </Typography>
            <IconButton
                aria-label="close"
                onClick={() => {
                    if (onClose) {
                        onClose();
                    }
                }}
                sx={{
                    position: 'absolute',
                    right: 8,
                    top: 8,
                    color: (theme) => theme.palette.grey[500],
                }}
                >
                <CloseIcon />
            </IconButton>
        </DialogTitle>
        <DialogContent>
            <FormControl fullWidth sx={{
                m: 0,
                marginTop: 2,
                marginBottom: 1,
            }}>
                <TextField
                    id="patrname2"
                    label="Ответ сотрудника"
                    variant="outlined"
                    value={devText}
                    size="small"
                    disabled
					multiline
					rows={4}
                />
            </FormControl>
            <FormControl fullWidth sx={{
                m: 0,
                // marginTop: 2,
                marginBottom: 1,
            }}>
                <TextField
                    id="email2"
                    label="Место"
                    variant="outlined"
                    value={devPlace}
                    size="small"
                    disabled
                />
            </FormControl>
            <FormControl fullWidth sx={{
                m: 0,
                // marginTop: 2,
                marginBottom: 1,
            }}>
                <TextField
                    id="patrname2"
                    label="Задача мероприятия"
                    variant="outlined"
                    value={taskText}
                    size="small"
					multiline
					rows={4}
                    disabled={status === 'ended'}
					onChange={event => {
                        setTaskText(event.target.value);
                    }}
                />
            </FormControl>
			<FormControl fullWidth sx={{
                m: 0,
                // marginTop: 2,
                marginBottom: 1,
            }}>
                <LocalizationProvider dateAdapter={AdapterDateFns} locale={ruLocale}>
                <DatePicker
                    label="Конечная дата"
                    value={taskDate}
                    size='small'
                    minDate={new Date()}
                    disabled={status === 'ended'}
                    mask={'__.__.____'}
                    // disabled={id == 0 || status === 'Черновик' ? false : true}
                    onChange={newValue => {
                        // console.warn('onChange: ', newValue);
                        setTaskDate(newValue);
                    }}
                    renderInput={(params) => <TextField size='small' {...params} />}
                />
                </LocalizationProvider>
            </FormControl>
			<FormControl fullWidth sx={{
                m: 0,
                // marginTop: 2,
                marginBottom: 1,
            }}>
                <Autocomplete
                    value={userId}
                    size="small"
                    onChange={(event, newValue) => {
						console.warn(newValue);
						setUserId(newValue);
                    }}
                    filterOptions={(options, params) => {
                        let array = options.filter(a => {
                            return a.fio.toLowerCase().indexOf(params.inputValue.toLowerCase()) !== -1;
                        });

                        let filtered = [];
                        for (let i=0;i<array.length;i++) {
                            filtered.push(array[i]);
                        }
                        // console.warn(JSON.stringify(filtered));
                        return filtered;
                    }}
                    selectOnFocus
                    clearOnBlur
                    disabled={status === 'ended'}
                    handleHomeEndKeys
                    id="free-solo-with-text-demo"
                    options={userOption}
                    getOptionLabel={(option) => {
                        if (typeof option === 'string') {
                            return option;
                        }
                        return option.fio;
                    }}
                    style={{
                        zIndex: 1000,
                    }}
                    freeSolo
                    renderInput={(params) => (
                        <TextField {...params} label="Ответственный" variant="outlined" />
                    )}
                />
            </FormControl>
            {status === 'ended' ? (<FormControl fullWidth sx={{
				m: 0,
				// marginTop: 2,
				marginBottom: 1,
			}}>
				<LocalizationProvider dateAdapter={AdapterDateFns} locale={ruLocale}>
				<DatePicker
					label="Дата исполнения"
					value={execDate}
					size='small'
                    disabled
					mask={'__.__.____'}
					renderInput={(params) => <TextField size='small' {...params} />}
				/>
				</LocalizationProvider>
			</FormControl>) : null}
            {status === 'ended' ? (<FormControl fullWidth sx={{
                m: 0,
                // marginTop: 2,
                marginBottom: 1,
            }}>
                <TextField
                    id="patrname2"
                    label="Комментарий исполнителя"
                    variant="outlined"
                    value={execText}
                    size="small"
					multiline
					rows={4}
                    disabled
                />
            </FormControl>) : null}
            {status === 'ended' ? photoBody : null}
        </DialogContent>
        <DialogActions>
            {status === 'ended' ? (<Button variant="contained" color="primary" onClick={() => {
				if (onClose) {
                    onClose();
                }
            }}>
                Закрыть
            </Button>) : (<Button variant="contained" color="primary" onClick={() => {
				// console.warn(taskDate);
				// return;
                if (taskText === null || taskText.length === 0) {
                    swal(Config.appName, 'Укажите задачу мероприятия!', "warning");
                    return;
                }
                if (taskDate === null || taskDate.length === 0) {
                    swal(Config.appName, 'Укажите конечную дату!', "warning");
                    return;
                }
                if (userId === null) {
                    swal(Config.appName, 'Укажите ответственного!', "warning");
                    return;
                }

                addEvent(eventId, devId, userId.id, taskText, Helper.getDateFromJSDateTime(taskDate))
                .then(() => {
                    if (onSave) {
                        onSave();
                    }
                })
                .catch(err => {
                    swal(Config.appName, err, "warning");
                });
            }}>
                Сохранить
            </Button>)}
        </DialogActions>
    </Dialog>);
};

export default EventModalView;
