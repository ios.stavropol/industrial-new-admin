import React, { useEffect } from 'react';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  Typography
} from '@material-ui/core';
import ImageUploader from '../ImageUploader';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import { getCompany, saveCompany } from '../../helpers/Network';
import Config from '../../constants/Config';
import swal from 'sweetalert';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Link as RouterLink, useNavigate } from 'react-router-dom';

const CommonInfo = (props) => {

    const navigate = useNavigate();

    const [imagePreviewUrl, setImagePreviewUrl] = React.useState(null);
    const [imageChanged, setImageChanged] = React.useState(false);
    const [user, setUser] = React.useState(null);
    const [status, setStatus] = React.useState(null);
    const [deviation, setDeviation] = React.useState('');
    const [description, setDescription] = React.useState('');
    const [place, setPlace] = React.useState('');
	const [open, setOpen] = React.useState(false);

    useEffect(() => {
        if (props.data !== null) {
            setUser(props.data.user);
            setStatus(props.data.status);
            setDeviation(props.data.deviation);
			setDescription(props.data.description);
			setPlace(props.data.place);
        }
    }, [props.data]);

    return (<Card
        sx={{ height: '100%' }}
      >
        <CardContent>
            <FormControl fullWidth sx={{
                    m: 0,
                    marginTop: 2,
                }}>
                <TextField
                    id="name"
                    size='small'
                    label="Сотрудник"
                    variant="outlined"
                    value={user ? user.fio : ''}
					disabled
                />
            </FormControl>
            <FormControl fullWidth sx={{
                m: 0,
                marginTop: 2,
            }}>
                <TextField
                    id="name"
                    size='small'
                    label="Статус"
                    variant="outlined"
                    // value={status === 'started' ? 'Зафиксировано' : ''}
                    value={status === 'ended' ? 'Завершено' : 'Зафиксировано'}
					disabled
                />
            </FormControl>
            <FormControl fullWidth sx={{
                m: 0,
                marginTop: 2,
            }}>
                <TextField
                    id="name"
                    size='small'
                    label="Отклонение"
                    variant="outlined"
                    value={deviation}
					disabled
					multiline
					rows={4}
                />
            </FormControl>
            <FormControl fullWidth sx={{
                m: 0,
                marginTop: 2,
                marginBottom: 2,
            }}>
                <TextField
                    id="name"
                    size='small'
                    label="Ответ сотрудника"
                    variant="outlined"
                    value={description}
					disabled
					multiline
					rows={4}
                />
            </FormControl>
			<FormControl fullWidth sx={{
                m: 0,
                marginBottom: 2,
            }}>
                <TextField
                    id="name"
                    size='small'
                    label="Место"
                    variant="outlined"
                    value={place}
					disabled
					multiline
					// rows={4}
                />
            </FormControl>
        </CardContent>
        <Backdrop
            sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
            open={open}
            onClick={() => {

            }}
        >
            <CircularProgress color="inherit" />
        </Backdrop>
      </Card>);
};

export default CommonInfo;
