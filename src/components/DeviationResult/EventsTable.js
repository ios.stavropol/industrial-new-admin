import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  Switch
} from '@material-ui/core';
import Config from './../../constants/Config';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import Helper from './../../helpers/Helper';

const statuses = {
	'started': 'К выполнению',
	'working': 'Выполняется',
	'ended': 'Завершено',
};

const EventsTable = ({ customers, onDelete, onEdit, ...rest }) => {
  const [selectedCustomerIds, setSelectedCustomerIds] = useState([]);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(0);
  const [units, setUnits] = useState([]);


  const handleLimitChange = (event) => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  return (
    <Card {...rest}>
        <Box sx={{
          // minWidth: 1050,
        }}>
          <Table size='small'>
            <TableHead>
              <TableRow>
                <TableCell>
                  ID
                </TableCell>
                <TableCell>
                  Исполнитель
                </TableCell>
                <TableCell>
                  Поручение
                </TableCell>
                <TableCell>
                  Крайний срок
                </TableCell>
				<TableCell>
                  Статус
                </TableCell>
                <TableCell>
                  Операции
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {customers.slice(page*limit, page*limit + limit).map((customer) => {
                return (<TableRow
                  hover
                  key={customer.id}
                  selected={selectedCustomerIds.indexOf(customer.id) !== -1}
                >
                  <TableCell style={{
                    width: '5%'
                  }}>
                    {customer.id}
                  </TableCell>
                  <TableCell>
                    {customer.user.fio}
                  </TableCell>
                  <TableCell style={{
                    // width: '5%'
                  }}>
                    {customer.task_text}
                  </TableCell>
                  <TableCell style={{
                    // width: '5%'
                  }}>
                    {Helper.fromDBDateToRusDateTime(customer.task_date)}
                  </TableCell>
                  <TableCell style={{
                    // width: '5%'
                  }}>
                    {statuses[customer.status]}
                  </TableCell>
                  <TableCell style={{
                    width: '15%'
                  }}>
                    <IconButton onClick={() => {
                        if (onEdit) {
                            onEdit(customer);
                        }
                    }}>
                        <EditIcon />
                    </IconButton>
                    <IconButton onClick={() => {
                        if (onDelete) {
                            onDelete(customer);
                        }
                    }}>
                        <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              )})}
            </TableBody>
          </Table>
        </Box>
      <TablePagination 
        component="div"
        labelRowsPerPage={'Показывать'}
        labelDisplayedRows={({ from, to, count }) => {
          return (from + '-' + to + ' из ' + count);
        }}
        count={customers.length}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  );
};

EventsTable.propTypes = {
  customers: PropTypes.array.isRequired
};

export default EventsTable;
