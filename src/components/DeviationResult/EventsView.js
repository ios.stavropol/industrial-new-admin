import React, { useEffect } from 'react';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  Typography
} from '@material-ui/core';
import ImageUploader from '../ImageUploader';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Network, { getCompanyUsers, deleteEvent } from '../../helpers/Network';
import Config from '../../constants/Config';
import swal from 'sweetalert';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import EventsTable from './EventsTable';
import EventModalView from './EventModalView';

const EventsView = (props) => {

    const navigate = useNavigate();

    const [events, setEvents] = React.useState([]);
	const [users, setUsers] = React.useState([]);
	const [open, setOpen] = React.useState(false);
	const [current, setCurrent] = React.useState(null);

	useEffect(() => {
		getCompanyUsers(Network.userProfile.company_id)
		.then(response => {
			setUsers(response);
		})
		.catch(err => {

		});
	}, []);

    useEffect(() => {
		setEvents(props.events);
    }, [props.events]);

    return (<Card
        sx={{ height: '100%' }}
      >
        <CardContent>
			<Button
				variant="contained"
				color="primary"
				onClick={() => {
					setCurrent({
						id: 0,
						dev_id: props.data.id,
						dev_text: props.data.description,
						dev_place: props.data.place,
						dev_date: props.data.date_start,
						exec_text: '',
						exec_date: null,
						task_text: '',
						task_date: null,
						user: null,
					});
					setOpen(true);
				}}
				style={{
					marginBottom: 10,
				}}
			>
				Добавить мероприятие
			</Button>
			<EventsTable
                customers={events}
                onEdit={customer => {
                    setCurrent(customer);
                    setOpen(true);
                }}
                onDelete={customer => {
                    swal({
                        title: Config.appName,
                        text: 'Вы хотите удалить мероприятие?',
                        icon: "warning",
                        buttons: ['Нет', 'Да'],
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            deleteEvent(customer.id)
                            .then(() => {
                                if (props.refresh) {
									props.refresh();
								}
                            })
                            .catch(err => {
                                swal(Config.appName, err, "warning");
                            });
                        }
                    });
                }}
            />
        </CardContent>
		<EventModalView
			open={open}
			current={current}
			userList={users}
			onClose={() => {
				setOpen(false);
			}}
			onSave={() => {
				if (props.refresh) {
					props.refresh();
				}
				setOpen(false);
			}}
		/>
        <Backdrop
            sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
            open={open}
            onClick={() => {

            }}
        >
            <CircularProgress color="inherit" />
        </Backdrop>
      </Card>);
};

export default EventsView;
