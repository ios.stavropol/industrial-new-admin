import React, { useEffect } from 'react';
import { Link as RouterLink, useLocation, useNavigate, useParams } from 'react-router-dom';
import { useTheme } from '@material-ui/core/styles';
import { Box, 
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Container, Modal, Typography, FormControl, TextField, Autocomplete, FormControlLabel, Switch, Select, OutlinedInput, MenuItem, Grid } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { saveQuestion } from '../../helpers/Network';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import swal from 'sweetalert';
import Config from '../../constants/Config';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    // border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    // maxHeight: '90%',
};

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
    //   width: '100%',
    },
  },
};

const answerTypes = [
    {value: 'radio', label: 'Единственный выбор'},
    {value: 'checkbox', label: 'Множественный выбор'},
];

function getStyles(name, personName, theme) {
    return {
      fontWeight:
        personName.indexOf(name) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    };
}

const QuestionModalView = ({questionsArray, open, question, onClose, onRefresh}) => {

  const location = useLocation();
  const navigate = useNavigate();

  let { id, checklist } = useParams();

  const [questions, setQuestions] = React.useState([]);
  const [answers, setAnswers] = React.useState([]);
  const [showModal, setShowModal] = React.useState(false);

  const [is_new, setIsNew] = React.useState(false);

  const [value, setValue] = React.useState(null);

  const theme = useTheme();
  const [personName, setPersonName] = React.useState(null);
  const [questionOptions, setQuestionOptions] = React.useState([]);
  const [answersView, setAnswersView] = React.useState([]);
  const [has_comment, setHasComment] = React.useState(false);
  const [has_parent, setHasParent] = React.useState(false);
  const [parentValue, setParentValue] = React.useState(null);
  const [answerOptions, setAnswerOptions] = React.useState([]);
  const [answerValue, setAnswerValue] = React.useState(null);
  const [is_shuffle, setIsShuffle] = React.useState(false);
  const [has_deviation, setHasDeviation] = React.useState(false);
  const [questionBody, setQuestionBody] = React.useState(null);

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setPersonName(
        // On autofill we get a the stringified value.
        //   typeof value === 'string' ? value.split(',') : value,
        value,
    );
  };

  const renderAnswersView = () => {
    //   setTimeout(() => {
        let views = [];
        for (let i = 0; i < answers.length; i++) {
            views.push(<Box display="flex" flexDirection="row" alignItems="flex-start" justifyContent="space-between" p={1} m={1}>
                <Box sx={{
                    mt: 1,
                }}>
                    {answers[i].id}
                </Box>
                <Box sx={{
                    width: '70%',
                }}>
                    <TextField
                        size='small'
                        id="name"
                        label="Ответ"
                        variant="outlined"
                        value={answers[i].answer}
                        style={{
                            width: '100%',
                        }}
                        onChange={event => {
                            let answersArray = answers;
                            answersArray[i].answer = event.target.value;
                            setAnswers(answersArray);
                            renderAnswersView();
                        }}
                    />
                    <TextField
                        size='small'
                        id="name"
                        label="Отклонение"
                        variant="outlined"
                        value={answers[i].deviation}
                        style={{
                            width: '100%',
                            marginTop: '10px'
                        }}
                        onChange={event => {
                            let answersArray = answers;
                            answersArray[i].deviation = event.target.value;
                            setAnswers(answersArray);
                            renderAnswersView();
                        }}
                    />
                </Box>
                <Box sx={{
                    width: '25%',
                }} display="flex" flexDirection="row" justifyContent="space-between">
                    <TextField
                        id="name"
                        size='small'
                        label="Бал"
                        variant="outlined"
                        value={answers[i].price}
                        type="number"
                        InputProps={{ inputProps: { min: -1, max: 100 } }}
                        style={{
                            width: '55%',
                        }}
                        onChange={event => {
                            let digit = event.target.value;
                            digit = digit.replace(/[^\d.-]/g, '');
                            if (digit < -1 || digit > 100 || digit.length === 0) {
                                swal(Config.appName, 'Диапазон баллов от -1 до 100!', "warning");
                            } else {
                                let answersArray = answers;
                                answersArray[i].price = digit;
                                setAnswers(answersArray);
                                renderAnswersView();
                            }
                        }}
                    />
                    <IconButton onClick={() => {
                        let answersArray = answers;
                        answersArray = answersArray.filter(value => {
                            return value.id !== answers[i].id;
                        });
                        setAnswers(answersArray);
                        renderAnswersView();
                    }}>
                        <DeleteIcon />
                    </IconButton>
                </Box>
            </Box>);
          }
          setAnswersView(views);
    //   }, 200);
  }

  const render2Answer = question => {
    if (question !==null && question !== undefined && question.fields !== null && question.fields !== undefined && question.fields.length) {
        let fields = JSON.parse(question.fields);
        if (fields !== null) {
            setAnswers(fields.answers);
            setHasComment(fields.has_comment);
            setIsShuffle(fields.is_shuffle !== null && fields.is_shuffle !== undefined ? fields.is_shuffle : 0);
        } else {
            setAnswers([]);
            setHasComment(false);
        }
        renderAnswersView();
    } else {
        setAnswers([]);
        setHasComment(false);
        renderAnswersView();
    }
  }

  useEffect(() => {
    setAnswersView([]);
    setShowModal(open);
  }, [open]);

  useEffect(() => {
    //   window.alert(questionsArray.length);
        setQuestionOptions(questionsArray);
  }, [questionsArray]);

  useEffect(() => {
    setQuestionBody(renderBodyView());
  }, [questionOptions]);

  useEffect(() => {
    setValue({
        value: question.id,
        label: question.question,
        fields: question.fields,
    });
    if (question.is_new) {
        setIsNew(true);
    } else {
        setIsNew(false);
    }
  }, [question]);

  useEffect(() => {
    if (value !== null && value.fields !== null && value.fields !== undefined && value.fields.length) {
        let fields = JSON.parse(value.fields);
        if (fields !== null) {
            setAnswers(fields.answers);
            setHasComment(fields.has_comment);
            setHasDeviation(fields.has_deviation ? true : false);
            setIsShuffle(fields.is_shuffle !== null && fields.is_shuffle !== undefined ? fields.is_shuffle : 0);
            for (let i = 0; i < answerTypes.length; i++) {
                if (answerTypes[i].value === fields.type) {
                    setPersonName(answerTypes[i].value);
                    break;
                }
            }
        } else {
            setAnswers([]);
            setHasComment(false);
            setPersonName(null);
        }
        if (question.prev_id !== null && question.answer_id !== null) {
            // window.alert(JSON.stringify(parent));
            // window.alert(question.prev_id);
            let parent = null;
            for (let i = 0; i < questionsArray.length; i++) {
                // console.warn(questionsArray[i].id + '==' + question.prev_id);
                // console.warn(JSON.stringify(questionsArray[i]));
                if (questionsArray[i].value == question.prev_id) {
                    parent = questionsArray[i];
                    // window.alert(question.prev_id);
                    break;
                }
            }
            // setTimeout(() => {
                setParentValue(parent);
            // }, 100);
        } else {
            setHasParent(null);
            setParentValue(null);
        }
        // renderAnswersView();
    } else {
        setAnswers([]);
        setHasComment(false);
        setPersonName(null);
        setAnswerValue(null);
        setParentValue(null);
        setAnswerOptions([]);
        setHasParent(false);
        setHasDeviation(false);
        // renderAnswersView();
    }
  }, [value]);

  useEffect(() => {
    renderAnswersView();
  }, [answers, has_deviation]);

  useEffect(() => {
    if (parentValue !== null) {
        let fields = JSON.parse(parentValue.fields);
        if (fields) {
            setAnswerOptions(fields.answers);
            let answ_id = null;
            for (let i = 0; i < fields.answers.length; i++) {
                if (fields.answers[i].id === question.answer_id) {
                    answ_id = fields.answers[i];
                }
            }
            setAnswerValue(answ_id);
        }
        setHasParent(true);
    }

    setQuestionBody(renderBodyView());
  }, [parentValue]);

  const renderBodyView = () => {
      return (<FormControl fullWidth sx={{
        m: 0,
        marginTop: 2,
        marginBottom: 1,
    }}>
        <Autocomplete
            value={parentValue}
            size='small'
            onChange={(event, newValue) => {
                // render2Answer(newValue);
                console.warn(newValue);
                setParentValue(newValue);
                let fields = JSON.parse(newValue.fields);
                if (fields) {
                    setAnswerOptions(fields.answers);
                }
                // window.alert(JSON.stringify(newValue));
                // setIsNew(false);
            }}
            filterOptions={(options, params) => {
                let array = options.filter(a => {
                    return a.label.toLowerCase().indexOf(params.inputValue.toLowerCase()) !== -1;
                });

                let filtered = [];
                for (let i=0;i<array.length;i++) {
                    // filtered.push({
                    //     label: array[i].label,
                    //     value: array[i].id,
                    //     fields: array[i].fields,
                    // });
                    filtered.push(array[i]);
                }
                console.warn(JSON.stringify(filtered));
                return filtered;
            }}
            selectOnFocus
            clearOnBlur
            handleHomeEndKeys
            id="free-solo-with-text-demo"
            options={questionOptions}
            getOptionLabel={(option) => {
                if (typeof option === 'string') {
                    return option;
                }
                return option.label;
            }}
            style={{
                zIndex: 1000,
            }}
            freeSolo
            renderInput={(params) => (
                <TextField {...params} label="Вопрос" variant="outlined" />
            )}
        />
    </FormControl>);
  }
  return (
    <Dialog
        open={showModal}
        onClose={(event, reason) => {
            if (reason !== 'backdropClick') {
                
            }
        }}
        fullWidth
        maxWidth="sm"
    >
        <DialogTitle>
            <Typography variant="h4">
                {value !== null && value.value === 0 ? 'Добавление вопроса' : 'Редактирование вопроса'}
            </Typography>
            <IconButton
                aria-label="close"
                onClick={() => {
                    setAnswers([]);
                    renderAnswersView();
                    if (onClose) {
                        onClose();
                    }
                }}
                sx={{
                    position: 'absolute',
                    right: 8,
                    top: 8,
                    color: (theme) => theme.palette.grey[500],
                }}
                >
                <CloseIcon />
            </IconButton>
        </DialogTitle>
        <DialogContent>
            <FormControl fullWidth sx={{
                m: 0,
                marginTop: 2,
                marginBottom: 1,
            }}>
                {value !==null && value.value !== 0 ? (<TextField
                    style={{
                        // height: 30,
                    }}
                    id="lastname2"
                    label="Вопрос"
                    variant="outlined"
                    value={value.label}
                    size="small"
                    onChange={event => {
                        let obj = JSON.parse(JSON.stringify(value));
                        obj.label = event.target.value;
                        console.warn(obj);
                        setValue(obj);
                    }}
                />) : (<Autocomplete
                    value={value}
                    size='small'
                    onChange={(event, newValue) => {
                        if (newValue && newValue.value === 0) {
                            render2Answer({
                                value: newValue.value,
                                label: newValue.raw,
                                fields: null,
                            });
                            setValue({
                                value: newValue.value,
                                label: newValue.raw,
                                fields: null,
                            });
                            setIsNew(true);
                        } else {
                            render2Answer(newValue);
                            setValue(newValue);
                            setIsNew(false);
                        }
                    }}
                    filterOptions={(options, params) => {
                        let array = options.filter(a => {
                            return a.label.toLowerCase().indexOf(params.inputValue.toLowerCase()) !== -1;
                        });

                        let filtered = [];
                        for (let i=0;i<array.length;i++) {
                            // filtered.push({
                            //     label: array[i].label,
                            //     value: array[i].id,
                            //     fields: array[i].fields,
                            // });
                            filtered.push(array[i]);
                        }
                        if (params.inputValue !== '') {
                            filtered.push({
                                value: 0,
                                label: `Создать "${params.inputValue}"`,
                                raw: params.inputValue,
                                fields: null,
                            });
                        }
                        console.warn(JSON.stringify(filtered));
                        return filtered;
                    }}
                    selectOnFocus
                    clearOnBlur
                    handleHomeEndKeys
                    id="free-solo-with-text-demo"
                    options={questionOptions}
                    getOptionLabel={(option) => {
                        if (typeof option === 'string') {
                            return option;
                        }
                        return option.label;
                    }}
                    style={{
                        zIndex: 1000,
                    }}
                    freeSolo
                    renderInput={(params) => (
                        <TextField {...params} label="Вопрос" variant="outlined" />
                    )}
                />)}
            </FormControl>
            {value !== null && value.label.length ? (<FormControl sx={{
                m: 0,
                // marginTop: 2,
                marginBottom: 1,
            }}>
                <FormControlLabel
                    value="top"
                    control={<Switch color="primary" checked={has_comment} onChange={event => {
                        setHasComment(event.target.checked);
                    }} />}
                    label="Есть комментарий?"
                    labelPlacement="start"
                />
            </FormControl>) : null}
            {value !== null && value.label.length ? (<FormControl sx={{
                m: 0,
                // marginTop: 2,
                marginBottom: 1,
            }}>
                <FormControlLabel
                    value="top"
                    control={<Switch color="primary" checked={has_deviation} onChange={event => {
                        setHasDeviation(event.target.checked);
                    }} />}
                    label="Есть отклонения?"
                    labelPlacement="start"
                />
            </FormControl>) : null}
            {value !== null && value.label.length ? (<FormControl sx={{
                m: 0,
                width: '100%',
                alignItems: 'flex-start',
                // marginTop: 2,
                marginBottom: 1,
            }}>
                <FormControlLabel
                    value="top"
                    control={<Switch color="primary" checked={has_parent} onChange={event => {
                        setHasParent(event.target.checked);
                        if (!event.target.checked) {
                            setParentValue(null);
                            setAnswerValue(null);
                        }
                    }} />}
                    label="Отображать если выбран ответ"
                    labelPlacement="start"
                />
            </FormControl>) : null}
            {has_parent ? (questionBody) : null}
            {parentValue !== null && parentValue !== false ? (<FormControl fullWidth sx={{
                m: 0,
                marginTop: 2,
                marginBottom: 1,
            }}>
                <Autocomplete
                    value={answerValue}
                    size='small'
                    onChange={(event, newValue) => {
                        // render2Answer(newValue);
                        // window.alert(JSON.stringify(newValue));
                        setAnswerValue(newValue);
                        // setIsNew(false);
                    }}
                    filterOptions={(options, params) => {
                        let array = options.filter(a => {
                            return a.answer.toLowerCase().indexOf(params.inputValue.toLowerCase()) !== -1;
                        });

                        let filtered = [];
                        for (let i=0;i<array.length;i++) {
                            // filtered.push({
                            //     label: array[i].label,
                            //     value: array[i].id,
                            //     fields: array[i].fields,
                            // });
                            filtered.push(array[i]);
                        }
                        console.warn(JSON.stringify(filtered));
                        return filtered;
                    }}
                    selectOnFocus
                    clearOnBlur
                    handleHomeEndKeys
                    id="free-solo-with-text-demo"
                    options={answerOptions}
                    getOptionLabel={(option) => {
                        if (typeof option === 'string') {
                            return option;
                        }
                        return option.answer;
                    }}
                    style={{
                        zIndex: 1000,
                    }}
                    freeSolo
                    renderInput={(params) => (
                        <TextField {...params} label="Ответ" variant="outlined" />
                    )}
                />
            </FormControl>) : null}
            {value !== null && value.label.length ? (<FormControl sx={{
                m: 0,
                width: '100%',
                // mt: 1,
                mb: 1,
            }}>
                <Select
                    displayEmpty
                    size='small'
                    value={personName}
                    onChange={handleChange}
                    input={<OutlinedInput />}
                    MenuProps={MenuProps}
                    inputProps={{ 'aria-label': 'Without label' }}
                >
                <MenuItem disabled value="">
                    <em>Тип ответов</em>
                </MenuItem>
                {answerTypes.map((name) => (
                    <MenuItem
                    key={name.value}
                    value={name.value}
                    style={{
                        fontWeight: theme.typography.fontWeightMedium,
                    }}
                    >
                    {name.label}
                    </MenuItem>
                ))}
                </Select>
            </FormControl>) : null}
            {value !== null && value.label.length ? (<FormControl sx={{
                m: 0,
                // marginTop: 2,
                marginBottom: 1,
            }}>
                <FormControlLabel
                    value="top"
                    control={<Switch color="primary" checked={is_shuffle} onChange={event => {
                        setIsShuffle(event.target.checked);
                    }} />}
                    label="Перемешивать ответы?"
                    labelPlacement="start"
                />
            </FormControl>) : null}
            {value !== null && value.label.length ? (<div style={{
                width: '100%',
            }}>
                <Button variant="contained" color="primary" onClick={() => {
                    let answersArray = answers;
                    answersArray.push({id: (answersArray.length + 1), answer: '', price: 0});
                    setAnswers(answersArray);
                    renderAnswersView();
                }}>
                    Добавить вариант ответа
                </Button>
            </div>) : null}
            {value !== null && value.label.length ? (<div style={{marginTop: 10}}>Значение бала -1 не учитывается при подсчете оценки</div>) : null}
            {value !== null && value.label.length ? answersView : null}
        </DialogContent>
        <DialogActions>
            <Button variant="contained" sx={{
                mt: 1,
            }} color="primary" onClick={() => {
                if (value !== null) {
                    if (personName === null || personName.length === 0) {
                        swal(Config.appName, 'Выберите тип ответа!', "warning");
                        return;
                    }

                    if (answers === null || answers.length === 0) {
                        swal(Config.appName, 'Укажите ответы!', "warning");
                        return;
                    }

                    saveQuestion(value.value, value.label, is_new, checklist, personName, has_comment, has_deviation, answers, is_shuffle, (parentValue !== null ? parentValue.value : null), (answerValue !== null ? answerValue.id : null))
                    .then(() => {
                        if (onRefresh) {
                            onRefresh();
                        }
                    })
                    .catch(err => {
                        swal(Config.appName, err, "warning");
                    });
                } else {
                    swal(Config.appName, 'Укажите название!', "warning");
                }
            }}>
                Сохранить
            </Button>
        </DialogActions>
    </Dialog>);
};

export default QuestionModalView;
