import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  AppBar,
  Badge,
  Box,
  Hidden,
  IconButton,
  Toolbar
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/NotificationsOutlined';
import InputIcon from '@material-ui/icons/Logout';
import Logo from './Logo';
import { logout } from '../helpers/Network';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import swal from 'sweetalert';
import Config from '../constants/Config';

const DashboardNavbar = ({ onMobileNavOpen, ...rest }) => {
  const [notifications] = useState([]);
  const navigate = useNavigate();

  return (
    <AppBar
      elevation={0}
      {...rest}
    >
      <Toolbar>
        <RouterLink to="/">
          <Logo style={{
            width: '50px'
          }}/>
        </RouterLink>
        <Box sx={{ flexGrow: 1 }} />
        <Hidden lgDown>
          <IconButton
            color="inherit"
            onClick={() => {
              swal({
                title: Config.appName,
                text: 'Вы хотите выйти?',
                icon: "warning",
                buttons: ['Нет', 'Да'],
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                  logout()
                  .then(() => {
                    // this.props.history.push('/login')
                    navigate('/login', { replace: true });
                  })
                  .catch(err => {
                    swal(Config.appName, err, "warning");
                  });
                }
              });
            }}
          >
            <InputIcon />
          </IconButton>
        </Hidden>
        <Hidden lgUp>
          <IconButton
            color="inherit"
            onClick={onMobileNavOpen}
          >
            <MenuIcon />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
};

DashboardNavbar.propTypes = {
  onMobileNavOpen: PropTypes.func
};

export default DashboardNavbar;
