import React, { Component } from 'react';
import { Button } from 'reactstrap';

class ImageUploader extends Component {

  state = {
    file: '',
    imagePreviewUrl: null,
  };

  componentWillMount() {
    this.setState({
      imagePreviewUrl: this.props.image,
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({
      imagePreviewUrl: nextProps.image,
    });
  }

  onDeleteImage = () => {
    if (this.props.onDeleteImage) {
      this.props.onDeleteImage();
    }
  }

  _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      }, () => {
        if(this.props.onChangeImage) {
          this.props.onChangeImage(reader);
          //data:image/jpeg;base64,
        }
      });
    }

    reader.readAsDataURL(file)
  }

  render() {

    let avatar = null;

    if(this.state.imagePreviewUrl != null) {
      avatar = (<div style={{width: '120px', height: '120px', position: 'relative'}}>
        <Button style={{width: '120px', height: '120px',backgroundColor: 'lightGray', padding: '0px', borderWidth: '0px'}}>
          <img
            src={this.state.imagePreviewUrl}
            style={{
              display: 'block',
              margin: 'auto',
              height: 'auto',
              maxHeight: '100%',
              width: 'auto',
              maxWidth: '100%'
            }}/>
        </Button>
        <input onChange={(e)=>this._handleImageChange(e)} type="file" name="file" style={{position: 'absolute',opacity: 0,top: 0, right: 0, width: '120px', height: '120px', cursor: 'pointer'}}/>
        <Button onClick={() => {
          this.onDeleteImage();
        }} style={{width: '20px', height: '20px',backgroundColor: 'lightGray', padding: '0px', position: 'absolute', top: 0, right: 0}}>
          X
        </Button>
      </div>);
    } else {
      avatar = (<div style={{width: '120px', height: '120px', position: 'relative'}}>
        <Button style={{width: '120px', height: '120px',backgroundColor: 'lightGray'}}>
          Выберите изображение
        </Button>
        <input onChange={(e)=>this._handleImageChange(e)} type="file" name="file" style={{position: 'absolute',opacity: 0,top: 0, right: 0, width: '120px', height: '120px', cursor: 'pointer'}}/>
      </div>);
    }

    return avatar;
  }
}

export default ImageUploader;
