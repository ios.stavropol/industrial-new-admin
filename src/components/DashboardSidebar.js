import React, { useEffect } from 'react';
import { Link as RouterLink, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  Avatar,
  Box,
  Button,
  Divider,
  Drawer,
  Hidden,
  List,
  Typography
} from '@material-ui/core';
import {
  AlertCircle as AlertCircleIcon,
  BarChart as BarChartIcon,
  BookOpen as BookIcon,
  Lock as LockIcon,
  Settings as SettingsIcon,
  ShoppingBag as ShoppingBagIcon,
  User as UserIcon,
  UserPlus as UserPlusIcon,
  Users as UsersIcon,
  DollarSign
} from 'react-feather';
import NavItem from './NavItem';
import Network, { getProfile } from '../helpers/Network';
import Config from '../constants/Config';

const user = {
  avatar: '/static/images/avatars/avatar_6.png',
  jobTitle: 'Senior Developer',
  name: 'Katarina Smith'
};

const rolesList = {
  'admin':          'Суперадминистратор',
  'admin_company':  'Администратор компании',
  'auditor':        'Аудитор',
  // 'top-manager':    'Топ менеджер',
  // 'middle-manager': 'Менеджер',
  // 'specialist':     'Специалист',
  'statist':        'Статист',
  // 'worker':         'Сотрудник',
};

const itemsAdmin = [
  {
    href: '/admin/companies',
    icon: ShoppingBagIcon,
    title: 'Компании'
  },
  {
    href: '/admin/permissions',
    icon: SettingsIcon,
    title: 'Разрешения'
  },
  {
    type: 'divider'
  },
  {
    href: '/admin/process',
    icon: BookIcon,
    title: 'Бизнес-процессы'
  },
  // {
  //   href: '/admin/staff',
  //   icon: BookIcon,
  //   title: 'Должности'
  // },
  // {
  //   href: '/admin/units',
  //   icon: BookIcon,
  //   title: 'Подразделения'
  // },
];

const itemsCompany = [
  {
    href: '/company/tasks',
    icon: ShoppingBagIcon,
    title: 'Задачи'
  }
];

const DashboardSidebar = ({ onMobileClose, openMobile }) => {
  const location = useLocation();

  const [profile, setProfile] = React.useState(null);
  const [menu, setMenu] = React.useState([]);
  const [allRoles, setAllRoles] = React.useState('');

  useEffect(() => {
    if (openMobile && onMobileClose) {
      onMobileClose();
    }
    getProfile()
    .then(profile => {
      setProfile(profile);
      let str = [];
      for (let i = 0; i < profile.roles.length; i++) {
        
        if (rolesList[profile.roles[i]] !== undefined && rolesList[profile.roles[i]] !== null) {
          str.push(<Typography
            color="textPrimary"
            variant="h5"
            align="center"
          >
            {rolesList[profile.roles[i]]}
          </Typography>);
        }
        
      }
      setAllRoles(str);
      
      if (localStorage.getItem('roles').includes('admin') && !localStorage.getItem('roles').includes('admin_company')) {
        setMenu(itemsAdmin);
        
      } else {
        let items = [
          {
            href: '/company/tasks',
            icon: ShoppingBagIcon,
            title: 'Задачи'
          },
          {
            href: '/company/events',
            icon: ShoppingBagIcon,
            title: 'Мероприятия'
          }
        ];

        if (localStorage.getItem('menu').includes('audit')) {
          items.push({
            href: '/company/audits',
            icon: ShoppingBagIcon,
            title: 'Аудит'
          });
        }

        if (localStorage.getItem('menu').includes('dashboard')) {
          items.push({
            href: '/company/dashboard',
            icon: ShoppingBagIcon,
            title: 'Статистика'
          });
        }

        if (localStorage.getItem('menu').includes('company')) {
          items.push({
            href: '/company/company',
            icon: SettingsIcon,
            title: 'Компания'
          });
        }

        if (localStorage.getItem('menu').includes('unit')) {
          items.push({
            href: '/company/units',
            icon: BookIcon,
            title: 'Подразделения'
          });
        }

        if (localStorage.getItem('menu').includes('staff')) {
          items.push({
            href: '/company/staff',
            icon: BookIcon,
            title: 'Должности'
          });
        }

        if (localStorage.getItem('menu').includes('user')) {
          items.push({
            href: '/company/users',
            icon: UsersIcon,
            title: 'Сотрудники'
          });
        }
  
        // const unique = [...new Set(items.map(item => item.title))];
  
        setMenu(items);
        // setAllRoles(str);
      }
    })
    .catch(err => {

    });
  }, [location.pathname, Network.updateLayout]);

  const content = (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        height: '100%'
      }}
    >
      <Box
        sx={{
          alignItems: 'center',
          display: 'flex',
          flexDirection: 'column',
          p: 2
        }}
      >
        {Network.userProfile !== undefined && Network.userProfile.avatar !== undefined && Network.userProfile !== null && Network.userProfile.avatar !== null && Network.userProfile.avatar.length ? (<Avatar
          component={RouterLink}
          sx={{
            cursor: 'pointer',
            width: 64,
            height: 64,
            bgcolor: '#381a6c',
            mb: 1,
          }}
          src={Config.apiDomain + '/uploads/user/' + Network.userProfile.avatar}
          to={localStorage.getItem('roles').includes('admin') ? '/admin/account' : '/company/account'}
        />) : (<Avatar
          component={RouterLink}
          sx={{
            cursor: 'pointer',
            width: 64,
            height: 64,
            bgcolor: '#381a6c',
            mb: 1,
          }}
          children={profile != undefined && profile.fio != undefined ? `${profile.fio.split(' ')[0][0]}${profile.fio.split(' ')[1][0]}` : ''}
          to={localStorage.getItem('roles') ? (localStorage.getItem('roles').includes('admin') ? '/admin/account' : '/company/account') : '/login'}
        />)}
        <Typography
          color="textPrimary"
          variant="h5"
          align="center"
        >
          {profile !== undefined && profile !== null ? profile.fio : ''}
        </Typography>
        {allRoles}
      </Box>
      <Divider />
      <Box sx={{ p: 2 }}>
        <List>
          {menu.map((item) => {
            if (item.type === 'divider') {
              return (<Divider />);
            } else {
              return (
                <NavItem
                  href={item.href}
                  key={item.title}
                  title={item.title}
                  icon={item.icon}
                />
              );
            }
          }
          )}
        </List>
      </Box>
      <Box sx={{ flexGrow: 1 }} />
      <div style={{
        // position: 'absolute',
        // left: 0,
        // bottom: 0,
        // right: 0,
        height: 40,
        'text-align': 'center',
        // backgroundColor: 'red',
      }}><b><h4 style={{
        fontFamily: 'Source Sans Pro'
      }}>v. 01.10.02</h4></b></div>
    </Box>
  );

  return (
    <div>
      <Hidden lgUp>
        <Drawer
          anchor="left"
          onClose={onMobileClose}
          open={openMobile}
          variant="temporary"
          PaperProps={{
            sx: {
              width: 256
            }
          }}
        >
          {content}
        </Drawer>
      </Hidden>
      <Hidden lgDown>
        <Drawer
          anchor="left"
          open
          variant="persistent"
          PaperProps={{
            sx: {
              width: 256,
              top: 64,
              height: 'calc(100% - 64px)'
            }
          }}
        >
          {content}
        </Drawer>
      </Hidden>
    </div>
  );
};

DashboardSidebar.propTypes = {
  onMobileClose: PropTypes.func,
  openMobile: PropTypes.bool
};

DashboardSidebar.defaultProps = {
  onMobileClose: () => { },
  openMobile: false
};

export default DashboardSidebar;
