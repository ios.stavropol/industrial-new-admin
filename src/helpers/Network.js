import React from 'react';
import { observable } from "mobx";
import Config from './../constants/Config';

class Network extends React.Component {

  isLoading = observable.box(false);
  modal = observable.box(false);
  userProfile = observable({});
  selectedUnit = observable([]);
  navigate = null;
  updateLayout = false;

  onUnitEdit = null;
  onUnitDelete = null;
  onSelectProcess = null;
  onAdminProcessEdit = null;
  onAdminProcessDelete = null;

  constructor(props) {
    super(props);
    // mobx.autorun(() => {
    //
    // });
  }
}

const network = new Network();
export default network;

export function login(login, password) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/user/login?login="+login+'&password='+password, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log(res);
        if(res.status === 200) {
          network.userProfile = res.profile;
          localStorage.setItem('authToken', res.profile.auth_key);
          localStorage.setItem('roles', res.profile.roles);
          localStorage.setItem('menu', res.profile.menu);
          resolve();
        } else {
          reject(res.message);
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function logout() {
  return new Promise(function(resolve, reject) {
    network.userProfile = null;
    localStorage.removeItem('authToken');
    localStorage.removeItem('roles');
    resolve();
  });
}

export function getProfile() {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/user/get-profile", {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getProfile:::: '+JSON.stringify(res));
        if(res.status == 200) {
          network.userProfile = res.profile;
          localStorage.setItem('roles', res.profile.roles);
          localStorage.setItem('menu', res.profile.menu);
          resolve(res.profile);
        } else {
          if(res.status == 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getStaff() {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/staff/get-staff", {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getStaff: '+res);
        if(res.status === 200) {
          resolve(res.staff);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function saveStaff(id, name) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/staff/save", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: JSON.stringify({
        id,
        name,
      }),
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('saveStaff: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function deleteStaff(id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/staff/delete-staff?id=" + id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('deleteStaff: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getPermissions() {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/settings/get-permissions", {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log(res);
        if(res.status === 200) {
          resolve(res.permissions);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function updatePermission(role, permission, value) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/settings/update-permission", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: JSON.stringify({
        role,
        permission,
        value
      })
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log(res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            if(res.hasOwnProperty('code')) {
              reject('Ошибка базы данных!');
            } else {
              reject(res.message);
            }
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getUnits() {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/unit/get-units", {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getUnits: '+res);
        if(res.status === 200) {
          resolve(res.units);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function saveUnit(id, name) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/unit/save", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: JSON.stringify({
        id,
        name,
      }),
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('saveUnit: '+res);
        if(res.status === 200) {
          resolve(res.id);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function deleteUnit(id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/unit/delete-unit?id=" + id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('deleteUnit: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function createUnit(id, name, company_id, parent_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/unit/create-unit", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: JSON.stringify({
        id,
        name,
        company_id,
        parent_id,
      }),
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('createUnit: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getCompanyUnits(company_id, parent_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/unit/get-company-units?company_id=" + company_id + '&parent_id=' + parent_id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getCompanyUnits: '+res);
        if(res.status === 200) {
          resolve(res.units);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getAllCompanyUnits(company_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/unit/get-all-company-units?company_id=" + company_id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getAllCompanyUnits: '+res);
        if(res.status === 200) {
          resolve(res.units);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getCompanyUnitsList(company_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/unit/get-company-units-list?company_id=" + company_id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getCompanyUnitsList: '+res);
        if(res.status === 200) {
          resolve(res.units);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function deleteCompanyUnit(id, company_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/unit/delete-company-unit?company_id=" + company_id + '&id=' + id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('deleteCompanyUnit: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getCompanies() {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/company/get-companies", {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getCompanies: '+res);
        if(res.status === 200) {
          resolve(res.companies);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function saveCompany(id, name, address, email, phone, avatar) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/company/save", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: JSON.stringify({
        id,
        name,
        address,
        email,
        phone,
        avatar,
      }),
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('saveCompany: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function deleteCompany(id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/company/delete-company?id=" + id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('deleteCompany: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getCompany(id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/company/get-company?id=" + id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getCompany: '+res);
        if(res.status === 200) {
          resolve(res.company);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getCompanyUsers(id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/company/get-company-users?company_id=" + id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getCompanyUsers: '+res);
        if(res.status === 200) {
          resolve(res.users);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function setUserStatus(id, status) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/user/set-user-status?user_id=" + id + '&status=' + status, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('setUserStatus: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function updateCompanyUser(id, lastname, firstname, patrname, email, avatar, role, company_id, staff_id, staff, unit_id, status) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/company/update-company-user", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: JSON.stringify({
        id,
        lastname,
        firstname,
        patrname,
        email,
        avatar,
        role,
        company_id,
        staff_id,
        staff,
        unit_id,
        status,
      }),
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('updateCompanyUser: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function deleteCompanyUser(id, company_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/company/delete-company-user?company_id=" + company_id + '&id=' + id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('deleteCompanyUser: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getCompanyStaff(id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/staff/get-company-staff?company_id=" + id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getCompanyStaff: '+res);
        if(res.status === 200) {
          resolve(res.staff);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getProcess() {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/process/get-process", {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getProcess: '+res);
        if(res.status === 200) {
          resolve(res.process);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function deleteProcess(id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/process/delete-process?id=" + id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('deleteProcess: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function createProcess(id, name, parent_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/process/create-process", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: JSON.stringify({
        id,
        name,
        parent_id,
      }),
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('createProcess: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getChecklists(process_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/checklist/get-checklists?process_id=" + process_id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getChecklists: '+res);
        if(res.status === 200) {
          resolve(res.checklists);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function deleteChecklist(id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/checklist/delete-checklist?id=" + id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('deleteChecklist: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function saveChecklist(id, name, process_id, role) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/checklist/save", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: JSON.stringify({
        id,
        name,
        process_id,
        role,
      }),
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('saveChecklist: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function cloneChecklist(id, name, process_id, role) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/checklist/clone", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: JSON.stringify({
        id,
        name,
        process_id,
        role,
      }),
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('cloneChecklist: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getQuestions(checklist_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/checklist/get-questions?checklist_id=" + checklist_id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getQuestions: '+res);
        if(res.status === 200) {
          resolve(res.questions);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getAllQuestions(checklist_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/checklist/get-all-questions?checklist_id=" + checklist_id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getAllQuestions: '+res);
        if(res.status === 200) {
          resolve(res.questions);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function deleteQuestion(id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/checklist/delete-question?id=" + id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('deleteQuestion: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function saveQuestion(id, question, is_new, checklist_id, type, has_comment, has_deviation, answers, is_shuffle, prev_id, answer_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/checklist/save-question", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: JSON.stringify({
        id,
        question,
        is_new,
        checklist_id,
        type,
        has_comment,
        has_deviation,
        answers,
        is_shuffle,
        prev_id,
        answer_id,
      }),
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('saveQuestion: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getCompanyProcess(company_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/company/get-company-process?company_id=" + company_id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getCompanyProcess: '+res);
        if(res.status === 200) {
          resolve(res.process);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function setCompanyProcess(company_id, process_id, selected) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/company/set-company-process?company_id=" + company_id + '&process_id=' + process_id + '&selected=' + selected, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('setCompanyProcess: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getCompanyProcessForAudit(company_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/process/get-company-process?company_id=" + company_id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getCompanyProcessForAudit: '+res);
        if(res.status === 200) {
          resolve(res.process);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getAuditsForCompany(company_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/audit/get-audits-for-company?company_id=" + company_id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getAuditsForCompany: '+res);
        if(res.status === 200) {
          resolve(res.audits);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getAudits() {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/audit/get-audits", {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getAudits: '+res);
        if(res.status === 200) {
          resolve(res.audits);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function deleteAudit(id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/audit/delete-audit?id=" + id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('deleteAudit: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function saveAudit(id, target, date, process_id, users, ready = null) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/audit/save", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: JSON.stringify({
        id,
        target,
        date,
        users,
        process_id,
        ready,
      }),
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('saveAudit: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function setReady(id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/audit/set-ready?id=" + id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('setReady: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getAudit(id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/audit/get-audit?id=" + id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getAudit: '+res);
        if(res.status === 200) {
          resolve(res.audit);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getTasks() {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/audit/get-tasks", {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getTasks: '+res);
        if(res.status === 200) {
          resolve(res.tasks);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getTask(task_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/audit/get-task?task_id=" + task_id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getTask: '+res);
        if(res.status === 200) {
          resolve(res.task);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function answerTask(task_id, answers, flag) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/audit/answer-task", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: JSON.stringify({
        task_id,
        answers,
        flag
      }),
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('answerTask: '+res);
        if(res.status === 200) {
          resolve(res.task);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function endTask(task) {
  return new Promise(function(resolve, reject) {

    const formData = new FormData();
    formData.append('task_id', task.id);

    // let array = [];

    // let answers = JSON.parse(task.answers);
    // console.warn('answers: ', answers);
    // let proms = [];
    // for (let i = 0; i < answers.length; i++) {
    //   if (answers[i].deviation) {
    //     for (let y = 0; y < answers[i].deviation.length; y++) {
    //       for (let z = 0; z < answers[i].deviation[y].photos.length; z++) {
    //         proms.push(fetch(answers[i].deviation[y].photos[z]));
    //       }
    //     }
    //   }
    // }
    // Promise.all(proms).then(values => {
    //   let array = [];
    //   for (let i = 0; i < values.length; i++) {
    //     array.push(values[i].blob());
    //   }
    //   Promise.all(array).then(blobs => {
        
    //   });
    // });
    // return;

    fetch(Config.apiDomain+"/v1/audit/end-task", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        // 'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: formData,
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('endTask: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getAuditStatistic(company_id, audit_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/audit/get-audit-statistic?company_id=" + company_id + "&audit_id=" + audit_id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getAuditStatistic: '+res);
        if(res.status === 200) {
          resolve(res.statistic);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getUserStatistic(audit_id, user_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/audit/get-user-statistic?audit_id=" + audit_id + "&user_id=" + user_id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getUserStatistic: '+res);
        if(res.status === 200) {
          resolve(res.statistic);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getProcessStatistic(company_id, process_id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/audit/get-process-statistic?company_id=" + company_id + "&process_id=" + process_id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getProcessStatistic: '+res);
        if(res.status === 200) {
          resolve(res.statistic);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function updateSelf(lastname, firstname, patrname, avatar) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/user/update-self", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: JSON.stringify({
        lastname,
        firstname,
        patrname,
        avatar,
      })
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('updateSelf: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function updatePassword(password, new_password) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/user/update-password", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: JSON.stringify({
        password,
        new_password,
      })
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('updatePassword: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getLastStatistic() {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/audit/get-last-statistic", {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getLastStatistic: '+res);
        if(res.status === 200) {
          resolve(res.process);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function uploadQuestions(id, file) {
  return new Promise(function(resolve, reject) {

    const formData = new FormData();
    formData.append('id', id);
    formData.append('file', file);

    fetch(Config.apiDomain+"/v1/checklist/upload-questions", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: formData,
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('uploadQuestions: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.history.push('/login');
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getProcessesStat(process_id, from, to, unit_id, role) {
  return new Promise(function(resolve, reject) {

    let str = '';

    if (process_id !== null && process_id !== 0) {
      str = str + '&process_id=' + process_id;
    }
    if (from !== null && from !== 0 && from.length !== 0) {
      str = str + '&from=' + from;
    }
    if (to !== null && to !== 0 && to.length !== 0) {
      str = str + '&to=' + to;
    }
    if (unit_id !== null && unit_id !== 0) {
      str = str + '&unit_id=' + unit_id;
    }
    if (role !== null && role !== 0) {
      str = str + '&role=' + role;
    }

    fetch(Config.apiDomain+"/v1/audit/get-processes?x=0" + str, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getProcessesStat: '+res);
        if(res.status === 200) {
          resolve(res);
        } else {
          if(res.status === 401) {
            network.history.push('/login');
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getDeviation(id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/audit/get-deviation?id=" + id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getDeviation: '+res);
        if(res.status === 200) {
          resolve(res);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function addEvent(id, dev_id, user_id, text, date) {
  return new Promise(function(resolve, reject) {
    console.warn(JSON.stringify({
      id,
      dev_id,
      user_id,
      text,
      date,
    }));
    fetch(Config.apiDomain+"/v1/audit/add-event", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: JSON.stringify({
        id,
        dev_id,
        user_id,
        text,
        date,
      }),
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('addEvent: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function deleteEvent(id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/audit/delete-event?id=" + id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('deleteEvent: '+res);
        if(res.status === 200) {
          resolve();
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function getMyEvents() {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/audit/get-my-events", {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('getMyEvents: '+res);
        if(res.status === 200) {
          resolve(res.events);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function execEvent(id) {
  return new Promise(function(resolve, reject) {
    fetch(Config.apiDomain+"/v1/audit/exec-event?id=" + id, {
      method: "GET",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('execEvent: '+res);
        if(res.status === 200) {
          resolve(res.events);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}

export function endEvent(id, exec_text, files) {
  return new Promise(function(resolve, reject) {

    const formData = new FormData();
    formData.append('id', id);
    formData.append('exec_text', exec_text);
    for (let i=0;i<files.length;i++) {
      // formData.append("docs_" + i, {uri: files[i], name: 'image.jpg', type: 'image/jpg'})
      formData.append("docs_" + i, files[i]);
    }

    fetch(Config.apiDomain+"/v1/audit/end-event", {
      method: "POST",
      mode: 'cors',
      'Access-Control-Allow-Origin': 'http://localhost:80',
      'Access-Control-Max-Age': 3600,
      headers: {
        'Accept': 'application/json',
        // 'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('authToken')
      },
      body: formData,
    })
    .then(result => {
      return result.json();
    })
    .then(res => {
        console.log('endEvent: '+res);
        if(res.status === 200) {
          resolve(res.events);
        } else {
          if(res.status === 401) {
            network.navigate('/login', { replace: true });
          } else {
            reject(res.message);
          }
        }
    })
    .catch(err => {
      console.log('error: '+err);
      reject(err);
    });
  });
}