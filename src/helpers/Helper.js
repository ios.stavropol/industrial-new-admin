import React from 'react';

class Helper extends React.Component {

  constructor(props) {
    super(props);
  }

  fromDBDateTimeToRusDateTime = time => {
    time = time.split(' ');
    time = new Date(time[0] + 'T' + time[1] + '.000Z');
    let dd = time.getDate();
    dd = dd < 10 ? '0' + dd : dd;

    let mm = time.getMonth() + 1;
    mm = mm < 10 ? '0' + mm : mm;

    let yyyy = time.getFullYear();

    let hh = time.getHours();
    hh = hh < 10 ? '0' + hh : hh;

    let ii = time.getMinutes();
    ii = ii < 10 ? '0' + ii : ii;

    let ss = time.getSeconds();
    ss = ss < 10 ? '0' + ss : ss;

    return dd + '.' + mm + '.' + yyyy + ' ' + hh + ':' + ii + ':' + ss;
  }

  fromDBDateToRusDateTime = time => {
    time = time.split(' ');
    time = new Date(time[0] + 'T' + time[1] + '.000Z');
    let dd = time.getDate();
    dd = dd < 10 ? '0' + dd : dd;

    let mm = time.getMonth() + 1;
    mm = mm < 10 ? '0' + mm : mm;

    let yyyy = time.getFullYear();

    let hh = time.getHours();
    hh = hh < 10 ? '0' + hh : hh;

    let ii = time.getMinutes();
    ii = ii < 10 ? '0' + ii : ii;

    let ss = time.getSeconds();
    ss = ss < 10 ? '0' + ss : ss;

    return dd + '.' + mm + '.' + yyyy;
  }

  getDateFromJSDateTime = date => {

    let dd = date.getDate();
    dd = dd < 10 ? '0' + dd : dd;

    let mm = date.getMonth() + 1;
    mm = mm < 10 ? '0' + mm : mm;

    let yyyy = date.getFullYear();

    let str = yyyy + '-' + mm + '-' + dd;

    return str;
  }

  getShortDateFromJSDateTime = date => {

    let mm = date.getMonth() + 1;
    mm = mm < 10 ? '0' + mm : mm;

    let yyyy = date.getFullYear();

    let str = yyyy + '-' + mm;

    return str;
  }

  getRusDateTimeFromJSDateTime = date => {

    let dd = date.getDate();
    dd = dd < 10 ? '0' + dd : dd;

    let mm = date.getMonth() + 1;
    mm = mm < 10 ? '0' + mm : mm;

    let yyyy = date.getFullYear();

    let hh = date.getHours();
    hh = hh < 10 ? '0' + hh : hh;

    let ii = date.getMinutes();
    ii = ii < 10 ? '0' + ii : ii;

    let str = dd + '.' + mm + '.' + yyyy + ' ' + hh + ':' + ii;

    return str;
  }

  getRusDateFromJSDateTime = date => {

    let dd = date.getDate();
    dd = dd < 10 ? '0' + dd : dd;

    let mm = date.getMonth() + 1;
    mm = mm < 10 ? '0' + mm : mm;

    let yyyy = date.getFullYear();

    let str = dd + '.' + mm + '.' + yyyy;

    return str;
  }

  getDateTimeFromJSDateTime = date => {

    let dd = date.getDate();
    dd = dd < 10 ? '0' + dd : dd;

    let mm = date.getMonth() + 1;
    mm = mm < 10 ? '0' + mm : mm;

    let yyyy = date.getFullYear();

    let hh = date.getHours();
    hh = hh < 10 ? '0' + hh : hh;

    let ii = date.getMinutes();
    ii = ii < 10 ? '0' + ii : ii;

    let str = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + ii + ':00';

    return str;
  }

  getTimeFromJSDateTime = date => {

    let hh = date.getHours();
    hh = hh < 10 ? '0' + hh : hh;

    let ii = date.getMinutes();
    ii = ii < 10 ? '0' + ii : ii;

    let str = hh + ':' + ii;

    return str;
  }

  getDigitStr = (digit, strings) => {
    let noteStr = '';

    if (digit > 10 && digit < 20) {
      noteStr = digit + ' ' + strings.d11_d19;
    } else if (digit % 10 == 1) {
      noteStr = digit + ' ' + strings.d1;
    } else if (digit % 10 > 1 && digit % 10 < 5) {
      noteStr = digit + ' ' + strings.d2_d4;
    } else {
      noteStr = digit + ' ' + strings.d5_d10;
    }

    return noteStr;
  }

  reformatPhone = phone => {
    phone = phone.replace(/[^\d.-]/g, '');
    if (phone.length) {
      if (phone.charAt(0) != '+') {
        phone = '+' + phone;
      }
      if (phone.charAt(1) != '7') {
        phone = phone.substring(0, 1) + '7' + phone.substring(2);
      }
      if (phone.length > 2 && phone.length < 6) {
        phone = phone.substring(0, 2) + ' (' + phone.substring(2, 5);
      } else if (phone.length >= 6 && phone.length < 9) {
        phone = phone.substring(0, 2) + " (" + phone.substring(2, 5) + ") " + phone.substring(5, 8);
      } else if (phone.length >= 9 && phone.length < 11) {
        phone = phone.substring(0, 2) + " (" + phone.substring(2, 5) + ") " + phone.substring(5, 8) + "-" + phone.substring(8, 10);
      } else if (phone.length >= 11) {
        phone = phone.substring(0, 2) + " (" + phone.substring(2, 5) +
            ') ' + phone.substring(5, 8) + "-" + phone.substring(8, 10) +
            '-' + phone.substring(10, 12);
      }
    }
    return phone;
  }

  reformatCode = code => {
    code = code.replace(/[^\d.-]/g, '');
    if (code.length) {
      if (code.length > 1 && code.length < 3) {
        code = code.substring(0, 1) + ' ' + code.substring(1, 2);
      } else if (code.length >= 2 && code.length < 4) {
        code = code.substring(0, 1) + ' ' + code.substring(1, 2) + ' ' + code.substring(2, 3);
      } else {
        code = code.substring(0, 1) + ' ' + code.substring(1, 2) + ' ' + code.substring(2, 3) + ' ' + code.substring(3, 4) + ' ' + code.substring(4, 5);
      }
    }
    return code;
  }

  calculateAgeInYears = date => {
    let now = new Date();
    let current_year = now.getFullYear();
    let year_diff = current_year - date.getFullYear();
    let birthday_this_year = new Date(current_year, date.getMonth(), date.getDate());
    let has_had_birthday_this_year = (now >= birthday_this_year);

    return has_had_birthday_this_year
        ? year_diff
        : year_diff - 1;
}

  validMail = mail => {
      return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()\.,;\s@\"]+\.{0,1})+([^<>()\.,;:\s@\"]{2,}|[\d\.]+))$/.test(mail);
  }

  generateUUID = () => {
    var d = new Date().getTime();
    var d2 = ((typeof performance !== 'undefined') && performance.now && (performance.now()*1000)) || 0;
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;
        if (d > 0) {
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        } else {
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }
}

const helper = new Helper();
export default helper;
