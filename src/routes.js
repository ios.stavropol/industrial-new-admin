import React from 'react';
import { Navigate } from 'react-router-dom';
import DashboardLayout from './components/DashboardLayout';
import MainLayout from './components/MainLayout';
import Account from './pages/Account';
import Companies from './pages/Admin/Companies';
import Company from './pages/Admin/Company';
import Staff from './pages/Admin/Staff';
import Units from './pages/Admin/Units';
import Process from './pages/Admin/Process';
import Checklists from './pages/Admin/Checklists';
import Questions from './pages/Admin/Questions';
import Permissions from './pages/Admin/Permissions';

import Company2 from './pages/Company/Company';
import Units2 from './pages/Company/Units';
import Staff2 from './pages/Company/Staff';
import Users from './pages/Company/Users';
import Tasks from './pages/Company/Tasks';
import Task from './pages/Company/Task';
import Audits from './pages/Company/Audits';
import AuditInfo from './pages/Company/AuditInfo';
import AuditResult from './pages/Company/AuditResult';
import UserResult from './pages/Company/UserResult';
import DeviationResult from './pages/Company/DeviationResult';
import Dashboard from './pages/Company/Dashboard';
import Events from './pages/Company/Events';

import Login from './pages/Login';
import NotFound from './pages/NotFound';


const routes = [
  {
    path: 'admin',
    element: <DashboardLayout />,
    children: [
      { path: 'account', element: <Account /> },
      { path: 'company/:id', element: <Company /> },
      { path: 'companies', element: <Companies /> },
      { path: 'process', element: <Process /> },
      { path: 'process/:id', element: <Checklists /> },
      { path: 'process/:id/:checklist', element: <Questions /> },
      { path: 'staff', element: <Staff /> },
      { path: 'units', element: <Units /> },
      { path: 'permissions', element: <Permissions /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  },
  {
    path: 'company',
    element: <DashboardLayout />,
    children: [
      { path: 'account', element: <Account /> },
      { path: 'company', element: <Company2 /> },
      { path: 'units', element: <Units2 /> },
      { path: 'staff', element: <Staff2 /> },
      { path: 'users', element: <Users /> },
      { path: 'tasks', element: <Tasks /> },
      { path: 'tasks/:id', element: <Task /> },
      { path: 'dashboard', element: <Dashboard /> },
      { path: 'audits', element: <Audits /> },
      { path: 'audits/:id/edit', element: <AuditInfo /> },
      { path: 'audits/:id/result', element: <AuditResult /> },
      { path: 'audits/:id/result/:userId', element: <UserResult /> },
      { path: 'audits/:id/deviation/:devId', element: <DeviationResult /> },
      { path: 'events', element: <Events /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  },
  {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: 'login', element: <Login /> },
      { path: '404', element: <NotFound /> },
      { path: '/', element: (localStorage.getItem('roles') !== null && localStorage.getItem('roles') !== undefined && localStorage.getItem('roles').includes('admin') && !localStorage.getItem('roles').includes('admin_company') ? <Navigate to="/admin/companies" /> : (localStorage.getItem('roles') === null || localStorage.getItem('roles') === undefined ? <Navigate to="/login" /> : <Navigate to="/company/tasks" />)) },
      // { path: '*', element: <Navigate to="/404" /> }
    ]
  }
];

export default routes;
